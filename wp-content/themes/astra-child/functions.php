<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
  wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
  wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
	wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
	wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

//Declaring WooCommerce support is straightforward and involves adding one function in your theme’s functions.php file.
function mytheme_add_woocommerce_support() {
  add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

/**
 * Disable add to cart
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 1);
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 1 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 1);
add_filter( 'woocommerce_is_purchasable', '__return_false');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
	if ( isset( $atts['facet'] ) ) {       
		$output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
	}
	return $output;
  }, 10, 2 );

add_action('woocommerce_after_shop_loop_item', 'add_a_custom_button', 5 );
function add_a_custom_button() {
    global $product;

    // Not for variable and grouped products that doesn't have an "add to cart" button
    if( $product->is_type('variable') || $product->is_type('grouped') ) return;

    // Output the custom button linked to the product
    echo '<div style="margin-bottom:10px;">
        <a class="button custom-button" href="' . esc_attr( $product->get_permalink() ) . '">' . __('View product') . '</a>
    </div>';
}

add_filter( 'facetwp_query_args', function( $query_args, $class ) {
    // if ( 'luxury_vinyl_tile' == $class->ajax_params['template'] ) {
      if( @$query_args['is_featured_temp'] == '1'){
        $query_args['posts_per_page']='4';
        $query_args['orderby'] = 'date';

      }else{
        $query_args['posts_per_page']='12';
      }  
     // unset($query_args['is_featured_temp']);
        $query_args['meta_key']='collection';
        if(($query_args['post_type'] !="fl-builder-template" && $query_args['post_type'] !="post" && $query_args['post_type'] !="product") &&  @wp_count_posts($query_args['post_type'])->publish > 1 ){
            add_filter('posts_groupby', 'query_group_by_filter');
        }
    // }
    return $query_args;
}, 10, 2 );

add_filter( 'astra_single_post_meta', 'remove_post_meta_home_page' );
function remove_post_meta_home_page($post_meta) {
 
	$post_meta = '';
	return $post_meta;
 }

 function mmsession_custom_footer_js_astratheme() {
     
  echo "<script src='https://session.mm-api.agency/js/mmsession.js' async></script>";
}
add_action( 'wp_footer', 'mmsession_custom_footer_js_astratheme' );


function year_shortcode() {
  $year = date('Y');
  return $year;
}
add_shortcode('year', 'year_shortcode');

//Roomvo script
add_action('astra_body_bottom', 'astra_roomvo_custom_footer_js');
function astra_roomvo_custom_footer_js() {

    $website_json_data = json_decode(get_option('website_json'));

    foreach($website_json_data->sites as $site_cloud){
            
        if($site_cloud->instance == 'prod'){
    
            if( $site_cloud->roomvo == 'true'){
    
  echo "<script src='https://www.roomvo.com/static/scripts/b2b/mobilemarketing.js' async></script>";

            }
        }
    }
}