<div class="product-grid featured col-md-12 col-sm-12 col-xs-12" itemscope itemtype="http://schema.org/CollectionPage">
    <div class="row">
<?php while ( $prod_list->have_posts() ): $prod_list->the_post(); ?>
    <div class="col-md-3 col-sm-6 col-xs-6">
 
    <div class="fl-post-grid-post" itemscope itemtype="Product">
        <?php FLPostGridModule::schema_meta(); ?>
        <?php
        
        if(get_field('swatch_image_link')){
            // loop through the rows of data
            // $gallery_images = get_field('gallery_room_images');
        
            // $room_image = single_gallery_image_product(get_the_ID(),'222','222');
        //}
       
        if( get_option('plpproductimg') !='' && get_option('plpproductimg') == '1' ){
            $gallery_images = get_field('swatch_image_link');
            $gallery_img = explode("|", $gallery_images);
            $room_image =  $gallery_img[0] ? thumb_gallery_images_in_plp_loop($gallery_img[0]) : swatch_image_product_thumbnail(get_the_ID(),'222','222');;
             
          }else{
            $room_image = swatch_image_product_thumbnail(get_the_ID(),'222','222');
          }	
		?>
            <div class="fl-post-grid-image">

                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				
                    <!-- <div class="grid-room" style=" position: relative; width: 100%; height: 250px; background: url(<?php echo $room_image;?>) no-repeat bottom center; background-size: cover;"> -->

                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="<?php echo $room_image; ?>" style=" position: relative; width: 100%; height: 250px;" alt="<?php the_title_attribute(); ?>" /> 
                    <!-- </div> -->
                </a>

            </div>
        <?php } else { ?>
            <div class="fl-post-grid-image">

                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php //the_post_thumbnail($settings->image_size); ?>
                    <img src="http://placehold.it/300x300?text=No+Image" alt="<?php the_title_attribute(); ?>" />
                </a>

            </div>

        <?php } ?>
        <div class="fl-post-grid-text product-grid btn-grey">

            <h4><?php the_title();//the_field('collection'); ?></h4>

            <h3 class="fl-post-grid-title" itemprop="headline">
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php //the_title(); ?><?php the_field('brand'); ?></a>
            </h3>
            <?php if(get_field('retail_price')) { ?>
<span class="strike-price">$<?php the_field('retail_price'); ?></span>
            <?php } ?>

            <?php if(get_field('sale_price')) { ?>
                <span class="sale-price">$<?php the_field('sale_price'); ?> sq. ft.</span>
            <?php } ?>


            <!-- <div class="fl-button-wrap fl-button-width-auto fl-button-center">
                <a href="<?php echo site_url(); ?>/flooring-coupon/" target="_self" class="fl-button" role="button">
                    <span class="fl-button-text">GET COUPON</span>
                </a>
            </div> -->


            <a class="greylink" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">VIEW PRODUCT</a>


        </div>
    </div>
    </div>
<?php endwhile; wp_reset_postdata();?>
</div>
</div>