;(function($) {

    WooPackModal = function( settings )
    {
        this.wrapper        = $('.woopack-modal');
        this.contentWrap    = $('.woopack-modal .woopack-modal-content');
        this.settings       = settings;
        this.breakpoint     = this.settings.breakpoint;
        this.animationSpeed = this.settings.animationSpeed;
        this.isActive       = false;

        if ( 'undefined' === typeof this.settings.source ) {
            console.error( 'Error initiating modal. Source is missing!' );
            return;
        }
        if ( 'html' === this.settings.source && 'undefined' === typeof this.settings.content ) {
            console.error( 'Error initiating modal. Content is missing!' );
            return;
        }
        if ( 'url' === this.settings.source && 'undefined' === typeof this.settings.url ) {
            console.error( 'Error initiating modal. URL is missing!' );
            return;
        }
        if ( 'ajax' === this.settings.source ) {
            if ( 'undefined' === typeof this.settings.ajaxUrl ) {
                console.error( 'Error initiating modal. AJAX URL is missing!' );
                return;
            }
            if ( 'object' !== typeof this.settings.ajaxData ) {
                console.error( 'Error initiating modal. AJAX data is missing!' );
                return;
            }
        }

        this._init();
    };

    WooPackModal.prototype = {
        wrapper: '',
        contentWrap: '',
        settings: {},
        isActive: '',
        breakpoint: 767,
        animationSpeed: 100,

        _init: function()
        {
            this._initModal();
            this._bindEvents();
            this._resize();
            this._resetHeight();
            this._show();
        },

        _initModal: function()
        {
            this.wrapper.removeClass( 'woopack-modal-fullscreen' );
            this.wrapper.removeClass('woopack-modal-has-iframe');

            if ( 'string' === typeof this.settings.cssClass ) {
                this.wrapper.addClass(this.settings.cssClass);
            }

            if ( this.settings.fullscreen ) {
                this.wrapper.addClass( 'woopack-modal-fullscreen' );
            }
            if ( 'undefined' !== typeof this.settings.closeButton && ! this.settings.closeButton ) {
                this.wrapper.addClass('woopack-modal-hide-close');
            }

            if ( 'html' == this.settings.source ) {
                this._initContent();
            }
            if ( 'url' == this.settings.source ) {
                this.wrapper.addClass('woopack-modal-has-iframe');
                this._initIframe();
            }
            if ( 'ajax' === this.settings.source ) {
                this._initAjax();
            }
        },

        _initContent: function()
        {
            this.contentWrap.html(this.settings.content);
        },

        _initIframe: function()
        {
            var iframe = '<iframe src="'+this.settings.url+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            this.contentWrap.html(iframe);
        },

        _initAjax: function()
        {
            var ajaxUrl     = this.settings.ajaxUrl,
                ajaxData    = this.settings.ajaxData,
				self        = this;
				
			if ( 'undefined' !== typeof this.settings.processAjax && ! this.settings.processAjax ) {
				if ( 'function' === typeof self.settings.responseHandler ) {
					self.settings.responseHandler( {}, self );
				}
				self.wrapper.addClass('woopack-content-loaded');
				if ( 'function' === typeof self.settings.onAjaxLoad ) {
					self.settings.onAjaxLoad( self );
				}
				return;
			}

            $.ajax({
                type: 'post',
                url: ajaxUrl,
                data: ajaxData,
                success: function(response) {
					if ( 'function' === typeof self.settings.responseHandler ) {
						self.settings.responseHandler( response, self );
					} else {
						self.contentWrap.html(response);
					}
					setTimeout(function() {
						self.wrapper.addClass('woopack-content-loaded');
						if ( 'function' === typeof self.settings.onAjaxLoad ) {
							self.settings.onAjaxLoad(self);
						}
					}, 100);
                }
            });
        },

        _bindEvents: function()
        {
            var self = this;

            $('.woopack-modal-close, .woopack-modal-close a, .woopack-modal-overlay').on('click', function(e) {
                e.preventDefault();
                self._hide();
            });

            if ( 'undefined' !== typeof this.settings.closeButtonEl && $(this.settings.closeButtonEl).length > 0 ) {
                $('body').delegate(this.settings.closeButtonEl, 'click', function(e) {
                    e.preventDefault();
                    self._hide();
                });
            }

            $(document).keyup(function(e) {
                if ( 27 == e.which && self.isActive ) {
                    self._hide();
                }
			});
			
			this.wrapper.find( '.woopack-modal-inner' ).on( 'click', $.proxy( this._resetHeight, this ) )
        },

        _show: function()
        {
            if ( this.isActive ) {
                return;
            }

            var speed = this.animationSpeed;

            $('body').trigger('woopack-modal-before-trigger');

            this.wrapper.fadeIn(speed).addClass('woopack-modal-active');
            this.isActive = true;

            if ( 'function' === typeof this.settings.onLoad ) {
                this.settings.onLoad(this.wrapper);
            }

            $('body').trigger('woopack-modal-after-trigger');
        },

        _hide: function()
        {
            var speed = this.animationSpeed;
            var wrapper = this.wrapper;
            var contentWrap = this.contentWrap;
            var self = this;

            $('body').trigger('woopack-modal-before-close');

            wrapper.fadeOut(speed, function() {
                contentWrap.html('');
                wrapper.removeClass('woopack-content-loaded');
                if ( 'function' === typeof self.settings.onClose ) {
                    self.settings.onClose(wrapper);
                }
            }).removeClass('woopack-modal-active');

            $('body').trigger('woopack-modal-after-close');

            this.isActive = false;
        },

        _resize: function()
        {	
            if ( 'object' === typeof this.settings.style ) {
                this.wrapper.find('.woopack-modal-inner').css(this.settings.style);
            }
            if ( window.innerWidth <= this.settings.breakpoint ) {
                this.wrapper.addClass('woopack-responsive-modal');
                this.wrapper.find('.woopack-modal-inner').css({
                    height: ( window.innerHeight ) + 'px',
                });
            }
		},
		
		_resetHeight: function() {
			var wrapper = this.wrapper;
			if ( wrapper.find('.woopack-modal-inner').height() >= window.innerHeight ) {
				wrapper.find('.woopack-modal-inner').css({
					'max-height': ( window.innerHeight - 60 ) + 'px',
				});
				wrapper.find('.woopack-modal-inner .woopack-modal-content').css({
					'max-height': ( window.innerHeight - 80 ) + 'px',
				});
			}
		}
    };

})(jQuery);
