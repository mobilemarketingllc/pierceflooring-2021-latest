<?php if ( isset( $settings->product_id ) && $settings->product_id != '' ) { ?>

<?php do_action( 'woopack_before_cart_button_wrap', $settings ); ?>

<div class="woopack-product-add-to-cart woocommerce">
	<div class="woopack-product-action">
	<?php do_action( 'woopack_before_cart_button', $settings ); ?>

	<?php

	global $post;

	$product_data = get_post( $settings->product_id );

	$product = is_object( $product_data ) && in_array( $product_data->post_type, array( 'product', 'product_variation' ), true ) ? wc_setup_product_data( $product_data ) : false;

	if ( $product ) {

		if ( $product->is_type( 'variable' ) && ( isset( $settings->variation_fields ) && 'yes' == $settings->variation_fields ) ) {
			woocommerce_variable_add_to_cart();
		}  elseif ( $product->is_type( 'grouped' ) && isset( $settings->variation_fields ) && 'yes' == $settings->variation_fields ) {
			woocommerce_grouped_add_to_cart();
		} else {
			?>
			<div class="product woocommerce add_to_cart_inline">

				<?php if ( 'yes' == $settings->product_price ) { // Price ?>
					<div class="woopack-product-price">
						<?php echo $product->get_price_html(); ?>
					</div>
				<?php } ?>

				<div class="woopack-product-action-inner">
					<?php if ( isset( $settings->qty_input ) && 'yes' == $settings->qty_input ) { // Quantity input field before button. ?>
						<?php //do not render quanity input if product has variation but variation_fields is set to "no". ?>
						<?php if ( ! $product->is_type( 'variable' ) ) { ?>
							<?php do_action( 'woopack_add_to_cart_before_quantity_input', $settings, $product ); ?>
							<div class="woopack-qty-input">
								<?php woocommerce_quantity_input( array(
									'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
									'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
									'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
								) ); ?>
							</div>
							<?php do_action( 'woopack_add_to_cart_after_quantity_input', $settings, $product ); ?>
						<?php } ?>
					<?php } ?>
					<?php do_action( 'woopack_add_to_cart_before_button', $settings, $product ); ?>
					<?php woocommerce_template_loop_add_to_cart(); ?>
					<?php do_action( 'woopack_add_to_cart_after_button', $settings, $product ); ?>
				</div>
			</div>
			<?php
		}

	}

	// Restore Product global in case this is shown inside a product post.
	wc_setup_product_data( $post );

	?>

	<?php do_action( 'woopack_after_cart_button', $settings ); ?>
	</div>
</div>

<?php do_action( 'woopack_after_cart_button_wrap', $settings ); ?>

<?php } ?>