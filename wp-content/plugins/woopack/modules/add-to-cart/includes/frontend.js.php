;(function($){

	$('.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart a.button').addClass('alt');
	$('.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-qty-input input.qty').on('change blur', function() {
		var qty = $(this).val();
		if ( '' === qty || isNaN( qty ) || qty < 1 ) {
			qty = 1;
		}
		$('.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action a.button').attr('data-quantity', qty);
	});

})(jQuery);
