<?php
/**
* @class WooPackAddToCart
*/
class WooPackAddToCart extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Add To Cart', 'woopack'),
            'description' 		=> __('Addon to display Add To Cart.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/add-to-cart/',
            'url' 				=> WOOPACK_URL . 'modules/add-to-cart/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
	}
	public function filter_settings( $settings, $helper )
	{
		// Handle old Button border and radius fields.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'button_border_style'	=> array(
				'type'				=> 'style',
			),
			'button_border_width'	=> array(
				'type'				=> 'width',
			),
			'button_border_color'	=> array(
				'type'				=> 'color',
			),
			'button_border_radius'	=> array(
				'type'				=> 'radius',
			),
		), 'button_border_group' );

        // Handle old Button font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'button_font'   => array(
                'type'          => 'font'
            ),
            'button_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->button_font_size ) && 'custom' == $settings->button_font_size )
            ),
            'button_line_height'    => array(
                'type'          => 'line_height',
            ),
            'button_text_transform' => array(
                'type'          => 'text_transform',
                'condition'     => ( isset( $settings->button_text_transform ) && 'default' != $settings->button_text_transform )
            )
		), 'button_typography' );

        // Handle old Regular Price font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'ragular_price_font'   => array(
                'type'          => 'font'
            ),
            'ragular_price_font_size'   => array(
                'type'          => 'font_size',
            )
		), 'ragular_price_typography' );

		return $settings;
	}

	public static function get_products_list() {
		if ( ! isset( $_GET['fl_builder'] ) ) {
			return array();
		}

		return WooPack_Helper::get_products_list();
	}
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackAddToCart', array(
    'structure'     => array(
        'title'         => __('Structure', 'woopack'),
        'sections'      => array(
            'product'       => array(
                'title'         => __('Product', 'woopack'),
                'fields'        => array(
                    'product_id'    => array(
                        'type'          => 'select',
                        'label' 		=> __('Product', 'woopack'),
						'options'       => WooPackAddToCart::get_products_list(),
						'connections'	=> array('string')
					),
                ),
            ),
            'alignment'     => array(
				'title'         => __('Alignment', 'woopack'),
				'collapsed'		=> true,
                'fields'        => array(
                    'align'          => array(
                        'type'          => 'align',
                        'label'         => __('Alignment', 'woopack'),
                        'default'       => 'center',
                    ),
                ),
            ),
            'product_price' => array(
				'title'         => __('Price', 'woopack'),
				'collapsed'		=> true,
                'fields'        => array(
                    'product_price' 	=> array(
                        'type'          => 'select',
                        'label'         => __('Show Price?', 'woopack'),
                        'default'       => 'yes',
                        'options'       => array(
                            'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack'),
                        ),
                        'toggle'         => array(
                            'yes'           => array(
                                'sections'      => array( 'ragular_price_fonts', 'sale_price_fonts' ),
                            ),
                        ),
					),
					'variation_fields'	=> array(
						'type'          => 'select',
                        'label'         => __('Show Variation Fields?', 'woopack'),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack'),
                        ),
					),
					'qty_input'			=> array(
						'type'			=> 'select',
						'label'			=> __('Show Quantity?', 'woopack'),
						'default'		=> 'no',
						'options'		=> array(
							'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack'),
						)
					),
                    'price_spacing' 	=> array(
                        'type'				=> 'unit',
                        'label'				=> __('Spacing', 'woopack'),
                        'default'			=> '5',
                        'units'				=> array('px'),
						'slider'			=> true,
                    )
                ),
            ),
        ),
    ),
    'style'         => array(
        'title'         => __('Style', 'woopack'),
        'sections'      => array(
            'button_property'   => array(
                'title'             => __('Structure', 'woopack'),
                'fields'            => array(
                    'button_width'          => array(
                        'type'                  => 'select',
                        'label'                 => __('Width', 'woopack'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'woopack'),
                            'full_width'            => __('Full Width', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('button_width_custom')
                            ),
                        ),
                    ),
                    'button_width_custom'   => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Width', 'woopack'),
						'units'					=> array('%'),
						'slider'				=> true,
						'default'				=> 45,
                    ),
                ),
            ),
            'button_color'      => array(
				'title'             => __('Button Colors', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_bg_color'		=> array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type' 				 => 'css',
                            'selector'			   => '.woopack-product-add-to-cart .button',
                            'property'			   => 'background-color',
                        ),
                    ),
                    'button_bg_color_hover' => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Hover Color', 'woopack'),
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type' 				 => 'css',
                            'selector'			   => '.woopack-product-add-to-cart .button:hover',
                            'property'			   => 'background-color',
                        ),
                    ),
                    'button_color'          => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-product-add-to-cart .button',
                            'property'              => 'color',
                        ),
                    ),
                    'button_color_hover'    => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Hover Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-product-add-to-cart .button:hover',
                            'property'              => 'color',
                        ),
                    ),
                ),
            ),
            'button_border'     => array(
				'title'             => __('Button Border', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
					'button_border_group'		=> array(
						'type'					=> 'border',
						'label'					=> __('Border Style', 'bb-powerpack'),
						'responsive'			=> true,
					),
                    'button_border_color_hover' => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Hover Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-product-add-to-cart .button:hover',
                            'property'                  => 'border-color',
                        ),
                    ),
                ),
            ),
            'button_padding'    => array(
				'title'             => __('Button Padding', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_padding'    => array(
                        'type'                  => 'dimension',
                        'label'                 => __('Padding', 'woopack'),
						'slider'		        => true,
						'units'		  	        => array( 'px' ),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-product-add-to-cart .button',
                            'property'              => 'padding',
                            'unit'                  => 'px'
                        ),
                        'responsive'            => true,
                    ),
                ),
			),
			'variation_style'	=> array(
				'title'				=> __('Variation Table', 'woopack'),
				'collapsed'			=> true,
				'fields'			=> woopack_product_variation_style_fields()
			)
        ),
    ),
    'typography'    => array(
        'title'         => __('Typography', 'woopack'),
        'sections' 	    => array(
            'button_fonts'          => array(
                'title'                 => __('Button', 'woopack'),
                'fields'                => array(
                    'button_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Button Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-product-add-to-cart a.button,
                                                    .woopack-product-add-to-cart a.button.alt,
                                                    .woopack-product-add-to-cart a.added_to_cart,
                                                    .woopack-product-add-to-cart button,
                                                    .woopack-product-add-to-cart .button,
                                                    .woopack-product-add-to-cart button.button,
                                                    .woopack-product-add-to-cart button.alt,
                                                    .woopack-product-add-to-cart .button.alt,
                                                    .woopack-product-add-to-cart button.button.alt',
						),
					),
                ),
            ),
            'ragular_price_fonts'   => array(
				'title'                 => __('Regular Price', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'ragular_price_typography'    => array(
						'type'        	=> 'typography',
						'label'       	=> __( 'Ragular Price Typography', 'woopack' ),
						'responsive'  	=> true,
						'preview'		=> array(
							'type'			=> 'css',
							'selector'		=> '.product .amount',
						),
					),
                    'ragular_price_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.product .amount',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'sale_price_fonts'      => array(
				'title'                 => __('Sale Price', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'sale_price_font_size'  => array(
                        'type'                  => 'unit',
                        'label'                 => __('Font Size', 'woopack'),
                        'slider'    			=> true,
                        'units'                 => array( 'px' ),
                        'responsive' 			=> true,
                        'preview'				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.product ins .amount',
                            'property'      		=> 'font-size',
                            'unit'					=> 'px',
                        ),
                    ),
                    'sale_price_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Color', 'woopack'),
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.product ins .amount',
                            'property'              => 'color',
                        ),
                    ),
                ),
            ),
        ),
    ),
));
