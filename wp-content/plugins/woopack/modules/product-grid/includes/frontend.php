<?php
if ( ! isset( $settings->item_html_tag ) ) {
	$settings->item_html_tag = $module->get_item_html_tag();
}
if ( ! isset( $settings->layout_style ) ) {
	$settings->layout_style = $module->get_layout_style();
}
if ( ! isset( $settings->woopack ) ) {
	$settings->woopack = true;
}
if ( ! isset( $settings->woopack_post_id ) ) {
	$settings->woopack_post_id = get_the_ID();
}

if ( isset( $settings->product_source ) && 'custom_query' === $settings->data_source ) {
	if ( 'featured' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10, 1 );
	}
	if ( 'best_selling' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10, 1 );
	}
	if ( 'sale' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10, 1 );
	}
	if ( 'top_rated' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10, 1 );
	}
	if ( 'related' === $settings->product_source ) {
		WooPack_Helper::$settings = $settings;
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::related_products', 10, 1 );
	}
}

if ( isset( $_GET['orderby'] ) && ! empty( $_GET['orderby'] ) ) {
	$orderby_value = wc_clean( (string) $_GET['orderby'] );
	// Get order + orderby args from string
	$orderby_value = explode( '-', $orderby_value );
	$orderby       = esc_attr( $orderby_value[0] );
	$order         = ! empty( $orderby_value[1] ) ? $orderby_value[1] : '';
	WooPack_Helper::$order = $order;

	if ( is_callable( 'WooPack_Helper::order_by_' . $orderby ) ) {
		add_filter( 'pre_get_posts', 'WooPack_Helper::order_by_' . $orderby, 10, 1 );
	}
}

// Get the query data.
$query = FLBuilderLoop::query( $settings );

// Render the posts.
if ( $query->have_posts() ) :

	do_action( 'woopack_before_products_grid', $settings, $query );

	$paged = ( FLBuilderLoop::get_paged() > 0 ) ? ' woopack-paged-scroll-to' : '';

	if ( function_exists( 'is_product' ) && ! is_product() ) {
		do_action( 'woocommerce_before_shop_loop' );
	}
	?>
	<div class="woopack-grid-wrap">
		<?php
		if ( 'yes' === $settings->enable_filter && 'acf_relationship' != $settings->data_source ) {
			include $module->dir . 'includes/post-filters.php';
		}
		?>
		<div id="woopack-<?php echo $id; ?>" class="woopack-products-grid-wrap<?php echo 'yes' === $settings->enable_filter ? ' woopack-filters-enabled' : ''; ?> woocommerce">
			<ul class="woopack-products products">
				<?php
				while ( $query->have_posts() ) {

					$query->the_post();

					$product = wc_get_product( get_the_ID() );

					if ( is_object( $product ) && $product->is_visible() ) {
						$product_data = $product->get_data();
						//include apply_filters( 'woopack_products_grid_layout_path', WOOPACK_DIR . 'templates/product-layout-' . $module->settings->product_layout . '.php', $settings );
						include apply_filters( 'woopack_products_grid_layout_path', WooPack_Helper::get_template_file( 'product-layout-' . $settings->product_layout, $settings, $product, 1 ), $settings, $product );
					}
				}
				?>
				<li class="woopack-grid-sizer"></li>
			</ul>

		</div>
	</div>

	<?php
	if ( function_exists( 'is_product' ) && ! is_product() ) {
		if ( function_exists( 'woocommerce_pagination' ) ) {
			remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
		}

		do_action( 'woocommerce_after_shop_loop' );
	}
	do_action( 'woopack_after_products_grid', $settings, $query );
endif;

// Render the pagination.
if ( 'none' != $settings->pagination && $query->have_posts() ) :

?>
<div class="fl-builder-pagination"<?php echo ( 'scroll' === $settings->pagination ) ? ' style="display: none;"' : ''; ?>>
	<?php
	if ( 'yes' === $settings->enable_filter && 'dynamic' === $settings->filter_type && 'numbers' === $settings->pagination ) {
		WooPack_Helper::pagination( $query );
	} else {
		FLBuilderLoop::pagination( $query );
	}
	?>
</div>
<?php endif; ?>
<?php

do_action( 'woopack_after_products_grid_pagination', $settings, $query );

// Render the empty message.
if ( ! $query->have_posts() ) :

?>
<div class="woopack-grid-empty">
	<p><?php echo $settings->no_results_message; ?></p>
	<?php if ( $settings->show_search ) : ?>
		<?php get_search_form(); ?>
	<?php endif; ?>
</div>

<?php

endif;

wp_reset_postdata();

if ( isset( $settings->product_source ) && 'custom_query' === $settings->data_source ) {
	if ( 'featured' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10 );
	}
	if ( 'best_selling' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10 );
	}
	if ( 'sale' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10 );
	}
	if ( 'top_rated' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10 );
	}
	if ( 'related' === $settings->product_source ) {
		WooPack_Helper::$settings = null;
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::related_products', 10 );
	}
}
if ( isset( $_GET['orderby'] ) && ! empty( $_GET['orderby'] ) ) {
	WooPack_Helper::$order = '';
	remove_filter( 'pre_get_posts', 'WooPack_Helper::order_by_' . $orderby, 10 );
}
?>
