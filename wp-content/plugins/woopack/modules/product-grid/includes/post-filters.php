<?php
$post_type          = $settings->post_type;
$filter_tax         = $settings->filter_taxonomy;
$filter_field       = 'tax_' . $post_type . '_' . $filter_tax;
$filter_value       = $settings->{$filter_field};
$filter_matching    = $settings->{$filter_field . '_matching'};
$filter_terms       = array();
$taxonomy           = get_taxonomy( $filter_tax );
$filter_all_label   = (isset($settings->filter_all_label) && ! empty($settings->filter_all_label)) ? $settings->filter_all_label : esc_html__( 'All', 'woopack' );

if ( $filter_value ) {
    $filter_term_ids = explode( ",", $filter_value );
    if ( ! $filter_matching ) {
        $filter_terms = get_terms( $filter_tax, array( 'exclude' => $filter_term_ids ) );
    } else {
        foreach ( $filter_term_ids as $filter_term_id ) {
			$filter_terms[] = get_term_by( 'id', $filter_term_id, $filter_tax );
		}
    }
}

$terms = ( count( $filter_terms ) > 0 ) ? $filter_terms : get_terms( $filter_tax );
$count = count( $terms );

?>

<?php do_action( 'woopack_before_products_grid_filters_wrap' ); ?>

<div class="woopack-product-filters-wrap">
    <div class="woopack-product-filters-toggle">
        <span class="toggle-text"><?php echo $filter_all_label; ?></span>
    </div>
    <ul class="woopack-product-filters">
        <li class="woopack-product-filter woopack-filter-active" data-filter="*" tabindex="0"><?php echo $filter_all_label; ?></li>
        <?php
        if ( $count > 0 ) {
			foreach ( $terms as $term ) {
				$slug = $term->slug;
				echo '<li class="woopack-product-filter" data-filter=".' . $taxonomy->name . '-' . $slug . '" data-term="'.$slug.'" tabindex="0">' . $term->name . '</li>';
			}
		}
        ?>
    </ul>
</div>

<?php do_action( 'woopack_after_products_grid_filters_wrap' ); ?>
