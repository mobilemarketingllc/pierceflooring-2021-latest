;(function($){
	<?php
	if ( ! isset( $settings->item_html_tag ) ) {
		$settings->item_html_tag = $module->get_item_html_tag();
	}
	if ( ! isset( $settings->layout_style ) ) {
		$settings->layout_style = $module->get_layout_style();
	}
	$columns_desktop = empty( $settings->product_columns ) ? 3 : intval( $settings->product_columns );
	$columns_medium = empty( $settings->product_columns_medium ) ? $columns_desktop : intval( $settings->product_columns_medium );
	$columns_responsive = empty( $settings->product_columns_responsive ) ? $columns_medium : intval( $settings->product_columns_responsive );
	?>

    var WooPackGridOptions = {
        id: '<?php echo $id ?>',
		layout: '<?php echo $settings->product_layout; ?>',
		layoutStyle: <?php echo $settings->product_layout; ?>,
		isCustomLayout: <?php echo isset( $settings->layout ) && 'custom' === $settings->layout ? 'true' : 'false'; ?>,
		columns: {
			desktop: <?php echo $columns_desktop; ?>,
			medium: <?php echo $columns_medium; ?>,
			responsive: <?php echo $columns_responsive; ?>,
		},
		pagination: '<?php echo $settings->pagination; ?>',
        perPage: '<?php echo $settings->posts_per_page; ?>',
		matchHeight: '<?php echo $settings->match_height; ?>',
        filters: '<?php echo $settings->enable_filter; ?>',
        filterTax: '<?php echo $settings->filter_taxonomy; ?>',
        filterType: '<?php echo $settings->filter_type; ?>',
        fields: <?php echo json_encode($settings); ?>,
		template: '<?php echo isset( $settings->quick_view_template ) ? $settings->quick_view_template : ''; ?>',
		imagesSlider: <?php echo isset( $settings->image_slider ) && 'yes' === $settings->image_slider ? 'true' : 'false'; ?>,
		siteUrl: '<?php echo site_url(); ?>'
    };

    <?php if ( isset( $_GET['orderby'] ) && ! empty( $_GET['orderby'] ) ) { ?>
    WooPackGridOptions.orderby = '<?php echo wc_clean( (string) $_GET['orderby'] ); ?>';
    <?php } ?>

    woopack_<?php echo $id; ?> = new WooPackGrid( WooPackGridOptions );

	// Layout fix in PowerPack Advanced Tabs module
	$(document).on('pp-tabs-switched', function(e, selector) {
		if ( selector.find('.woopack-grid-wrap').length > 0 ) {
			var wrap = selector.find('#woopack-<?php echo $id; ?> .woopack-products');
			if ( 'undefined' !== typeof $.fn.isotope && 'undefined' !== typeof $.fn.imagesLoaded ) {
				setTimeout(function() {
					if ( ! wrap.hasClass('woopack-isotope-initialized') ) {
						wrap.imagesLoaded(function() {
							var hasSlider = false;
							if ( wrap.find( '.slick-slider' ).length > 0 ) {
								hasSlider = true;
								wrap.find( '.slick-slider' ).slick('unslick');
							}
							if ( ! wrap.data('isotope') ) {
								wrap.isotope( woopack_<?php echo $id; ?>.isotopeData );
							}
							if ( hasSlider ) {
								woopack_<?php echo $id; ?>._initSlider();
							}

							wrap.isotope('layout');
							wrap.addClass('woopack-isotope-initialized');
						});
					}
				}, 500);
			}
		}
	});

})(jQuery);
