(function($) {

	WooPackCarousel = function(settings)
	{
		this.settings       = settings;
		this.nodeClass      = '.fl-node-' + settings.id;
		this.wrapperClass   = this.nodeClass + ' .woopack-products-carousel';
		this.postClass      = this.wrapperClass + ' .woopack-product-carousel';
		this.matchHeight	= settings.matchHeight == 'yes' ? true : false;
		this.layoutStyle	= settings.layoutStyle;

		if(this._hasPosts()) {
			this._initLayout();
			//this._matchWidth();
		}
	};

	WooPackCarousel.prototype = {

		settings        : {},
		nodeClass       : '',
		wrapperClass    : '',
		postClass       : '',
		matchHeight		: '',
		layoutStyle		: '',

		_hasPosts: function()
		{
			return $(this.postClass).length > 0;
		},

		_matchWidth:function()
		{
			var wrap = $(this.wrapperClass);

			wrap.imagesLoaded( $.proxy( function() {
				if ( 3 == this.layoutStyle || 4 == this.layoutStyle ) {
				   var img_width = $( this.postClass + ' .woopack-product-image img').attr('width');
					$( this.postClass + ' .woopack-product-content').css({ 'width': 'calc(100% - ' + img_width+ 'px)' });
			    }
			}, this ) );
	   },

		_initLayout: function()
		{
			this._carouselLayout();

			$(this.postClass).css('visibility', 'visible');
		},

		_carouselLayout: function()
		{
			var wrap = $(this.nodeClass + ' .owl-carousel');
			wrap.imagesLoaded( $.proxy( function() {
				var owlOptions = {
					onInitialized: $.proxy(this._gridLayoutMatchHeight, this),
					onResized: $.proxy(this._gridLayoutMatchHeight, this),
					onRefreshed: $.proxy(this._gridLayoutMatchHeight, this),
					onLoadedLazy: $.proxy(this._gridLayoutMatchHeight, this),
					mouseDrag: true,
					rtl: $('body').hasClass( 'rtl' ),
				};
				wrap.owlCarousel( $.extend({}, this.settings.carousel, owlOptions) );
			}, this));
		},

		_gridLayoutMatchHeight: function()
		{
			var highestBox = 0;
			var contentHeight = 0;
			if ( false === this.matchHeight ) {
				return;
			}

            $(this.postClass).css('height', '').each(function(){

                if($(this).height() > highestBox) {
                	highestBox = $(this).height();
                	contentHeight = $(this).outerHeight();
                }
            });

            $(this.postClass).height(highestBox);
		},

	};

})(jQuery);
