<?php
// Common
include WOOPACK_DIR . 'includes/product-common.css.php';
// Image
include WOOPACK_DIR . 'includes/product-image.css.php';
// Sale badge
include WOOPACK_DIR . 'includes/product-badge.css.php';
// Quick View
include WOOPACK_DIR . 'includes/product-quick-view.css.php';
// Content, price.
include WOOPACK_DIR . 'includes/product-content.css.php';
// Actions, button
include WOOPACK_DIR . 'includes/product-action.css.php';
?>
.fl-node-<?php echo $id; ?> .woocommerce-result-count {
	display: none;
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
	margin: <?php echo ( $settings->product_spacing / 2 ); ?>% !important;
}

.fl-node-<?php echo $id; ?> .owl-theme .owl-dots .owl-dot span {
	<?php WooPack_Helper::print_css( 'background-color', $settings->owl_pagination_color ); ?>
	opacity: 1;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-dots .owl-dot.active span,
.fl-node-<?php echo $id; ?> .owl-theme .owl-dots .owl-dot:hover span {
	<?php WooPack_Helper::print_css( 'background-color', $settings->owl_active_pagination_color ); ?>
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav button {
	line-height: 0;
	box-shadow: none;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav button:hover {
	background: none;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav button svg {
	<?php WooPack_Helper::print_css( 'color', $settings->owl_arrow_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->owl_arrow_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'border-radius', $settings->owl_arrow_radius, '%' ); ?>
	width: 38px;
	height: 38px;
	opacity: 1;
	padding: 3px 13px;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav button:hover svg {
	<?php WooPack_Helper::print_css( 'color', $settings->owl_arrow_hover_color, '', isset( $settings->owl_arrow_hover_color ) ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->owl_arrow_bg_hover_color, '', isset( $settings->owl_arrow_bg_hover_color ) ); ?>
	transition: all 0.2s ease;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav .owl-prev,
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav .owl-next {
	position: absolute;
	top: 50%;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav .owl-prev {
	left: -22px;
}
.fl-node-<?php echo $id; ?> .owl-theme .owl-nav .owl-next {
	right: -22px;
}
<?php if ( 3 == $settings->product_layout || 4 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-wrapper {
		display: flex;
		justify-content: space-evenly;
		flex-direction: row;
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-wrapper .woopack-product-image {
		width: 30%
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-content {
		width: 70%;
	}
<?php } ?>

@media only screen and (max-width: 767px) {
	.fl-node-<?php echo $id; ?> .owl-theme .owl-nav .owl-prev,
	.fl-node-<?php echo $id; ?> .owl-theme .owl-nav .owl-next {
		position: static;
	}
}