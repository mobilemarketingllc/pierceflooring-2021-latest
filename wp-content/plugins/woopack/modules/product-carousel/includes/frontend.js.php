;(function($){
	<?php
	$product_columns 			= !empty( $settings->product_columns ) ? $settings->product_columns : '3' ;
	$product_columns_medium 	= !empty( $settings->product_columns_medium ) ? $settings->product_columns_medium : '2' ;
	$product_columns_responsive	= !empty( $settings->product_columns_responsive ) ? $settings->product_columns_responsive : '1' ;
	$slideDuration				= !empty( $settings->slider_duration ) ? $settings->slider_duration * 1000 : '5000';
	$speed     					= !empty( $settings->transition_duration ) ? $settings->transition_duration * 1000 : '1000';
	?>
	var left_arrow_svg = '<svg aria-hidden="true" data-prefix="fal" data-icon="angle-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" class="svg-inline--fa fa-angle-left fa-w-6 fa-2x"><path fill="currentColor" d="M25.1 247.5l117.8-116c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L64.7 256l102.2 100.4c4.7 4.7 4.7 12.3 0 17l-7.1 7.1c-4.7 4.7-12.3 4.7-17 0L25 264.5c-4.6-4.7-4.6-12.3.1-17z" class=""></path></svg>';
	var right_arrow_svg = '<svg aria-hidden="true" data-prefix="fal" data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 192 512" class="svg-inline--fa fa-angle-right fa-w-6 fa-2x"><path fill="currentColor" d="M166.9 264.5l-117.8 116c-4.7 4.7-12.3 4.7-17 0l-7.1-7.1c-4.7-4.7-4.7-12.3 0-17L127.3 256 25.1 155.6c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0l117.8 116c4.6 4.7 4.6 12.3-.1 17z" class=""></path></svg>';

	new WooPackCarousel({
		id: '<?php echo $id ?>',
		layout: 'carousel',
		layoutStyle: <?php echo $settings->product_layout; ?>,
		<?php echo ('dots' == $settings->owl_pagination) ? 'pagination: true' : 'pagination: false'; ?>,
		postSpacing: '<?php echo $settings->product_spacing; ?>',
		postColumns: {
			desktop: <?php echo $product_columns; ?>,
			tablet: <?php echo $product_columns_medium ?>,
			mobile: <?php echo $product_columns_responsive ?>,
		},
		matchHeight: '<?php echo $settings->match_height; ?>',
		carousel: {
			items: <?php echo $product_columns; ?>,
			responsive: {
				0: {
					items: <?php echo $product_columns_responsive; ?>,
				},
				768: {
					items: <?php echo $product_columns_medium; ?>,
				},
				980: {
					items: <?php echo $product_columns; ?>,
				},
				1199: {
					items: <?php echo $product_columns; ?>,
				},
			},
			<?php echo 'dots' == $settings->owl_pagination ? 'dots: true' : 'dots: false'; ?>,
			<?php if( isset( $settings->auto_play ) && 'yes' == $settings->auto_play ) { ?>
				autoplay: true,
				autoplayTimeout: <?php echo $slideDuration; ?>,
		        autoplaySpeed: <?php echo $speed; ?>,
			<?php } else { ?>
				autoplay: false,
			<?php } ?>
			<?php echo ($settings->stop_on_hover == 'yes') ? 'autoplayHoverPause: true' : 'autoplayHoverPause: false'; ?>,
			<?php if ( 'scrollPerProduct' == $settings->scroll_as ) { ?>
			slideBy: 1,
			<?php } ?>
			<?php if ( 'scrollPerPage' == $settings->scroll_as ) { ?>
			slideBy: 'page',
			<?php } ?>
			navSpeed: <?php echo $speed; ?>,
			dotsSpeed: <?php echo $speed; ?>,
			<?php echo 'arrows' == $settings->owl_pagination ? 'nav: true' : 'nav: false'; ?>,
			<?php echo ($settings->lazy_load == 'yes') ? 'lazyLoad: true' : 'lazyLoad: false'; ?>,
			navText : [left_arrow_svg, right_arrow_svg],
			responsiveRefreshRate : 200,
			responsiveBaseWidth: window,
			loop: <?php echo $settings->carousel_loop; ?>,
			rewind: false,
		}
	});

})(jQuery);
