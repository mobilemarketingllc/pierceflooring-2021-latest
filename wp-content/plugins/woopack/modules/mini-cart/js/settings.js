(function($) {

	FLBuilder.registerModuleHelper('mini-cart', {
		rules: '',

		init: function()
		{
			var form = $('.fl-builder-settings'),
				node = form.data('node');

			if ( 'undefined' !== typeof window['woopack_' + node] ) {
				window['woopack_' + node]._renderPreview();
			}
		}
	});

})(jQuery);