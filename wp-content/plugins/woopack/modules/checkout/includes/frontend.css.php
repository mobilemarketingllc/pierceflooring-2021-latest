<?php
// *************************************** Border ***************************************
// Cart Input Border - Settings
$input_classes 	= ".fl-node-$id .woocommerce form .form-row input.input-text,
					.fl-node-$id .woocommerce form .form-row input.input-text:focus,
					.fl-node-$id .woocommerce form .form-row textarea,
					.fl-node-$id .woocommerce form .form-row textarea:focus,
					.fl-node-$id .woocommerce form .form-row select,
					.fl-node-$id .woocommerce .select2-container--default .select2-selection--single,
					.fl-node-$id .woocommerce .select2-container--default .select2-selection--single:focus,
					.fl-node-$id .woocommerce form .form-row.woocommerce-validated .select2-container--default .select2-selection--single,
					.fl-node-$id .woocommerce form .form-row.woocommerce-validated input.input-text";

if ( 'inset' ==  $settings->inpur_shadow_direction ) {
	$devices = array( '', '_medium', '_responsive' );
	foreach ( $devices as $device ) {
		$input 			= 'input_border_group' . $device;
		$border_h		= $settings->$input['shadow']['horizontal'];
		$border_v		= $settings->$input['shadow']['vertical'];
		$border_b		= $settings->$input['shadow']['blur'];
		$border_s		= $settings->$input['shadow']['spread'];
		$border_c		= $settings->$input['shadow']['color'];

		if ( is_array( $settings->$input ) && isset( $settings->$input['shadow'] ) ) {
			unset( $settings->$input['shadow'] );
			if ( '_medium' == $device ) { ?>
				@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
			<?php } else if ( '_responsive' == $device ) { ?>
				@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
			<?php }
				echo $input_classes; ?> {
					-webkit-box-shadow: inset <?php echo $border_h; ?>px <?php echo $border_v; ?>px <?php echo $border_b; ?>px <?php echo $border_s; ?>px <?php echo WooPack_Helper::get_color_value( $border_c ); ?>;
					-moz-box-shadow: inset <?php echo $border_h; ?>px <?php echo $border_v; ?>px <?php echo $border_b; ?>px <?php echo $border_s; ?>px <?php echo WooPack_Helper::get_color_value( $border_c ); ?>;
					-o-box-shadow: inset <?php echo $border_h; ?>px <?php echo $border_v; ?>px <?php echo $border_b; ?>px <?php echo $border_s; ?>px <?php echo WooPack_Helper::get_color_value( $border_c ); ?>;
					box-shadow: inset <?php echo $border_h; ?>px <?php echo $border_v; ?>px <?php echo $border_b; ?>px <?php echo $border_s; ?>px <?php echo WooPack_Helper::get_color_value( $border_c ); ?>;
				}
	<?php	if ( '_medium' == $device || '_responsive' ==  $device ) { ?>
				}
	<?php	}
		}
	}
	FLBuilderCSS::border_field_rule( array(
		'settings' 		=> $settings,
		'setting_name' 	=> 'input_border_group',
		'selector' 		=> $input_classes,
	) );
} else {
	FLBuilderCSS::border_field_rule( array(
		'settings' 		=> $settings,
		'setting_name' 	=> 'input_border_group',
		'selector' 		=> $input_classes,
	) );
}

// Cart Table Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'form_border_group',
	'selector' 		=> ".fl-node-$id #customer_details,
						.fl-node-$id .woocommerce table.shop_table,
						.fl-node-$id .woocommerce #payment,
						.fl-node-$id .woocommerce .woocommerce-info,
						.fl-node-$id .woocommerce .checkout_coupon",
) );
// Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-checkout input.button,
						.fl-node-$id .woocommerce .woocommerce-checkout #place_order",
) );
// Border - Hover Settings
if ( ! empty( $settings->button_border_color_hover ) && is_array( $settings->button_border_group ) ) {
	$settings->button_border_group['color'] = $settings->button_border_color_hover;
}

FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-checkout input.button:hover,
						.fl-node-$id .woocommerce .woocommerce-checkout #place_order:hover",
) );

// ******************* Padding *******************
// Input Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'input_padding',
	'selector' 		=> $input_classes,
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'input_padding_top',
		'padding-right' 	=> 'input_padding_right',
		'padding-bottom' 	=> 'input_padding_bottom',
		'padding-left' 		=> 'input_padding_left',
	),
) );
// Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-checkout input.button,
						.fl-node-$id .woocommerce .woocommerce-checkout #place_order",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );
// *************************************** Typography ***************************************
// Button Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-checkout input.button,
						.fl-node-$id .woocommerce .woocommerce-checkout #place_order",
) );

// Form Header Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'form_header_typography',
	'selector' 		=> ".fl-node-$id #customer_details h3,
						.fl-node-$id #customer_details h3 label",
) );

// Form Label Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'form_label_typography',
	'selector' 		=> ".fl-node-$id .woocommerce form .form-row label",
) );

// Table Title Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'table_title_typography',
	'selector' 		=> ".fl-node-$id .woocommerce #order_review_heading",
) );

// Table Header Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'table_header_typography',
	'selector' 		=> ".fl-node-$id .woocommerce table.shop_table thead th",
) );

// Cart Item Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'cart_item_typography',
	'selector' 		=> ".fl-node-$id .woocommerce table.shop_table td,
						.fl-node-$id .woocommerce table.shop_table tfoot td,
						.fl-node-$id .woocommerce table.shop_table tfoot th",
) );

// Coupon Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'coupon_typography',
	'selector' 		=> ".fl-node-$id .woocommerce .woocommerce-info",
) );

// Payment Method Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'payment_method_typography',
	'selector' 		=> ".fl-node-$id .woocommerce #payment ul.payment_methods li",
) );
?>
.fl-node-<?php echo $id; ?> #customer_details,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table,
.fl-node-<?php echo $id; ?> .woocommerce #payment,
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info,
.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon {
	<?php if ( '' == $settings->form_bg_color ) { ?>
		background: none;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->form_bg_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->form_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->form_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->form_padding_right_left, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->form_padding_right_left, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space, 'px' ); ?>
	margin-top: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce-NoticeGroup {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> #order_review .shop_table {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space, 'px !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment {
	margin-bottom: 0px;
}
.fl-node-<?php echo $id; ?> #customer_details h3,
.fl-node-<?php echo $id; ?> #customer_details h3 label {
	<?php WooPack_Helper::print_css( 'color', $settings->form_header_color ); ?>
}
.fl-node-<?php echo $id; ?> #ship-to-different-address-checkbox {
	margin-left: 0;
}
.fl-node-<?php echo $id; ?> #ship-to-different-address span {
	margin-left: 20px;
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row label {
	<?php WooPack_Helper::print_css( 'color', $settings->form_label_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid label {
	<?php WooPack_Helper::print_css( 'color', $settings->form_label_invalid_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #order_review {
	background: transparent;
	padding: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_item:nth-child(even),
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot tr:nth-child(even) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_even_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .cart_item:nth-child(odd),
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot tr:nth-child(odd) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_odd_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table thead th {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-bottom-style', 'solid', '', '' != $settings->table_header_border_width ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->table_header_border_color, '!important', '' != $settings->table_header_border_width ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->table_header_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table td,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td,
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot th {
	<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'border-top-style', 'solid', '', '' != $settings->cart_item_border_width ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_item_border_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->table_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tbody tr:first-child td {
	border-top-width: 0 !important;
}
.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td {
	font-weight: 600;
}
.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading {
	<?php WooPack_Helper::print_css( 'color', $settings->table_title_color ); ?>
}

<?php
// *********************
// Inputs
// *********************
?>
<?php echo $input_classes; ?> {
	<?php WooPack_Helper::print_css( 'color', $settings->input_text_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->input_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'text-align-last', $settings->input_text_align ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->input_margin_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->input_line_height ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid input.input-text,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row.woocommerce-invalid .select2-container--default .select2-selection--single {
	<?php WooPack_Helper::print_css( 'border-color', $settings->input_border_invalid ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text:focus,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea:focus {
	<?php WooPack_Helper::print_css( 'border-color', $settings->input_border_focus, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row select,
.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single {
	<?php WooPack_Helper::print_css( 'height', $settings->input_height, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea {
	<?php WooPack_Helper::print_css( 'height', $settings->textarea_height, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text::-webkit-input-placeholder,
.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea::-webkit-input-placeholder {
	<?php WooPack_Helper::print_css( 'color', $settings->placeholder_color ); ?>
}

<?php
// *********************
// Button
// *********************
?>
.fl-node-<?php echo $id; ?> .woocommerce input.button,
.fl-node-<?php echo $id; ?> .woocommerce #place_order {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
	<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%', 'custom' == $settings->button_width ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment #place_order {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>
	float: none;
}
.fl-node-<?php echo $id; ?> .woocommerce input.button:hover,
.fl-node-<?php echo $id; ?> .woocommerce #place_order:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>
	transition: all 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .woocommerce #payment .form-row.place-order,
.fl-node-<?php echo $id; ?> .woocommerce-page #payment .form-row.place-order {
	<?php WooPack_Helper::print_css( 'text-align', $settings->button_alignment ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-error {
	<?php WooPack_Helper::print_css( 'background-color', $settings->error_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->error_text_color ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->error_border_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info {
	<?php WooPack_Helper::print_css( 'color', $settings->coupon_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info a {
	<?php WooPack_Helper::print_css( 'color', $settings->coupon_link_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment ul.payment_methods label {
	<?php WooPack_Helper::print_css( 'color', $settings->payment_method_label_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box {
	<?php WooPack_Helper::print_css( 'background', $settings->payment_method_box_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'padding-top', $settings->payment_method_box_top_bottom_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->payment_method_box_top_bottom_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->payment_method_box_right_left_padding, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->payment_method_box_right_left_padding, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box::before {
	<?php WooPack_Helper::print_css( 'border-color', $settings->payment_method_box_bg_color ); ?>
	border-right-color: transparent;
	border-left-color: transparent;
	border-top-color: transparent;
}
.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box p {
	<?php WooPack_Helper::print_css( 'color', $settings->payment_method_box_text_color ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_msg_font_size, 'px', 'custom' == $settings->payment_method_msg_font_size ); ?>
}

<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> #customer_details,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table,
	.fl-node-<?php echo $id; ?> .woocommerce #payment,
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info,
	.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->form_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->form_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->form_padding_right_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->form_padding_right_left_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_medium, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce-NoticeGroup {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> #order_review .shop_table {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_medium, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_medium, 'px' ); ?>
	}

	<?php echo $input_classes; ?> {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->input_margin_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->input_line_height_medium ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single {
		<?php WooPack_Helper::print_css( 'height', $settings->input_height_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea {
		<?php WooPack_Helper::print_css( 'height', $settings->textarea_height_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment #place_order {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->payment_method_box_top_bottom_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->payment_method_box_top_bottom_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->payment_method_box_right_left_padding_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->payment_method_box_right_left_padding_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box p {
		<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_msg_font_size_medium, 'px', 'custom' == $settings->payment_method_msg_font_size ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> #customer_details,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table,
	.fl-node-<?php echo $id; ?> .woocommerce #payment,
	.fl-node-<?php echo $id; ?> .woocommerce .woocommerce-info,
	.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->form_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->form_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->form_padding_right_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->form_padding_right_left_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_responsive, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce-NoticeGroup {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> #order_review .shop_table {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->form_space_responsive, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table thead th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->table_header_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->table_header_border_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot td,
	.fl-node-<?php echo $id; ?> .woocommerce table.shop_table tfoot th {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->cart_item_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'border-top-width', $settings->cart_item_border_width_responsive, 'px' ); ?>
	}

	<?php echo $input_classes; ?> {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->input_margin_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->input_line_height_responsive ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce form .form-row input.input-text,
	.fl-node-<?php echo $id; ?> .woocommerce .select2-container--default .select2-selection--single {
		<?php WooPack_Helper::print_css( 'height', $settings->input_height_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce form .form-row textarea {
		<?php WooPack_Helper::print_css( 'height', $settings->textarea_height_responsive, 'px' ); ?>
	}

	.fl-node-<?php echo $id; ?> .woocommerce #payment #place_order {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->payment_method_box_top_bottom_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->payment_method_box_top_bottom_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->payment_method_box_right_left_padding_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->payment_method_box_right_left_padding_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce #payment div.payment_box p {
		<?php WooPack_Helper::print_css( 'font-size', $settings->payment_method_msg_font_size_responsive, 'px', 'custom' == $settings->payment_method_msg_font_size ); ?>
	}
}

<?php if ( '2column' == $settings->checkout_layout ) {?>
	.fl-node-<?php echo $id; ?> .woocommerce .checkout_coupon .form-row,
	.fl-node-<?php echo $id; ?> .woocommerce #coupon_code {
		margin-bottom: 0;
	}
	.fl-node-<?php echo $id; ?> #customer_details {
		<?php WooPack_Helper::print_css( 'width', $settings->first_column_width, '%' ); ?>
		<?php WooPack_Helper::print_css( 'margin-right', $settings->space_bw_columns, '%' ); ?>
		float: left;
	}
	.fl-node-<?php echo $id; ?> #customer_details .col-1,
	.fl-node-<?php echo $id; ?> #customer_details .col-2 {
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading {
		<?php WooPack_Helper::print_css( 'width', $settings->second_column_width, '%' ); ?>
		float: left;
		margin-top: 0;
		margin-bottom: 10px;
		display: none;
	}
	.fl-node-<?php echo $id; ?> #order_review {
		<?php WooPack_Helper::print_css( 'width', $settings->second_column_width, '%' ); ?>
		float: left;
	}

	@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
		.fl-node-<?php echo $id; ?> #customer_details {
			<?php WooPack_Helper::print_css( 'width', $settings->first_column_width_medium, '%' ); ?>
			<?php WooPack_Helper::print_css( 'margin-right', $settings->space_bw_columns_medium, '%' ); ?>
		}
		.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading,
		.fl-node-<?php echo $id; ?> #order_review {
			<?php WooPack_Helper::print_css( 'width', $settings->second_column_width_medium, '%' ); ?>
		}
	}

	@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
		.fl-node-<?php echo $id; ?> #customer_details {
			<?php WooPack_Helper::print_css( 'width', $settings->first_column_width_responsive, '%' ); ?>
			<?php WooPack_Helper::print_css( 'margin-right', $settings->space_bw_columns_responsive, '%' ); ?>
		}
		.fl-node-<?php echo $id; ?> .woocommerce #order_review_heading,
		.fl-node-<?php echo $id; ?> #order_review {
			<?php WooPack_Helper::print_css( 'width', $settings->second_column_width_responsive, '%' ); ?>
		}
	}
<?php } // End if().?>
