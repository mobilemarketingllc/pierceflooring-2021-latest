<?php do_action( 'woopack_before_checkout_wrap', $settings ); ?>

<div class="woopack-product-checkout">
	<?php do_action( 'woopack_before_checkout_content', $settings ); ?>
    <?php echo do_shortcode('[woocommerce_checkout]'); ?>
	<?php do_action( 'woopack_after_checkout_content', $settings ); ?>
</div>

<?php do_action( 'woopack_after_checkout_wrap', $settings ); ?>