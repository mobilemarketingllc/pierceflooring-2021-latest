<?php
/**
* @class WooPackCheckout
*/
class WooPackCheckout extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Checkout', 'woopack'),
            'description' 		=> __('Addon to display Checkout.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/checkout/',
            'url' 				=> WOOPACK_URL . 'modules/checkout/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
		));
	}
	public function filter_settings( $settings, $helper ) {
		// Handle old Button Settings.
		$settings = filter_product_button_settings( $settings );

		// Handle old Form Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'form_border_style'		=> array(
				'type'					=> 'style'
			),
			'form_border_width'		=> array(
				'type'					=> 'width'
			),
			'form_border_color'		=> array(
				'type'					=> 'color'
			),
			'form_border_radius'	=> array(
				'type'					=> 'radius'
			),
			'form_shadow_h'      	=> array(
				'type'              	=> 'shadow_horizontal',
				'condition'         	=> isset( $settings->form_shadow ) ? 'yes' == $settings->form_shadow : false
			),
			'form_shadow_v'      	=> array(
				'type'              	=> 'shadow_vertical',
				'condition'         	=> isset( $settings->form_shadow ) ? 'yes' == $settings->form_shadow : false
			),
			'form_shadow_blur'   	=> array(
				'type'              	=> 'shadow_blur',
				'condition'         	=> isset( $settings->form_shadow ) ? 'yes' == $settings->form_shadow : false
			),
			'form_shadow_spread'	=> array(
				'type'              	=> 'shadow_spread',
				'condition'         	=> isset( $settings->form_shadow ) ? 'yes' == $settings->form_shadow : false
			),
			'form_shadow_color'		=> array(
				'type'              	=> 'shadow_color',
				'condition'         	=> isset( $settings->form_shadow ) ? 'yes' == $settings->form_shadow : false
			)
		), 'form_border_group' );

		// Handle old Input Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'input_border_style'	=> array(
				'type'					=> 'style'
			),
			'input_border_width'	=> array(
				'type'					=> 'width'
			),
			'input_border_color'	=> array(
				'type'					=> 'color'
			),
			'input_border_radius'	=> array(
				'type'					=> 'radius'
			),
            'inpur_shadow_h'      	=> array(
                'type'              	=> 'shadow_horizontal',
                'condition'         	=> isset( $settings->inpur_shadow ) ? 'yes' == $settings->inpur_shadow : false
            ),
            'inpur_shadow_v'      	=> array(
                'type'              	=> 'shadow_vertical',
                'condition'         	=> isset( $settings->inpur_shadow ) ? 'yes' == $settings->inpur_shadow : false
            ),
            'inpur_shadow_blur'   	=> array(
                'type'              	=> 'shadow_blur',
                'condition'         	=> isset( $settings->inpur_shadow ) ? 'yes' == $settings->inpur_shadow : false
            ),
            'inpur_shadow_spread'	=> array(
                'type'              	=> 'shadow_spread',
                'condition'         	=> isset( $settings->inpur_shadow ) ? 'yes' == $settings->inpur_shadow : false
            ),
            'inpur_shadow_color'	=> array(
                'type'              	=> 'shadow_color',
                'condition'         	=> isset( $settings->inpur_shadow ) ? 'yes' == $settings->inpur_shadow : false
            )
		), 'input_border_group' );

		// Handle old Form Header Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'form_header_font'   			=> array(
				'type'          				=> 'font'
			),
			'form_header_font_size_custom'	=> array(
				'type'          				=> 'font_size',
				'condition'     				=> ( isset( $settings->form_header_font_size ) && 'custom' == $settings->form_header_font_size )
			),
			'form_header_text_transform'	=> array(
				'type'          				=> 'text_transform',
			),
			'form_header_line_height'		=> array(
				'type'          				=> 'line_height',
			),
			'form_header_align'				=> array(
				'type'          				=> 'text_align',
			),
		), 'form_header_typography' );

		// Handle old Form Header Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'form_label_font'   			=> array(
				'type'          				=> 'font'
			),
			'form_label_font_size_custom'	=> array(
				'type'          				=> 'font_size',
				'condition'     				=> ( isset( $settings->form_label_font_size ) && 'custom' == $settings->form_label_font_size )
			),
			'form_label_text_transform'		=> array(
				'type'          				=> 'text_transform',
			),
			'form_label_line_height'		=> array(
				'type'          				=> 'line_height',
			),
			'form_label_align'				=> array(
				'type'          				=> 'text_align',
			),
		), 'form_label_typography' );

		// Handle old Table Title Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'table_title_font'   			=> array(
				'type'          				=> 'font'
			),
			'table_title_font_size_custom'	=> array(
				'type'          				=> 'font_size',
				'condition'     				=> ( isset( $settings->table_title_font_size ) && 'custom' == $settings->table_title_font_size )
			),
			'table_title_text_transform'	=> array(
				'type'          				=> 'text_transform',
			),
			'table_title_line_height'		=> array(
				'type'          				=> 'line_height',
			),
			'table_title_align'				=> array(
				'type'          				=> 'text_align',
			),
		), 'table_title_typography' );

		// Handle old Table Heade Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'table_header_font'   			=> array(
				'type'          				=> 'font'
			),
			'table_header_font_size_custom'	=> array(
				'type'          				=> 'font_size',
				'condition'     				=> ( isset( $settings->table_header_font_size ) && 'custom' == $settings->table_header_font_size )
			),
			'table_header_text_transform'	=> array(
				'type'          				=> 'text_transform',
			),
			'table_header_line_height'		=> array(
				'type'          				=> 'line_height',
			),
		), 'table_header_typography' );

		// Handle old Cart Item Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'table_font'   				=> array(
				'type'          			=> 'font'
			),
			'table_font_size_custom'	=> array(
				'type'          			=> 'font_size',
				'condition'     			=> ( isset( $settings->table_font_size ) && 'custom' == $settings->table_font_size )
			),
			'table_font_line_height'	=> array(
				'type'          			=> 'line_height',
			),
		), 'cart_item_typography' );

		// Handle old Coupons Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'coupon_font'   			=> array(
				'type'          			=> 'font'
			),
			'coupon_font_size_custom'	=> array(
				'type'          			=> 'font_size',
				'condition'     			=> ( isset( $settings->coupon_font_size ) && 'custom' == $settings->coupon_font_size )
			),
			'coupon_font_line_height'	=> array(
				'type'          			=> 'line_height',
			),
		), 'coupon_typography' );

		// Handle old Payment Method Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'payment_method_font'   			=> array(
				'type'          			=> 'font'
			),
			'payment_method_font_size_custom'	=> array(
				'type'          			=> 'font_size',
				'condition'     			=> ( isset( $settings->payment_method_font_size ) && 'custom' == $settings->payment_method_font_size )
			),
			'payment_method_font_line_height'	=> array(
				'type'          			=> 'line_height',
			),
		), 'payment_method_typography' );

		return $settings;
	}

}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackCheckout', array(
    'checkout_sections' => array(
        'title'             => __('Sections', 'woopack'),
        'sections'          => array(
            'layout'            => array(
                'title'             => __('Layout', 'woopack'),
                'fields'            => array(
                    'checkout_layout'       => array(
                        'type'                  => 'select',
                        'label'                 => __('Layout', 'woopack'),
                        'default'               => '1column',
                        'options'               => array(
                            '1column'               => __('One Column', 'woopack'),
                            '2column'               => __('Two Column', 'woopack'),
                        ),
                        'toggle'                => array(
							'2column'                => array(
                                'fields'                    => array( 'first_column_width', 'second_column_width', 'space_bw_columns'),
							),
						),
                    ),
                    'first_column_width'    => array(
                        'type'                  => 'unit',
                        'label' 			    => __('First Column Width', 'woopack'),
						'default'               => '50',
                        'units'					=> array('%'),
						'slider'				=> true,
                        'responsive' 			=> true,
                    ),
                    'second_column_width'   => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Second Column Width', 'woopack'),
                        'default'               => '48',
						'units'					=> array('%'),
						'slider'				=> true,
                        'responsive' 			=> true,
                    ),
                    'space_bw_columns'      => array(
                        'type'                  => 'unit',
                        'label' 			    => __('Space Between Columns', 'woopack'),
                        'default'               => '2',
						'units'					=> array('%'),
						'slider'				=> true,
                        'responsive' 			=> true,
                    ),
                ),
            ),
            'form_color'        => array(
				'title'             => __('Background', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'form_bg_color'     => array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                            'property'          => 'background-color',
                        ),
                    ),
                ),
            ),
            'form_border'       => array(
				'title'             => __('Border', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
					'form_border_group'	=> array(
						'type'					=> 'border',
						'label'					=> __('Border Style', 'woopack'),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> '#customer_details, table.shop_table, #payment, .woocommerce-info',
						),
					),
                ),
            ),
            'form_padding'      => array(
				'title'             => __('Padding', 'woopack'),
				'collapsed'			=> true,
                'fields'        	=> array(
                    'form_padding_top_bottom'=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Top & Bottom Padding', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                    'form_padding_right_left'=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Right & Left Padding', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '#customer_details, table.shop_table, #payment, .woocommerce-info',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                ),
            ),
            'form_spacing'      => array(
				'title'             => __('Spacing', 'woopack'),
				'collapsed'			=> true,
                'fields'        	=> array(
                    'form_space'        => array(
                        'type'                  => 'unit',
                        'label'                 => __('Space Between Sections', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'responsive'            => true,
                    ),
                ),
            ),
        ),
    ),
    'inputs'            => array(
        'title'             => __('Inputs', 'woopack'),
        'sections'          => array(
            'input_color'       => array(
                'title'             => __('Color', 'woopack'),
                'fields'            => array(
                    'input_text_color'	=> array(
                        'type'              => 'color',
                        'label'             => __('Text Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'          => 'color',
                        ),
                    ),
                    'input_bg_color'	=> array(
                        'type'              => 'color',
                        'label'             => __('Background Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'          => 'background-color',
                        ),
                    ),
                    'placeholder_color'	=> array(
                        'type'              => 'color',
                        'label'             => __('Placeholder Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                    ),
                ),
            ),
            'input_border'      => array(
				'title'             => __('Border', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
					'input_border_group'	=> array(
						'type'					=> 'border',
						'label'					=> __('Border Style', 'woopack'),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
						),
					),
                    'input_border_focus'    => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Focus Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => 'form .form-row input.input-text:focus',
                            'property'              => 'border-color',
                        ),
                    ),
                    'input_border_invalid'  => array(
                        'type'                  => 'color',
                        'label' 			    => __('Border Invalid Color', 'woopack'),
                        'show_reset' 		    => true,
                        'show_alpha' 		    => true,
                        'preview' 			    => array(
                            'type' 				    => 'css',
                            'selector'              => '',
                            'property'              => 'border-color',
                        ),
                    ),
                ),
            ),
            'inpur_shadow'      => array(
				'title'             => __('Shadow', 'woopack'),
				'collapsed'			=> true,
                'fields'        	=> array(
                    'inpur_shadow_direction'=> array(
                        'type'          		=> 'select',
                        'label'         		=> __('Shadow Direction', 'woopack'),
                        'default'       		=> 'out',
                        'options'       		=> array(
                            'out'           		=> __('Outside', 'woopack'),
                            'inset'            		=> __('Inside', 'woopack'),
                        ),
                    ),
                ),
            ),
            'input_alignment'   => array(
				'title'             => __('Size & Alignment', 'woopack'),
				'collapsed'			=> true,
                'fields'        	=> array(
                    'input_height'		=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Input Height', 'woopack'),
                        'default'               => '34',
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row select',
                            'property'              => 'height',
                            'unit'                  => 'px',
                        ),
                        'responsive'            => true,
                    ),
                    'textarea_height'	=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Textarea Height', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row textarea',
                            'property'              => 'height',
                            'unit'                  => 'px',
                        ),
                        'responsive'            => true,
                    ),
                    'input_line_height'	=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Line Height', 'woopack'),
						'units'					=> array('em'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row textarea',
                            'property'              => 'line-height',
                        ),
                        'responsive'            => true,
                    ),
                    'input_text_align'	=> array(
                        'type'          		=> 'align',
                        'label'         		=> __('Text Alignment', 'woopack'),
                        'default'       		=> 'left',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'              => 'text-align-last',
                        ),
                    ),
                ),
            ),
            'input_padding'     => array(
				'title'             => __('Padding', 'woopack'),
				'collapsed'			=> true,
                'fields'        	=> array(
                    'input_padding'     	=> array(
                        'type'              		=> 'dimension',
                        'label' 					=> __('Padding', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview' 					=> array(
                            'type' 						=> 'css',
                            'selector'					=> 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'					=> 'padding',
                            'unit' 						=> 'px'
                        ),
                        'responsive' 				=> true,
                    ),
                ),
            ),
            'input_margin'      => array(
				'title'             => __('Margin Bottom', 'woopack'),
				'collapsed'			=> true,
                'fields'        	=> array(
                    'input_margin_bottom'  	=> array(
                        'type'                  	=> 'unit',
                        'label'                 	=> __('Margin Bottom', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview' 					=> array(
                            'type' 						=> 'css',
                            'selector'					=> 'form .form-row input.input-text, form .form-row textarea, form .form-row select',
                            'property'					=> 'margin-bottom',
                            'unit' 						=> 'px'
                        ),
                        'responsive'        		=> true,
                    ),
                ),
            ),
        ),
	),
    'contents'          => array(
        'title'             => __('Content', 'woopack'),
        'sections'          => array(
            'table_header_style'    => array(
                'title'             => __('Cart Heading', 'woopack'),
                'fields'            => array(
                    'table_header_padding'      => array(
                        'type'                      => 'unit',
                        'label' 				    => __('Vertical Spacing', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'          => 'table.shop_table thead th',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => 'table.shop_table thead th',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => true,
                    ),
                    'table_header_border_width' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table thead th',
                            'property'                  => 'border-bottom-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => true,
                    ),
                    'table_header_border_color' => array(
                        'type'                      => 'color',
                        'label'                     => __('Separator Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                    ),
                ),
            ),
            'cart_item_style'       => array(
				'title'             => __('Cart Item', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'cart_item_padding'         => array(
                        'type'                      => 'unit',
                        'label' 				    => __('Vertical Spacing', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'              => 'table.shop_table td, table.shop_table tfoot td, table.shop_table tfoot th',
                                    'property'              => 'padding-top',
                                    'unit'                  => 'px',
                                ),
                                array(
                                    'selector'	         => 'table.shop_table td, table.shop_table tfoot td, table.shop_table tfoot th',
                                    'property'              => 'padding-bottom',
                                    'unit'                  => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => true,
                    ),
                    'cart_item_even_color'      => array(
                        'type'                      => 'color',
                        'label'                     => __('Even Item Background Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_item:nth-child(even), table.shop_table tfoot tr:nth-child(even)',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'cart_item_odd_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Odd Item Background Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.cart_item:nth-child(odd), table.shop_table tfoot tr:nth-child(odd)',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'cart_item_border_width'    => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table td, table.shop_table tfoot td, table.shop_table tfoot th',
                            'property'                  => 'border-top-width',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => true,
                    ),
                    'cart_item_border_color'    => array(
                        'type'                      => 'color',
                        'label'                     => __('Separator Color', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                    ),
                ),
            ),
            'payment_method_style'  => array(
				'title'                 => __('Payment Method', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'payment_method_label_color'            => array(
                        'type'                                  => 'color',
                        'label'                                 => __('Label Color', 'woopack'),
                        'default'                               => '',
                        'show_reset'                            => true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'selector'                              => '#payment ul.payment_methods label',
                            'property'                              => 'color',
                        ),
                    ),
                    'payment_method_box_text_color'         => array(
                        'type'                                  => 'color',
                        'label'                                 => __('Message Text Color', 'woopack'),
                        'default'                               => '',
                        'show_reset'                            => true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'selector'                              => '#payment div.payment_box p',
                            'property'                              => 'color',
                        ),
                    ),
                    'payment_method_box_bg_color'           => array(
                        'type'                                  => 'color',
                        'label'                                 => __('Message Background Color', 'woopack'),
                        'default'                               => '',
                        'show_reset'                            => true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'selector'                              => '#payment div.payment_box',
                            'property'                              => 'background',
                        ),
                    ),
                    'payment_method_box_top_bottom_padding' => array(
                        'type'                                  => 'unit',
                        'label'                                 => __('Message Top & Bottom Padding', 'woopack'),
						'units'									=> array('px'),
						'slider'								=> true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'rules'                                 => array(
                                array(
                                    'selector'                          => '#payment div.payment_box',
                                    'property'                          => 'padding-top',
                                    'unit'                              => 'px',
                                ),
                                array(
                                    'selector'                          => '#payment div.payment_box',
                                    'property'                          => 'padding-bottom',
                                    'unit'                              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                            => true,
                    ),
                    'payment_method_box_right_left_padding' => array(
                        'type'                                  => 'unit',
                        'label'                                 => __('Message Right & Left Padding', 'woopack'),
						'units'									=> array('px'),
						'slider'								=> true,
                        'preview'                               => array(
                            'type'                                  => 'css',
                            'rules' 	                            => array(
                                array(
                                    'selector'                          => '#payment div.payment_box',
                                    'property'                          => 'padding-right',
                                    'unit'                              => 'px',
                                ),
                                array(
                                    'selector'	                     => '#payment div.payment_box',
                                    'property'                          => 'padding-left',
                                    'unit'                              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                            => true,
                    ),
                ),
            ),
        ),
    ),
    'messages'          => array(
        'title'             => __('Messages', 'woopack'),
        'sections'          => array(
            'errors'            => array(
                'title'             => __('Errors', 'woopack'),
                'fields'            => array(
                    'error_bg_color'    => array(
                        'type'              => 'color',
                        'label'             => __('Error Field Background Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce-error',
                            'property'          => 'background-color',
                        ),
                    ),
                    'error_text_color'  => array(
                        'type'              => 'color',
                        'label'             => __('Error Field Message Text Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce-error',
                            'property'          => 'color',
                        ),
                    ),
                    'error_border_color'=> array(
                        'type'              => 'color',
                        'label'             => __('Error Field Border Color', 'woopack'),
                        'show_reset'        => true,
                        'show_alpha'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce-error',
                            'property'          => 'border-color',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'button'            => array(
        'title'             => __('Button', 'woopack'),
        'sections'          => woopack_product_button_fields()
    ),
    'typography'        => array(
        'title'             => __('Typography', 'woopack'),
        'sections'          => array(
            'form_header_fonts'     => array(
                'title'                 => __('Customer Details Header', 'woopack'),
                'fields'                => array(
					'form_header_typography'	=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '#customer_details h3, #customer_details h3 label',
						),
					),
                    'form_header_color'			=> array(
                        'type'						=> 'color',
                        'label'						=> __('Text Color', 'woopack'),
                        'default'					=> '',
                        'show_reset'				=> true,
                        'preview'					=> array(
                            'type'						=> 'css',
                            'selector'					=> '#customer_details h3, #customer_details h3 label',
                            'property'					=> 'color',
                        ),
                    ),
                ),
            ),
            'form_label_fonts'      => array(
				'title'                 => __('Customer Details Label', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'form_label_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.form-row label',
						),
					),
                    'form_label_color'			=> array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.form-row label',
                            'property'                  => 'color',
                        ),
                    ),
                    'form_label_invalid_color'	=> array(
                        'type'                      => 'color',
                        'label'                     => __('Invalid Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.form-row.woocommerce-invalid label',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_title_fonts'     => array(
				'title'                 => __('Review Order Title', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'table_title_typography'	=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '#order_review_heading',
						),
					),
                    'table_title_color'			=> array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '#order_review_heading',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_header_fonts'    => array(
				'title'                 => __('Cart Header', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'table_header_typography'	=> array(
						'type'						=> 'typography',
						'label'						=> __( 'Typography', 'woopack' ),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> 'table.shop_table thead th',
						),
					),
                    'table_header_color'		=> array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table thead th',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'table_fonts'           => array(
				'title'                 => __('Cart Items', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'cart_item_typography'	=> array(
                        'type'					=> 'typography',
                        'label'                 => __('Typography', 'woopack'),
						'responsive'			=> true,
                        'preview'               => array(
                            'type'              	=> 'css',
							'selector'              => 'table.shop_table td,
														table.shop_table tfoot td,
														table.shop_table tfoot th',
                        ),
                    ),
                    'table_color'           => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => 'table.shop_table td, table.shop_table tfoot th',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'coupons_fonts'         => array(
				'title'                 => __('Coupons', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'coupon_typography'		=> array(
                        'type'					=> 'typography',
                        'label'					=> __('Typography', 'woopack'),
						'responsive'			=> true,
						'preview'				=> array(
                            'type'					=> 'css',
                            'selector'				=> '.woocommerce-info',
                        ),
                    ),
                    'coupon_color'			=> array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-info',
                            'property'                  => 'color',
                        ),
                    ),
                    'coupon_link_color'		=> array(
                        'type'                      => 'color',
                        'label'                     => __('Link Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-info a',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'payment_method_fonts'  => array(
				'title'                 => __('Payment Method', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'payment_method_typography'		=> array(
						'type'							=> 'typography',
                        'label'							=> __('Typography', 'woopack'),
                        'responsive'					=> true,
                        'preview'						=> array(
                            'type'							=> 'css',
                            'selector'						=> '#payment ul.payment_methods li',
                        ),
                    ),
                    'payment_method_msg_font_size'	=> array(
                        'type'							=> 'unit',
                        'label'							=> __('Message Font Size', 'woopack'),
						'slider'						=> true,
						'units'							=> array( 'px' ),
                        'responsive'					=> true,
                        'preview'						=> array(
                            'type'							=> 'css',
                            'selector'						=> '#payment div.payment_box, #payment div.payment_box p',
                            'property'						=> 'font-size',
                            'unit'							=> 'px',
                        ),
                    ),
                ),
            ),
        ),
    ),
));
