(function($) {

	<?php if( $settings->product_layout == 1 || $settings->product_layout == 2 ) { ?>
		$('.fl-node-<?php echo $id; ?> .woopack-single-product').imagesLoaded(function() {
			var content_width = $('.fl-node-<?php echo $id; ?> .woopack-single-product').width();
			var img_width = $('.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image').width();
			var calc_wdith = content_width - img_width;
			$('.fl-node-<?php echo $id; ?> .woopack-single-product .product-content').css('width', calc_wdith + 'px');
		});
	<?php } ?>

	var product_class = $('.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .add_to_cart_inline');
	if ( $(product_class).find('span').length > 0 ) {
		var product_html = $(product_class).html().replace("span> – ", "span>");
		$('.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .add_to_cart_inline').html(product_html);
	}

})(jQuery);
