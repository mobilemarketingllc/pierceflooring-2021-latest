<?php if ( 'yes' == $settings->show_image ) :
	include $module->dir . 'includes/partials/product-image.php';
endif; ?>

<div class="product-content">

	<?php
	if ( 'yes' == $settings->product_title ) :
		include $module->dir . 'includes/partials/product-title.php';
    endif;

    if ( 'yes' == $settings->product_rating ) :
		include $module->dir . 'includes/partials/product-rating.php';
    endif;

    if ( 'yes' == $settings->product_price ) :
		include $module->dir . 'includes/partials/product-price.php';
    endif;

    if ( 'yes' == $settings->product_short_description ) :
		include $module->dir . 'includes/partials/product-description.php';
    endif;

    if ( 'cart' == $settings->button_type ) :
		include $module->dir . 'includes/partials/product-add-button.php';
    endif;

    if ( 'custom' == $settings->button_type ) :
		include $module->dir . 'includes/partials/product-custom-button.php';
    endif;

    if ( 'yes' == $settings->show_sku || 'yes' == $settings->show_taxonomy ) :
		include $module->dir . 'includes/partials/product-meta.php';
	endif;
	?>

</div>  <!-- product-content -->
