<?php do_action( 'woopack_single_product_before_button_wrap', $settings, $product ); ?>

<div class="woopack-product-action woocommerce-product-add-to-cart">
	<?php woocommerce_template_single_add_to_cart(); ?>
</div>

<?php do_action( 'woopack_single_product_after_button_wrap', $settings, $product ); ?>