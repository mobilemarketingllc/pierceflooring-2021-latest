<?php
/**
* @class WooPackSingleProduct
*/
class WooPackSingleProduct extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Single Product', 'woopack'),
            'description' 		=> __('Addon to display Single Product.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/single-product/',
            'url' 				=> WOOPACK_URL . 'modules/single-product/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
    }

     /**
    * @method enqueue_scripts
    */
    public function enqueue_scripts()
    {
		$this->add_js('imagesloaded');
	}
	    /**
	 * Ensure backwards compatibility with old settings.
	 *
	 * @since 2.2
	 * @param object $settings A module settings object.
	 * @param object $helper A settings compatibility helper.
	 * @return object
	 */
	public function filter_settings( $settings, $helper ) {
        // Handle old Box Border setting.
        $settings = WooPack_Fields::handle_border_field( $settings, array(
            'box_border_style'  => array(
                'type'              => 'style'
            ),
            'box_border_color'  => array(
                'type'              => 'color'
            ),
            'box_border_radius' => array(
                'type'              => 'radius'
            ),
            'box_shadow_h'      => array(
                'type'              => 'shadow_horizontal',
                'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
            ),
            'box_shadow_v'      => array(
                'type'              => 'shadow_vertical',
                'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
            ),
            'box_shadow_blur'   => array(
                'type'              => 'shadow_blur',
                'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
            ),
            'box_shadow_spread'      => array(
                'type'              => 'shadow_spread',
                'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
            ),
            'box_shadow_color'      => array(
                'type'              => 'shadow_color',
                'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
            )
        ), 'box_border_group' );

        $settings = WooPack_Fields::handle_border_dimension_field( $settings, 'box_border', 'box_border_group' );

        // Handle old Sale Badge Border setting.
        $settings = WooPack_Fields::handle_border_field( $settings, array(
            'sale_badge_border_style'   => array(
                'type'                  => 'style'
            ),
            'sale_badge_border_width'   => array(
                'type'                  => 'width'
            ),
            'sale_badge_border_color'   => array(
                'type'                  => 'color'
            ),
            'sale_badge_border_radius'  => array(
                'type'                  => 'radius'
            )
		), 'sale_badge_border_group' );

        // Handle old Button Border setting.
        $settings = WooPack_Fields::handle_border_field( $settings, array(
            'button_border_style'   => array(
                'type'                  => 'style'
            ),
            'button_border_width'   => array(
                'type'                  => 'width'
            ),
            'button_border_color'   => array(
                'type'                  => 'color'
            ),
            'button_border_radius'  => array(
                'type'                  => 'radius'
            )
		), 'button_border_group' );

        // Handle old Button font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'button_font'   => array(
                'type'          => 'font'
            ),
            'button_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->button_font_size ) && 'custom' == $settings->button_font_size )
            ),
            'button_text_transform' => array(
                'type'          => 'text_transform',
                'condition'     => ( isset( $settings->button_text_transform ) && 'default' != $settings->button_text_transform )
            )
		), 'button_typography' );
        // Handle old Title font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'product_title_font'   => array(
                'type'          => 'font'
            ),
            'product_title_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->product_title_font_size ) && 'custom' == $settings->product_title_font_size )
            ),
            'product_title_line_height'    => array(
                'type'          => 'line_height',
            ),
            'product_title_text_transform' => array(
                'type'          => 'text_transform',
                'condition'     => ( isset( $settings->product_title_text_transform ) && 'default' != $settings->product_title_text_transform )
            )
        ), 'product_title_typography' );

        // Handle old Regular Price font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'regular_price_font'   => array(
                'type'          => 'font'
            ),
            'regular_price_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->regular_price_font_size ) && 'custom' == $settings->regular_price_font_size )
            ),
            'regular_price_line_height'    => array(
                'type'          => 'line_height',
            ),
        ), 'regular_price_typography' );

        // Handle old Short Description font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'short_description_font'   => array(
                'type'          => 'font'
            ),
            'short_description_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->short_description_font_size ) && 'custom' == $settings->short_description_font_size )
            ),
            'short_description_line_height'    => array(
                'type'          => 'line_height',
            ),
        ), 'short_description_typography' );

        // Handle old Sale Badge font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'sale_badge_font'   => array(
                'type'          => 'font'
            ),
            'sale_badge_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->sale_badge_font_size ) && 'custom' == $settings->sale_badge_font_size )
            ),
            'sale_badge_line_height'    => array(
                'type'          => 'line_height',
            ),
        ), 'sale_badge_typography' );

        // Handle old Product font size setting.
        $settings = WooPack_Fields::handle_typography_field( $settings, array(
            'meta_font'   => array(
                'type'          => 'font'
            ),
            'meta_font_size_custom'   => array(
                'type'          => 'font_size',
                'condition'     => ( isset( $settings->meta_font_size ) && 'custom' == $settings->meta_font_size )
            ),
            'meta_text_transform' => array(
                'type'          => 'text_transform',
                'condition'     => ( isset( $settings->meta_text_transform ) && 'default' != $settings->meta_text_transform )
            )
		), 'meta_typography' );

		return $settings;
	}

	public static function get_products_list() {
		if ( ! isset( $_GET['fl_builder'] ) ) {
			return array();
		}

		return WooPack_Helper::get_products_list();
	}
}

/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackSingleProduct', array(
    'layouts'   => array(
        'title'     => __('Layout', 'woopack'),
        'sections'  => array(
            'layout'    => array(
                'title'     => '',
                'fields'    => array(
                    'product_layout'    => array(
                        'type'              => 'layout',
                        'label'             => __('', 'woopack'),
                        'default'           => 1,
                        'options'           => array(
                            1                    => WOOPACK_URL . 'modules/single-product/images/layout-1.png',
                            2                    => WOOPACK_URL . 'modules/single-product/images/layout-2.png',
                            3                    => WOOPACK_URL . 'modules/single-product/images/layout-3.png',
                            4                    => WOOPACK_URL . 'modules/single-product/images/layout-4.png',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'content'   => array(
        'title'     => __('Content', 'woopack'),
        'sections'  => array(
            'product'           => array(
                'title'             => __('Product', 'woopack'),
                'fields'            => array(
                    'product_id'                => array(
                        'type'                      => 'select',
                        'label'                     => __('Product', 'woopack'),
						'options'                   => WooPackSingleProduct::get_products_list(),
						'connections'				=> array('string')
                    ),
                    'product_title'             => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Product Title?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'                  => array( 'product_title_style' ),
                            ),
                        ),
                    ),
                    'product_price'             => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Price?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'              => array( 'product_price_align', 'regular_price_fonts', 'sale_price_fonts' ),
                            ),
                        ),
                    ),
                    'product_rating'            => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Rating?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'              => array( 'product_rating_style' ),
								'fields'				=> array( 'product_rating_count', 'product_rating_text' )
                            ),
                        ),
                    ),
                    'product_rating_count'            => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Rating Count?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
								'fields'				=> array( 'product_rating_text', 'product_rating_count_color' ),
                                'sections'              => array( 'product_rating_style', 'rating_count_taxonomy' ),
                            ),
                        ),
                    ),
					'product_rating_text'       => array(
                        'type'              => 'text',
                        'label'             => __('Custom Text', 'woopack'),
						'default' 			=> __( 'customer review', 'woopack' ),
						'connections' 		=> array( 'string', 'html' ),
                    ),
                    'product_short_description' => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Short Description?', 'woopack'),
                        'default'                   => 'yes',
                        'options'                   => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no'                        => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'                  => array( 'short_description' ),
                            ),
                        ),
                    ),
                ),
            ),
            'sale_badge'        => array(
				'title'         => __('Sale Badge', 'woopack'),
				'collapsed'		=> true,
                'fields'        => array(
                    'show_sale_badge'   => array(
                        'type'              => 'select',
                        'label'             => __('Show Sale Badge?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'sections'          => array( 'sale_badge_style', 'sale_badge_fonts' ),
                            ),
                        ),
                    ),
                ),
            ),
            'product_button'    => array(
				'title'             => __('Button', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_type'       => array(
                        'type'              => 'select',
                        'label'             => __('Button Type', 'woopack'),
                        'default'           => 'cart',
                        'options'           => array(
                            'cart'              => __('Add to Cart', 'woopack'),
                            'custom'            => __('Custom', 'woopack'),
                            'none'              => __('None', 'woopack'),
                        ),
                        'toggle'            => array(
                            'cart'              => array(
                                'tabs'				=> array( 'button' ),
                            ),
                            'custom'            => array(
                                'tabs'				=> array( 'button' ),
                                'fields'            => array( 'button_text', 'button_link', 'button_link_target' ),
                            ),
                        ),
                    ),
                    'button_text'       => array(
                        'type'              => 'text',
                        'label'             => __('Custom Button Text', 'woopack'),
                        'connections'       => array('string'),
                        'preview'           => array(
                            'type'      		=> 'text',
                            'selector'  		=> '.woocommerce-product-add-to-cart .woopack-product-button-custom'
                        ),
                    ),
                    'button_link'       => array(
                        'type'              => 'link',
                        'label'             => __('Link', 'woopack'),
                        'connections'       => array('url'),
                    ),
                    'button_link_target'=> array(
                        'type'              => 'select',
                        'label'             => __('Target', 'woopack'),
                        'options'			=> array(
                            '_self'				=> __('Same Window', 'woopack'),
                            '_blank'			=> __('New Window', 'woopack')
                        )
                    ),
                ),
            ),
            'product_image'     => array(
				'title'             => __('Product Image', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'show_image'        => array(
                        'type'              => 'select',
                        'label'             => __('Show Image?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'sections'          => array( 'image_style' ),
                                'fields'            => array( 'image_size' ),
                            ),
                        ),
                    ),
                    'image_size'        => array(
                        'type'              => 'photo-sizes',
                        'label'             => __('Size', 'woopack'),
                        'default'           => 'medium',
                    ),
                ),
            ),
            'product_taxonomy'  => array(
				'title'             => __('Product Meta', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
					'show_sku'			=> array(
						'type'				=> 'select',
						'label'				=> __('Show SKU', 'woopack'),
						'default'			=> 'no',
						'options' 		    => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no' 			    => __('No', 'woopack'),
                        ),
					),
                    'show_taxonomy'             => array(
                        'type'                      => 'select',
                        'label'                     => __('Show Taxonomy', 'woopack'),
                        'default' 		            => 'no',
                        'options' 		            => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no' 			            => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'sections'                  => array('meta_style'),
                                'fields'                    => array('select_taxonomy', 'show_taxonomy_custom_text')
                            )
                        )
                    ),
                    'select_taxonomy'           => array(
                        'type'                      => 'select',
                        'label'                     => __('Select Taxonomy', 'woopack'),
                        'options'                   => WooPack_Helper::get_taxonomies_list()
                    ),
                    'show_taxonomy_custom_text' => array(
                        'type'                      => 'select',
                        'label'                     => __('Use Custom Taxonomy Label?', 'woopack'),
                        'default'                   => 'no',
                        'options' 		            => array(
                            'yes'                       => __('Yes', 'woopack'),
                            'no' 			            => __('No', 'woopack'),
                        ),
                        'toggle'                    => array(
                            'yes'                       => array(
                                'fields'                    => array('taxonomy_custom_text')
                            )
                        )
                    ),
                    'taxonomy_custom_text'      => array(
                        'type'                      => 'text',
                        'label'                     => __('Custom Label', 'woopack'),
                        'default'                   => __('Category: ', 'woopack'),
                        'connections'               => array('string'),
                    ),
                ),
            ),
        ),
    ),
    'style'     => array(
        'title'     => __('Style', 'woopack'),
        'sections'  => array(
            'box'               => array(
                'title'             => __('Box', 'woopack'),
                'fields'            => array(
                    'content_bg_color'      => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woopack-single-product',
                            'property'              => 'background-color',
                        ),
                    ),
                    'content_bg_color_hover'=> array(
                        'type'                  => 'color',
                        'label'                 => __('Background Hover Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'none'
                        )
                    ),
                    'content_alignment'     => array(
                        'type'                  => 'align',
                        'label'                 => __('Content Alignment', 'woopack'),
                        'default'               => '',
                    ),
                    'box_padding'           => array(
                        'type'                  => 'dimension',
                        'label' 			    => __('Padding', 'woopack'),
						'slider'		        => true,
						'units'		  	        => array( 'px' ),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'			    => '.woopack-single-product',
                            'property'			    => 'padding',
                            'unit' 				    => 'px'
                        ),
                        'responsive'            => true,
                    ),
                ),
            ),
            'box_border'        => array(
				'title'             => __('Box Border', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'box_border_group'      => array(
                        'type'                  => 'border',
                        'label'                 => __('Border Style', 'woopack'),
                        'responsive'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector' 		        => '.woopack-single-product',
                        ),
                    ),
                ),
            ),
            'content_style' 	=> array(
				'title'             => __('Content', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'content_padding'       => array(
                        'type'                      => 'dimension',
                        'label' 			        => __('Padding', 'woopack'),
						'slider'		            => true,
						'units'		  	            => array( 'px' ),
						'responsive' 		        => true,
                        'preview' 			        => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-single-product .product-content',
                            'property'                  => 'padding',
                            'unit'                      => 'px'
                        ),
                    ),
                ),
            ),
            'product_rating_style'=> array(
				'title'                 => __('Rating', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'product_rating_size'           => array(
                        'type'                          => 'unit',
                        'label'                         => __('Size', 'woopack'),
						'units'							=> array('px'),
						'slider'						=> true,
                        'responsive' 			        => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'				        => '.woocommerce-product-rating',
                            'property'                      => 'font-size',
                            'unit'                          => 'px',
                        ),
                    ),
                    'product_rating_default_color'  => array(
                        'type'                          => 'color',
                        'label'                         => __('Rating Star Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce-product-rating .star-rating:before',
                            'property'                      => 'color',
                        ),
                    ),
                    'product_rating_color'          => array(
                        'type'                          => 'color',
                        'label'                         => __('Rating Star Active Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woocommerce-product-rating .star-rating span:before',
                            'property'                      => 'color',
                        ),
                    ),
                    'product_rating_margin'         => array(
                        'type'                          => 'unit',
                        'label'                         => __('Margin Bottom', 'woopack'),
                        'default'                       => '',
						'units'							=> array('px'),
						'slider'						=> true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product .woocommerce-product-rating',
                            'property'                      => 'margin-bottom',
                            'unit'                          => 'px',
                        ),
                        'responsive' 			        => true,
                    ),
					'product_rating_count_color'   => array(
						'type'       => 'color',
						'label'      => __('Rating Count Color', 'woopack'),
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-single-product .woocommerce-product-rating .woocommerce-rating-count',
							'property' => 'color',
						),
					),
                ),
            ),
            'sale_badge_style' 	=> array(
				'title'             => __('Sale Badge', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'badge_position'            => array(
                        'type'                      => 'select',
                        'label'  			        => __('Sale Badge Position', 'woopack'),
                        'default'                   => 'top-left',
                        'options' 		            => array(
                            'top-left'                  => __('Top Left', 'woopack'),
                            'top-center'                => __('Top Center', 'woopack'),
                            'top-right'                 => __('Top Right', 'woopack'),
                            'bottom-left'               => __('Bottom Left', 'woopack'),
                            'bottom-center'             => __('Bottom Center', 'woopack'),
                            'bottom-right'              => __('Bottom Right', 'woopack'),
                        ),
                    ),
                    'badge_bg_color'            => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '1e8cbe',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'			        => '.onsale',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'badge_color'               => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'default'                   => 'ffffff',
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.onsale',
                            'property'                  => 'color',
                        ),
                    ),
					'badge_margin_left_right'	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Horizontal Spacing', 'woopack'),
						'units'							=> array('px'),
						'slider'						=> true,
                        'responsive' 			        => true,
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-left',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-right',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                ),
					'badge_margin_top_bottom'	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Vertical Spacing', 'woopack'),
						'units'							=> array('px'),
						'slider'						=> true,
                        'responsive' 			        => true,
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-top',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'margin-bottom',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                ),
                    'badge_padding_left_right'	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Padding Left/Right', 'woopack'),
						'units'							=> array('px'),
						'slider'						=> true,
                        'responsive' 			        => true,
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-left',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-right',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                ),
					'badge_padding_top_bottom'	=> array(
	                    'type'              		=> 'unit',
	                    'label' 					=> __('Padding Top/Bottom', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'responsive' 			    => true,
	                    'preview' 					=> array(
	                        'type' 						=> 'css',
	                        'rules'						=> array(
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-top',
			                        'unit' 						=> 'px'
								),
								array(
									'selector'					=> '.onsale',
			                        'property'					=> 'padding-bottom',
			                        'unit' 						=> 'px'
								),
							)
	                    ),
	                ),
                    'sale_badge_border_group'   => array(
                        'type'                      => 'border',
                        'label'                     => __('Border Style', 'woopack'),
                        'responsive'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector' 		            => '.onsale',
                        ),
                    ),                ),
            ),
            'meta_style'        => array(
				'title'             => __('Product Meta', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'meta_text_color'	=> array(
                        'type'              => 'color',
                        'label'             => __('Text Color', 'woopack'),
                        'show_reset'        => true,
                        'preview'           => array(
                            'type' 				=> 'css',
                            'selector'			=> '.woopack-single-product .product_meta',
                            'property'			=> 'color',
                        ),
                    ),
                    'meta_link_color'	=> array(
                        'type'              => 'color',
                        'label'             => __('Link Color', 'woopack'),
                        'show_reset'        => true,
                    ),
                    'meta_border'		=> array(
                        'type'              => 'select',
                        'label'  			=> __('Show Divider', 'woopack'),
                        'default'           => 'yes',
                        'options' 		    => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'             	=> __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'fields'            => array('meta_border_color')
                            )
                        )
                    ),
                    'meta_border_color'	=> array(
                        'type'              => 'color',
                        'label'             => __('Divider Color', 'woopack'),
                        'show_reset'        => true,
                        'default'           => 'eeeeee',
                        'preview'           => array(
                            'type' 				=> 'css',
                            'selector'			=> '.woopack-single-product .product_meta',
                            'property'			=> 'border-color',
                        ),
                    ),
                    'meta_padding'		=> array(
                        'type'              => 'dimension',
                        'label'             => __('Padding', 'woopack'),
						'units'				=> array('px'),
						'slider'			=> true,
						'responsive'		=> true,
						'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woopack-single-product .product_meta',
                            'property'          => 'padding',
                            'unit'              => 'px'
                        ),
                    ),
                ),
			),
			'variation_style'	=> array(
				'title'				=> __('Variation Table', 'woopack'),
				'collapsed'			=> true,
				'fields'			=> woopack_product_variation_style_fields()
			)
        ),
    ),
    'button'    => array(
        'title'     => __('Button', 'woopack'),
        'sections'  => array(
            'button_property'   => array(
                'title'             => __('Structure', 'woopack'),
                'fields'            => array(
                    'button_width'          => array(
                        'type'                  => 'select',
                        'label'                 => __('Width', 'woopack'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'woopack'),
                            'full_width'            => __('Full Width', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('button_width_custom')
                            ),
                        ),
                    ),
                    'button_width_custom'   => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Width', 'woopack'),
						'slider'				=> true,
						'units'					=> array( '%' ),
						'responsive'			=> true,
                    ),
                    'button_margin_top'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Margin Top', 'woopack'),
                        'default'               => '5',
						'slider'				=> true,
						'units'					=> array( 'px' ),
						'responsive'			=> true,
                        'preview' 				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.woocommerce-product-add-to-cart',
                            'property'				=> 'margin-top',
                            'unit' 					=> 'px'
                        ),
                    ),
                    'button_margin_bottom'  => array(
                        'type'                  => 'unit',
                        'label'                 => __('Margin Bottom', 'woopack'),
                        'default'               => '15',
						'slider'				=> true,
						'units'					=> array( 'px' ),
						'responsive'			=> true,
                        'preview' 				=> array(
                            'type' 					=> 'css',
                            'selector'				=> '.woocommerce-product-add-to-cart',
                            'property'				=> 'margin-bottom',
                            'unit' 					=> 'px'
                        ),
                    ),
                ),
            ),
            'button_color'      => array(
				'title'             => __('Colors', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_bg_color'       => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'              => 'background-color',
                        ),
                    ),
                    'button_bg_color_hover' => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Hover Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a:hover, .woocommerce-product-add-to-cart a:hover, .woocommerce-product-add-to-cart .button:hover',
                            'property'              => 'background-color',
                        ),
                    ),
                    'button_color'          => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'              => 'color',
                        ),
                    ),
                    'button_color_hover'    => array(
                        'type'                  => 'color',
                        'label'                 => __('Text Hover Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                    ),
                ),
            ),
            'button_border'     => array(
				'title'             => __('Border', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_border_group'       => array(
                        'type'                      => 'border',
                        'label'                     => __('Border Style', 'woopack'),
                        'responsive'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector' 		            => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                        ),
                    ),
                    'button_border_color_hover' => array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color on Hover', 'woopack'),
                        'default'			        => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce-product-add-to-cart a:hover, .woocommerce-product-add-to-cart .button:hover',
                            'property'                  => 'border-color',
                        ),
                    ),
                ),
            ),
            'button_padding'    => array(
				'title'             => __('Padding', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_padding'    => array(
                        'type'                  => 'dimension',
                        'label' 			    => __('Padding', 'woopack'),
						'slider'				=> true,
						'units'					=> array( 'px' ),
						'responsive'			=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce-product-add-to-cart .add_to_cart_inline a, .woocommerce-product-add-to-cart a, .woocommerce-product-add-to-cart .button',
                            'property'              => 'padding',
                            'unit'                  => 'px'
                        ),
                    ),
                ),
            ),
            'button_font'       => array(
				'title'             => __('Typography', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'button_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Button Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woocommerce-product-add-to-cart .add_to_cart_inline a, 
                                                    .woocommerce-product-add-to-cart a, 
                                                    .woocommerce-product-add-to-cart .button',
                        ),
					),
                ),
            ),
        ),
    ),
    'typography'=> array(
        'title'         => __('Typography', 'woopack'),
        'sections' 	    => array(
            'product_title_style'   => array(
                'title'                 => __('Title', 'woopack'),
                'fields'                => array(
                    'product_title_heading_tag'     => array(
                        'type'                          => 'select',
                        'label' 		                => __('Heading Tag', 'woopack'),
                        'default'                       => 'h2',
                        'options'                       => array(
                            'h1'                            => __('H1', 'woopack'),
                            'h2'                            => __('H2', 'woopack'),
                            'h3'                            => __('H3', 'woopack'),
                            'h4'                            => __('H4', 'woopack'),
                            'h5'                            => __('H5', 'woopack'),
                            'h6'                            => __('H6', 'woopack'),
                        ),
                    ),
                    'product_title_typography'      => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Title Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-product-title',
                        ),
					),
                    'product_title_color'           => array(
                        'type'                          => 'color',
                        'label'                         => __('Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product.woocommerce .woopack-product-title',
                            'property'                      => 'color',
                        ),
                    ),
                    'product_title_margin'          => array(
                        'type'                          => 'unit',
                        'label'                         => __('Margin Bottom', 'woopack'),
						'slider'		                => true,
						'units'		  	                => array( 'px' ),
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.woopack-single-product .woopack-product-title',
                            'property'                      => 'margin-bottom',
                            'unit'                          => 'px',
                        ),
                        'responsive'                    => true,
                    ),
                ),
            ),
			'rating_count_taxonomy' => array(
				'title'     => __('Rating Count Taxonomy', 'woopack'),
				'collapsed' => true,
				'fields'    => array(
					'rating_count_taxonomy' => array(
						'type'       => 'typography',
						'label'      => __( 'Typography', 'woopack' ),
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-single-product .woocommerce-product-rating .woocommerce-rating-count',
						),
					)
				),
			),
            'regular_price_fonts'   => array(
				'title'                 => __('Price', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'regular_price_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Regular Price Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.price .amount',
                        ),
					),
                    'regular_price_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price .amount',
                            'property'                  => 'color',
                        ),
                    ),
                    'product_price_margin'      => array(
                        'type'                      => 'unit',
                        'label'                     => __('Margin Bottom', 'woopack'),
						'slider'		            => true,
						'units'		  	            => array( 'px' ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woopack-single-product .price',
                            'property'                  => 'margin-bottom',
                            'unit'                      => 'px',
                        ),
                        'responsive' 			    => true,
                    ),
                ),
            ),
            'sale_price_fonts'      => array(
				'title'                 => __('Sale Price', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'sale_price_font_size'  => array(
                        'type'                  => 'select',
                        'label'                 => __('Font Size', 'woopack'),
                        'default'               => 'default',
                        'options'               => array(
                            'default'               => __('Default', 'woopack'),
                            'custom'                => __('Custom', 'woopack')
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('sale_price_font_size_custom')
                            )
                        )
                    ),
                    'sale_price_font_size_custom' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Font Size', 'woopack'),
                        'description'               => 'px',
                        'responsive'                => array(
                            'placeholder'               => array(
                                'default'                   => '15',
                                'medium'                    => '',
                                'responsive'                => '',
                            ),
                        ),
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price ins .amount',
                            'property'                  => 'font-size',
                            'unit'                      => 'px',
                        ),
                    ),
                    'sale_price_color'          => array(
                        'type'                      => 'color',
                        'label'                     => __('Color', 'woopack'),
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.price ins .amount',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'short_description'     => array(
				'title'                 => __('Short Description', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'short_description_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Short Description Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woocommerce-product-details__short-description p',
                        ),
					),
                    'short_description_color'           => array(
                        'type'                              => 'color',
                        'label'                             => __('Color', 'woopack'),
                        'show_reset'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce-product-details__short-description p',
                            'property'                          => 'color',
                        ),
                    ),
                    'product_description_margin_bottom'        => array(
                        'type'                              => 'unit',
                        'label'                             => __('Margin Bottom', 'woopack'),
						'slider'		                    => true,
						'units'		  	                    => array( 'px' ),
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woopack-single-product .woocommerce-product-details__short-description p',
                            'property'                          => 'margin-bottom',
                            'unit'                              => 'px',
                        ),
                        'responsive' 			            => true,
                    ),
                ),
            ),
            'sale_badge_fonts'      => array(
				'title'                 => __('Sale Badge', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'sale_badge_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Sale Badge Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.onsale',
                        ),
					),
                ),
            ),
            'meta_fonts'            => array(
				'title'                 => __('Product Taxonomy', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'meta_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Product Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-single-product .product_meta',
                        ),
					),
                ),
            ),
        ),
    ),
));
