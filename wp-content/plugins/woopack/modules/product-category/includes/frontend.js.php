;(function($) {
	<?php
	$columns = empty( $settings->category_columns ) ? 3 : $settings->category_columns;
	$columns_medium = empty( $settings->category_columns_medium ) ? $columns : $settings->category_columns_medium;
	$columns_responsive = empty( $settings->category_columns_responsive ) ? $columns_medium : $settings->category_columns_responsive;
	?>

	function fix_columns() {
		$('.fl-node-<?php echo $id; ?> .woopack-product-category').removeClass('woopack-last');
	
		if ( window.innerWidth > <?php echo $global_settings->medium_breakpoint; ?> ) {
			$('.fl-node-<?php echo $id; ?> .woopack-product-category:nth-child(<?php echo $columns . 'n'; ?>)').addClass('woopack-last');
		} else if ( window.innerWidth <= <?php echo $global_settings->medium_breakpoint; ?> && window.innerWidth > <?php echo $global_settings->responsive_breakpoint; ?> ) {
			$('.fl-node-<?php echo $id; ?> .woopack-product-category').removeClass('woopack-last');
			$('.fl-node-<?php echo $id; ?> .woopack-product-category:nth-child(<?php echo $columns_medium . 'n'; ?>)').addClass('woopack-last');
		} else if ( window.innerWidth <= <?php echo $global_settings->responsive_breakpoint; ?> ) {
			$('.fl-node-<?php echo $id; ?> .woopack-product-category').removeClass('woopack-last');
			$('.fl-node-<?php echo $id; ?> .woopack-product-category:nth-child(<?php echo $columns_responsive . 'n'; ?>)').addClass('woopack-last');
		}
	}

	fix_columns();

	$(window).resize(function() {
		fix_columns();
	});
})(jQuery);
