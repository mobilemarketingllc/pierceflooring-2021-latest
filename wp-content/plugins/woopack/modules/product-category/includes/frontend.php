<?php
$cat_match    = isset( $settings->tax_product_product_cat_matching ) ? $settings->tax_product_product_cat_matching : false ;
$ids          = isset( $settings->tax_product_product_cat ) ? explode( ',', $settings->tax_product_product_cat ) : array();
$taxonomy     = 'product_cat';
$show_count   = 1;      // 1 for yes, 0 for no
$pad_counts   = 1;      // 1 for yes, 0 for no
$hierarchical = 1;      // 1 for yes, 0 for no
$title        = '';
$empty        = 1;

$args = array(
	'taxonomy'     => $taxonomy,
	'show_count'   => $show_count,
	'pad_counts'   => $pad_counts,
	'hierarchical' => $hierarchical,
	'title_li'     => $title,
	'hide_empty'   => $empty,
);

if ( ! empty( $settings->order_by ) ) {
	$args['orderby'] = $settings->order_by;
	if ( ! empty( $settings->order ) ) {
		$args['order'] = $settings->order;
	}
}

if ( $cat_match && 'related' != $cat_match && ! empty( $ids ) ) {
	if ( isset( $settings->display_data ) && ( 'children_only' === $settings->display_data || 'default' === $settings->display_data ) && ! empty( $ids[0] ) ) {
		$args['parent'] = $ids;
	} else {
		$args['include'] = $ids;
	}
}
if ( ( ! $cat_match || 'related' == $cat_match ) && ! empty( $ids ) ) {
	if ( isset( $settings->display_data ) && ( 'parent_only' !== $settings->display_data ) && ! empty( $ids[0] ) ) {

		foreach ( $ids as $term_id ) {
			$tmp_ids = get_term_children( $term_id, $taxonomy );
			$ids     = array_merge( $ids, $tmp_ids );
		}
		$args['exclude'] = $ids;
	} else {
		$args['exclude'] = $ids;
	}
}

// Show child terms on taxonomy archive page.
if ( isset( $settings->on_tax_archive ) && is_tax( $taxonomy ) ) {
	$current_object = get_queried_object();
	if ( 'children_only' === $settings->on_tax_archive ) {
		$args['child_of'] = $current_object->term_id;
	}
	if ( 'parent_only' === $settings->on_tax_archive && intval( $current_object->parent ) > 0 ) {
		$args['include'] = (array) $current_object->parent;
	}
}

$args = apply_filters( 'woopack_product_category_args', $args, $settings );

if ( isset( $settings->display_data ) && 'children_only' === $settings->display_data && isset( $args['parent'] ) && ! empty( $args['parent'][0] ) ) {
	$all_categories = $module->get_categories( $args, 'children_only' );
} elseif ( isset( $settings->display_data ) && 'default' === $settings->display_data && isset( $args['parent'] ) && ! empty( $args['parent'][0] ) ) {
	$all_categories = $module->get_categories( $args, 'default' );
} else {
	$all_categories = get_categories( $args );
}

?>

<div class="woopack-product-categories woopack-clear">
<?php
foreach ( $all_categories as $cat ) {
	if ( isset( $settings->display_data ) && 'parent_only' === $settings->display_data ) {
		if ( isset( $args['include'][0] ) && intval( $args['include'][0] ) > 0 ) {
			$inc_array = $args['include'];
			if ( ! in_array( $cat->term_id, $inc_array ) ) {
				continue;
			}
		} elseif ( 0 !== $cat->parent ) {
			continue;
		}
	} elseif ( isset( $settings->display_data ) && 'children_only' === $settings->display_data ) {
		if ( isset( $args['include'][0] ) && intval( $args['include'][0] ) > 0 ) {
			$inc_array = $args['include'];
			if ( ! in_array( $cat->parent, $inc_array ) ) {
				continue;
			}
		} elseif ( isset( $args['exclude'][0] ) && intval( $args['exclude'][0] ) > 0 ) {
			$exc_array = $args['exclude'];
			if ( in_array( $cat->parent, $exc_array ) || 0 === $cat->parent ) {
				continue;
			}
		} elseif ( 0 === $cat->parent ) {
			continue;
		}
	} elseif ( isset( $settings->display_data ) && 'default' === $settings->display_data && isset( $args['exclude'] ) && ! empty( $args['exclude'][0] ) ) {
		$exc_array = $args['exclude'];
		if ( in_array( $cat->parent, $exc_array ) ) {
			continue;
		}
	}

	$cat_thumb_id     = get_term_meta( $cat->term_id, 'thumbnail_id', true );
	$shop_catalog_img = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
	$term_link        = get_term_link( $cat, $taxonomy );

	include WOOPACK_DIR . 'modules/product-category/includes/layout-1.php';
}
?>
</div>
