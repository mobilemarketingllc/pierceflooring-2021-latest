<?php
$md_class = '';
$sm_class = '';

if ( '' != $settings->tabs_position_medium ) {
	$md_class = ' tabs-position-md-' . $settings->tabs_position_medium;
}
if ( '' != $settings->tabs_position_responsive ) {
	$sm_class = ' tabs-position-sm-' . $settings->tabs_position_responsive;
}
?>
<?php do_action( 'woopack_before_my_account_wrap', $settings ); ?>
<div class="woopack-my-account tabs-position-<?php echo $settings->tabs_position; ?><?php echo $md_class; ?><?php echo $sm_class; ?>">
	<?php do_action( 'woopack_before_my_account_content', $settings ); ?>
		<?php
			if ( isset( $settings->endpoint ) && ! empty( $settings->endpoint ) && FLBuilderModel::is_builder_active() ) {
				global $wp;
				$wp->query_vars[ $settings->endpoint ] = 1;
			}
			add_filter( 'woocommerce_account_menu_items', array( $module, 'customize_my_account_tabs' ) );
			echo do_shortcode('[woocommerce_my_account]');
			remove_filter( 'woocommerce_account_menu_items', array( $module, 'customize_my_account_tabs' ) );
		?>
	 <?php do_action( 'woopack_after_my_account_content', $settings ); ?>
</div>
<?php do_action( 'woopack_before_my_account_wrap', $settings ); ?>
