<?php

class WooPack_Fields {
    /**
     * Convert old typography fields value into single typography field.
     * 
     * @since 2.6.8
     * 
     * @param object $settings	Module settings
     * @param string $fields	Array of old fields
     * @param string $new_field	Name of the new field
     * 
     * @return object $settings
     */
	
	public static function handle_typography_field( $settings, $fields = array(), $new_field = 'typography' ) {
		if ( ! is_object( $settings ) || empty( $fields ) || empty( $new_field ) ) {
			return $settings;
		}

		$typography 			= array();
		$typography_medium 		= array();
		$typography_responsive 	= array();

		foreach ( $fields as $name => $field ) {
			if ( ! isset( $settings->{$name} ) ) {
				continue;
			}

			$type		= $field['type'];
			$value 		= $settings->{$name};
			$condition 	= isset( $field['condition'] ) ? $field['condition'] : true;

			if ( ! $condition ) {
				continue;
			}
			
			switch ( $type ) {
				case 'font':
					$typography['font_family'] = $value['family'];
					$typography['font_weight'] = $value['weight'] === 'regular' ? 'normal' : $value['weight'];
					break;

				case 'font_size':
				case 'letter_spacing':
				case 'line_height':
					$unit = ( 'line_height' == $type ) ? '' : 'px';
					if ( ! is_array( $value ) ) {
						$typography[ $type ] = array(
							'length'	=> $value,
							'unit'		=> $unit
						);
						if ( isset( $settings->{ $name . '_medium' } ) ) {
							$typography_medium[ $type ] = array(
								'length'	=> $settings->{ $name . '_medium' },
								'unit'		=> $unit
							);

							unset( $settings->{ $name . '_medium' } );
						}
						if ( isset( $settings->{ $name . '_responsive' } ) ) {
							$typography_responsive[ $type ] = array(
								'length'	=> $settings->{ $name . '_responsive' },
								'unit'		=> $unit
							);

							unset( $settings->{ $name . '_responsive' } );
						}
					} else {
						$desktop = 'desktop';
						$tablet = 'tablet';
						$mobile = 'mobile';
						if ( isset( $field['keys'] ) ) {
							if ( isset( $field['keys']['desktop'] ) ) {
								$desktop = $field['keys']['desktop'];
							}
							if ( isset( $field['keys']['tablet'] ) ) {
								$tablet = $field['keys']['tablet'];
							}
							if ( isset( $field['keys']['mobile'] ) ) {
								$mobile = $field['keys']['mobile'];
							}
						}
						$typography[ $type ] = array(
							'length'	=> $value[ $desktop ],
							'unit'		=> $unit
						);
						$typography_medium[ $type ] = array(
							'length'	=> $value[ $tablet ],
							'unit'		=> $unit
						);
						$typography_responsive[ $type ] = array(
							'length'	=> $value[ $mobile ],
							'unit'		=> $unit
						);
					}
					break;

				case 'medium_font_size':
				case 'medium_line_height':
					$orig_type = str_replace( 'medium_', '', $type );
					$typography_medium[ $orig_type ] = array(
						'length'	=> $value,
						'unit'		=> ( 'line_height' == $orig_type ) ? '' : 'px'
					);
					break;

				case 'responsive_font_size':
				case 'responsive_line_height':
					$orig_type = str_replace( 'responsive_', '', $type );
					$typography_responsive[ $orig_type ] = array(
						'length'	=> $value,
						'unit'		=> ( 'line_height' == $orig_type ) ? '' : 'px'
					);
					break;

				case 'text_align':
				case 'text_transform':
				case 'font_style':
					$typography[ $type ] = $value;
					break;

				case 'text_shadow':
					$typography[ $type ] = array(
						'horizontal'	=> isset( $value['horizontal'] ) ? $value['horizontal'] : '',
						'vertical'		=> isset( $value['vertical'] ) ? $value['vertical'] : '',
						'blur'			=> isset( $value['blur'] ) ? $value['blur'] : '',
					);
					if ( isset( $field['color'] ) ) {
						$typography[ $type ]['color'] = $field['color'];
					}
					break;
			}

			unset( $settings->{$name} );
		}

		if ( ! empty( $typography ) ) {
			$settings->{$new_field} = $typography;
		}
		if ( ! empty( $typography_medium ) ) {
			$settings->{$new_field . '_medium'} = $typography_medium;
		}
		if ( ! empty( $typography_responsive ) ) {
			$settings->{$new_field . '_responsive'} = $typography_responsive;
		}

		return $settings;
	}

    public static function handle_border_field( $settings, $fields = array(), $new_field = '' ) {
        if ( ! is_object( $settings ) || empty( $fields ) || empty( $new_field ) ) {
            return $settings;
        }

        $border = array();
        $border_medium = array();
        $border_responsive = array();

        foreach ( $fields as $name => $field ) {
            if ( ! isset( $settings->{$name} ) ) {
                continue;
            }

            $type		= $field['type'];
            $value 		= isset( $field['value'] ) ? $field['value'] : $settings->{$name};
            $condition 	= isset( $field['condition'] ) ? $field['condition'] : true;

            if ( ! $condition ) {
                continue;
            }

            switch ( $type ) {
                case 'style':
                case 'color':
                    $border[ $type ] = $value;
                    break;

                case 'width':
                    if ( ! is_array( $value ) ) {
                        
                        $border['width']['top'] = $value;
                        $border['width']['right'] = $value;
                        $border['width']['bottom'] = $value;
                        $border['width']['left'] = $value;

                        if ( isset( $settings->{$name . '_medium'} ) ) {
                            $value = $settings->{$name . '_medium'};
                            $border_medium['width']['top'] = $value;
                            $border_medium['width']['right'] = $value;
                            $border_medium['width']['bottom'] = $value;
                            $border_medium['width']['left'] = $value;
                        }
                        if ( isset( $settings->{$name . '_responsive'} ) ) {
                            $value = $settings->{$name . '_responsive'};
                            $border_responsive['width']['top'] = $value;
                            $border_responsive['width']['right'] = $value;
                            $border_responsive['width']['bottom'] = $value;
                            $border_responsive['width']['left'] = $value;
                        }

                    } else {
                        $border['width']['top'] = isset( $value['top'] ) ? $value['top'] : '';
                        $border['width']['right'] = isset( $value['right'] ) ? $value['right'] : '';
                        $border['width']['bottom'] = isset( $value['bottom'] ) ? $value['bottom'] : '';
                        $border['width']['left'] = isset( $value['left'] ) ? $value['left'] : '';
                    }
                    break;

                case 'radius':
                    if ( ! is_array( $value ) ) {
                        $border['radius'] = array(
                            'top_left'		=> $value,
                            'top_right'		=> $value,
                            'bottom_left'	=> $value,
                            'bottom_right'	=> $value,
                        );

                        if ( isset( $settings->{$name . '_medium'} ) ) {
                            $value = $settings->{$name . '_medium'};
                            $border_medium['radius'] = array(
                                'top_left'		=> $value,
                                'top_right'		=> $value,
                                'bottom_left'	=> $value,
                                'bottom_right'	=> $value,
                            );
                        }
                        if ( isset( $settings->{$name . '_responsive'} ) ) {
                            $value = $settings->{$name . '_responsive'};
                            $border_responsive['radius'] = array(
                                'top_left'		=> $value,
                                'top_right'		=> $value,
                                'bottom_left'	=> $value,
                                'bottom_right'	=> $value,
                            );
                        }
                    }
                    break;

                case 'shadow':
                    if ( is_array( $value ) ) {
                        $border['shadow'] = array(
                            'horizontal'	=> $value['horizontal'],
                            'vertical'		=> $value['vertical'],
                            'blur'			=> $value['blur'],
                            'spread'		=> $value['spread'],
                        );
                    }
                    break;

                case 'shadow_horizontal':
                    $border['shadow']['horizontal'] = $value;
                    if ( isset( $settings->{$name . '_medium'} ) ) {
                        $border_medium['shadow']['horizontal'] = $settings->{$name . '_medium'};
                    }
                    if ( isset( $settings->{$name . '_responsive'} ) ) {
                        $border_responsive['shadow']['horizontal'] = $settings->{$name . '_responsive'};
                    }
                    break;

                case 'shadow_vertical':
                    $border['shadow']['vertical'] = $value;
                    if ( isset( $settings->{$name . '_medium'} ) ) {
                        $border_medium['shadow']['vertical'] = $settings->{$name . '_medium'};
                    }
                    if ( isset( $settings->{$name . '_responsive'} ) ) {
                        $border_responsive['shadow']['vertical'] = $settings->{$name . '_responsive'};
                    }
                    break;

                case 'shadow_blur':
                    $border['shadow']['blur'] = $value;
                    if ( isset( $settings->{$name . '_medium'} ) ) {
                        $border_medium['shadow']['blur'] = $settings->{$name . '_medium'};
                    }
                    if ( isset( $settings->{$name . '_responsive'} ) ) {
                        $border_responsive['shadow']['blur'] = $settings->{$name . '_responsive'};
                    }
                    break;

                case 'shadow_spread':
                    $border['shadow']['spread'] = $value;
                    if ( isset( $settings->{$name . '_medium'} ) ) {
                        $border_medium['shadow']['spread'] = $settings->{$name . '_medium'};
                    }
                    if ( isset( $settings->{$name . '_responsive'} ) ) {
                        $border_responsive['shadow']['spread'] = $settings->{$name . '_responsive'};
                    }
                    break;

                case 'shadow_color':
                    $border['shadow']['color'] = $value;
                    $border_medium['shadow']['color'] = $value;
                    $border_responsive['shadow']['color'] = $value;
                    break;
            }

            unset( $settings->{$name} );
        }

        if ( ! empty( $border ) ) {
            $settings->{$new_field} = $border;
        }
        if ( ! empty( $border_medium ) ) {
            $settings->{$new_field . '_medium'} = $border_medium;
        }
        if ( ! empty( $border_responsive ) ) {
            $settings->{$new_field . '_responsive'} = $border_responsive;
        }

        return $settings;
	}
	
    public static function handle_border_dimension_field( $settings, $field = '', $new_field = '' ) {
        if ( empty( $field ) ) {
            return;
        }
  
        $dimensions = array( '_top', '_right', '_bottom', '_left' );
        $devices = array( '', '_medium', '_responsive' );
        
        foreach ( $devices as $device ) {
            if ( ! isset( $settings->{$new_field . $device} ) ) {
                $settings->{$new_field . $device} = array();
            }

            foreach ( $dimensions as $dimension ) {
                if ( isset( $settings->{$field . $dimension . $device} ) ) {
                    $settings->{$new_field . $device}['width'][ str_replace( '_', '', $dimension ) ] = $settings->{$field . $dimension . $device};
				}
				unset( $settings->{$field . $dimension . $device} );
            }
        }

        return $settings;
	}

	public static function handle_link_field( $settings, $fields = array(), $new_field = '' ) {
		if ( ! is_object( $settings ) || empty( $fields ) || empty( $new_field ) ) {
			return $settings;
		}

		foreach ( $fields as $name => $field ) {
			if ( ! isset( $settings->{$name} ) ) {
				continue;
			}

			$type		= $field['type'];
			$value 		= isset( $field['value'] ) ? $field['value'] : $settings->{$name};
			$condition 	= isset( $field['condition'] ) ? $field['condition'] : true;

			if ( ! $condition ) {
				continue;
			}

			switch ( $type ) {
				case 'link':
					if ( $name != $new_field ) {
						// let's unset old field first.
						unset( $settings->{$name} );
					}
					// $value should be URL
					$settings->{$new_field} = $value;
					break;

				case 'target':
					// let's unset old field first.
					unset( $settings->{$name} );

					// $value should be _blank or _self
					$settings->{$new_field . '_target'} = $value;
					break;

				case 'nofollow':
					// let's unset old field first.
					unset( $settings->{$name} );

					// $value should be yes or no
					$settings->{$new_field . '_nofollow'} = $value;
					break;
			}
		}

		return $settings;
	}
	
    public static function handle_dimension_field( $settings, $field = '', $new_field = '' ) {
        if ( empty( $field ) ) {
            return;
        }
        $devices = array( '', '_medium', '_responsive' );
        foreach ( $devices as $device ) {
			if ( isset( $settings->{$field . $device} ) ) {
				$settings->{$new_field . $device} = $settings->{$field . $device};
			}
		}
        return $settings;
    }
}