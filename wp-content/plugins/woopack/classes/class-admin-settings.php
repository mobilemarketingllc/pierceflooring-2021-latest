<?php
/**
 * Handles logic for the admin settings page.
 *
 * @since 1.0.0
 */
final class WooPack_Admin_Settings {
	/**
	 * Cache options value.
	 *
	 * @since 1.3.9
	 * @var array $_options
	 * @access private
	 */
	static private $_options = array();

    /**
	 * Holds any errors that may arise from
	 * saving admin settings.
	 *
	 * @since 1.0.0
	 * @var array $errors
	 */
	static public $errors = array();

	/**
	 * Initializes the admin settings.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function init()
	{
		add_action( 'plugins_loaded', __CLASS__ . '::init_hooks' );
	}

	/**
	 * Adds the admin menu and enqueues CSS/JS if we are on
	 * the plugin's admin settings page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function init_hooks()
	{
		if ( ! is_admin() ) {
			return;
		}

        self::multisite_fallback();

        add_action( 'admin_menu',           __CLASS__ . '::menu' );
        add_action( 'network_admin_menu',   __CLASS__ . '::menu' );
        add_filter( 'all_plugins',          __CLASS__ . '::update_branding' );

		if ( isset( $_REQUEST['page'] ) && 'woopack-settings' == $_REQUEST['page'] ) {
            add_action( 'admin_enqueue_scripts', __CLASS__ . '::styles_scripts' );
			self::save();
		}
	}

    /**
	 * Enqueues the needed CSS/JS for WooPack admin settings page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function styles_scripts()
	{
		// Admin settings style.
		wp_enqueue_style( 'woopack-admin-settings', WOOPACK_URL . 'assets/css/admin-settings.css', array(), WOOPACK_VER );
	}

    /**
	 * Renders the admin settings menu.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function menu()
	{
        $admin_label = WooPack_Helper::get_admin_label();

		if ( current_user_can( 'manage_options' ) ) {

			$title = $admin_label;
			$cap   = 'manage_options';
			$slug  = 'woopack-settings';
			$func  = __CLASS__ . '::render';

			add_submenu_page( 'options-general.php', $title, $title, $cap, $slug, $func );
		}

        if ( current_user_can( 'manage_network_plugins' ) ) {

            $title = $admin_label;
    		$cap   = 'manage_network_plugins';
    		$slug  = 'woopack-settings';
    		$func  = __CLASS__ . '::render';

    		add_submenu_page( 'settings.php', $title, $title, $cap, $slug, $func );
        }
	}

    /**
	 * Renders the admin settings.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function render()
	{
		include WOOPACK_DIR . 'includes/admin-settings.php';
	}

    /**
	 * Renders the update message.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function render_update_message()
	{
		if ( ! empty( self::$errors ) ) {
			foreach ( self::$errors as $message ) {
				echo '<div class="error"><p>' . $message . '</p></div>';
			}
		}
		else if ( ! empty( $_POST ) && ! isset( $_POST['email'] ) ) {
			echo '<div class="updated"><p>' . esc_html__( 'Settings updated!', 'woopack' ) . '</p></div>';
		}
	}

    /**
	 * Renders the action for a form.
	 *
	 * @since 1.0.0
	 * @param string $type The type of form being rendered.
	 * @return void
	 */
	static public function get_form_action( $type = '' )
	{
		if ( is_network_admin() ) {
			return network_admin_url( '/settings.php?page=woopack-settings' . $type );
		}
		else {
			return admin_url( '/options-general.php?page=woopack-settings' . $type );
		}
	}

    /**
	 * Adds an error message to be rendered.
	 *
	 * @since 1.0.0
	 * @param string $message The error message to add.
	 * @return void
	 */
	static public function add_error( $message )
	{
		self::$errors[] = $message;
	}

    /**
	 * Returns an option from the database for
	 * the admin settings page.
	 *
	 * @since 1.0.0
	 * @param string $key The option key.
	 * @return mixed
	 */
    static public function get_option( $key, $network_override = true )
    {
		if ( isset( self::$_options[ $key ] ) ) {
			return self::$_options[ $key ];
		}

    	if ( is_network_admin() ) {
    		$value = get_site_option( $key );
    	}
        elseif ( ! $network_override && is_multisite() ) {
            $value = get_site_option( $key );
        }
        elseif ( $network_override && is_multisite() ) {
            $value = get_option( $key );
            $value = ( false === $value || (is_array($value) && in_array('disabled', $value) && get_option('woopack_override_ms') != 1) ) ? get_site_option( $key ) : $value;
        }
        else {
    		$value = get_option( $key );
		}
		
		self::$_options[ $key ] = $value;

        return $value;
    }

    /**
	 * Updates an option from the admin settings page.
	 *
	 * @since 1.0.0
	 * @param string $key The option key.
	 * @param mixed $value The value to update.
	 * @return mixed
	 */
    static public function update_option( $key, $value, $network_override = true )
    {
    	if ( is_network_admin() ) {
    		update_site_option( $key, $value );
    	}
        // Delete the option if network overrides are allowed and the override checkbox isn't checked.
		else if ( $network_override && is_multisite() && ! isset( $_POST['woopack_override_ms'] ) ) {
			delete_option( $key );
		}
        else {
    		update_option( $key, $value );
    	}
    }

    /**
	 * Delete an option from the admin settings page.
	 *
	 * @since 1.0.0
	 * @param string $key The option key.
	 * @param mixed $value The value to delete.
	 * @return mixed
	 */
    static public function delete_option( $key )
    {
    	if ( is_network_admin() ) {
    		delete_site_option( $key );
    	} else {
    		delete_option( $key );
    	}
    }

    /**
	 * Saves the admin settings.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function save()
	{
		// Only admins can save settings.
		if ( ! current_user_can('manage_options') ) {
			return;
		}

        self::save_license();
        self::save_white_label();
	}

    /**
	 * Saves the license.
	 *
	 * @since 1.0.0
	 * @access private
	 * @return void
	 */
    static private function save_license()
    {
        if ( isset( $_POST['woopack_license_key'] ) ) {

        	$old = get_option( 'woopack_license_key' );
            $new = $_POST['woopack_license_key'];

        	if( $old && $old != $new ) {
        		self::delete_option( 'woopack_license_status' ); // new license has been entered, so must reactivate
        	}

            self::update_option( 'woopack_license_key', $new );
        }
    }

    /**
	 * Saves the white label settings.
	 *
	 * @since 1.0.0
	 * @access private
	 * @return void
	 */
	static private function save_white_label()
	{
		if( ! isset( $_POST['woopack-settings-nonce'] ) || ! wp_verify_nonce( $_POST['woopack-settings-nonce'], 'woopack-settings' ) ) {
			return;
		}
		if ( isset( $_POST['woopack_plugin_name'] ) ) {
			self::update_option( 'woopack_plugin_name', sanitize_text_field( $_POST['woopack_plugin_name'] ) );
			self::update_option( 'woopack_plugin_desc', esc_textarea( $_POST['woopack_plugin_desc'] ) );
			self::update_option( 'woopack_plugin_author', sanitize_text_field( $_POST['woopack_plugin_author'] ) );
			self::update_option( 'woopack_plugin_uri', esc_url( $_POST['woopack_plugin_uri'] ) );
		}
		$admin_label            = isset( $_POST['woopack_admin_label'] ) ? sanitize_text_field( $_POST['woopack_admin_label'] ) : 'WooPack';
		$category_label         = isset( $_POST['woopack_builder_label'] ) ? sanitize_text_field( $_POST['woopack_builder_label'] ) : 'WooPack ' . __( 'Modules', 'woopack' );
		$support_link           = isset( $_POST['woopack_support_link'] ) ? esc_url_raw( $_POST['woopack_support_link'] ) : 'httsp://wpbeaveraddons.com/contact/';
		$list_modules_standard  = isset( $_POST['woopack_list_modules_with_standard'] ) ? absint( $_POST['woopack_list_modules_with_standard'] ) : 0;
		$hide_support_msg       = isset( $_POST['woopack_hide_support_msg'] ) ? absint( $_POST['woopack_hide_support_msg'] ) : 0;
		$hide_wl_form           = isset( $_POST['woopack_hide_form'] ) ? absint( $_POST['woopack_hide_form'] ) : 0;
		$hide_plugin            = isset( $_POST['woopack_hide_plugin'] ) ? absint( $_POST['woopack_hide_plugin'] ) : 0;
		self::update_option( 'woopack_admin_label', $admin_label );
		self::update_option( 'woopack_builder_label', $category_label );
		self::update_option( 'woopack_support_link', $support_link );
		self::update_option( 'woopack_list_modules_with_standard', $list_modules_standard );
		self::update_option( 'woopack_hide_support_msg', $hide_support_msg );
		self::update_option( 'woopack_hide_form', $hide_wl_form );
		self::update_option( 'woopack_hide_plugin', $hide_plugin );
	}

    /**
	 * Update branding.
	 *
	 * @since 1.0.0
	 * @return array
	 */
    static public function update_branding( $all_plugins )
    {

    	$woopack_plugin_path = plugin_basename(  WOOPACK_DIR . 'woopack.php' );

    	$all_plugins[$woopack_plugin_path]['Name']           = self::get_option( 'woopack_plugin_name' ) ? self::get_option( 'woopack_plugin_name' ) : $all_plugins[$woopack_plugin_path]['Name'];
    	$all_plugins[$woopack_plugin_path]['PluginURI']      = self::get_option( 'woopack_plugin_uri' ) ? self::get_option( 'woopack_plugin_uri' ) : $all_plugins[$woopack_plugin_path]['PluginURI'];
    	$all_plugins[$woopack_plugin_path]['Description']    = self::get_option( 'woopack_plugin_desc' ) ? self::get_option( 'woopack_plugin_desc' ) : $all_plugins[$woopack_plugin_path]['Description'];
    	$all_plugins[$woopack_plugin_path]['Author']         = self::get_option( 'woopack_plugin_author' ) ? self::get_option( 'woopack_plugin_author' ) : $all_plugins[$woopack_plugin_path]['Author'];
    	$all_plugins[$woopack_plugin_path]['AuthorURI']      = self::get_option( 'woopack_plugin_uri' ) ? self::get_option( 'woopack_plugin_uri' ) : $all_plugins[$woopack_plugin_path]['AuthorURI'];
    	$all_plugins[$woopack_plugin_path]['Title']          = self::get_option( 'woopack_plugin_name' ) ? self::get_option( 'woopack_plugin_name' ) : $all_plugins[$woopack_plugin_path]['Title'];
    	$all_plugins[$woopack_plugin_path]['AuthorName']     = self::get_option( 'woopack_plugin_author' ) ? self::get_option( 'woopack_plugin_author' ) : $all_plugins[$woopack_plugin_path]['AuthorName'];

    	if ( self::get_option('woopack_hide_plugin') == 1 ) {
    		unset( $all_plugins[$woopack_plugin_path] );
    	}

    	return $all_plugins;
    }

    /**
     * Fallback for license key option for multisite.
     *
     * @since 1.0.0
     * @access private
     * @return mixed
     */
    static private function multisite_fallback()
    {
        if ( is_multisite() && is_network_admin() ) {

            $license_key    = get_option( 'woopack_license_key' );
            $license_status = get_option( 'woopack_license_status' );

            if ( $license_key != '' ) {
                self::update_option( 'woopack_license_key', $license_key );
            }
            if ( $license_status != '' ) {
                self::update_option( 'woopack_license_status', $license_status );
            }

        }
    }

}

WooPack_Admin_Settings::init();
