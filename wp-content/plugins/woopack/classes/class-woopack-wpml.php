<?php

class WooPack_WPML {
    static public function init()
    {
        add_filter( 'wpml_beaver_builder_modules_to_translate', __CLASS__ . '::translate_fields', 10, 1 );
    }

    static public function translate_fields( $modules )
    {
        $config = array(
            'product-grid'      => array(
                'fields'                => array(
                    array(
                        'field'             => 'button_text',
                        'type'              => __('Product Grid - Button Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                    array(
                        'field'             => 'taxonomy_custom_text',
                        'type'              => __('Product Grid - Taxonomy Custom Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                    array(
                        'field'             => 'quick_view_custom_text',
                        'type'              => __('Product Grid - Quick View Custom Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                    array(
                        'field'             => 'no_results_message',
                        'type'              => __('Product Grid - No Pagination Results Message', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                ),
            ),
            'product-carousel'  => array(
                'fields'                => array(
                    array(
                        'field'             => 'button_text',
                        'type'              => __('Product Carousel - Button Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                    array(
                        'field'             => 'taxonomy_custom_text',
                        'type'              => __('Product Carousel - Taxonomy Custom Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                    array(
                        'field'             => 'quick_view_custom_text',
                        'type'              => __('Product Carousel - Quick View Custom Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                ),
            ),
            'single-product'    => array(
                'fields'                => array(
                    array(
                        'field'             => 'button_text',
                        'type'              => __('Single Product - Button Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                    array(
                        'field'             => 'button_link',
                        'type'              => __('Single Product - Button Link', 'woopack'),
                        'editor_type'       => 'LINK'
                    ),
                    array(
                        'field'             => 'taxonomy_custom_text',
                        'type'              => __('Single Product - Taxonomy Custom Text', 'woopack'),
                        'editor_type'       => 'LINE'
                    ),
                ),
            ),
        );

        foreach ( $config as $module_name => $module_fields ) {
            $module_fields['conditions'] = array( 'type' => $module_name );
            $modules[$module_name] = $module_fields;
        }

        return $modules;
    }
}
WooPack_WPML::init();
