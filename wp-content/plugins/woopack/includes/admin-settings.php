<?php
/**
 * WooPack admin settings page.
 *
 * @since 1.0.0
 * @package woopack
 */

?>

<?php

$license 	  = self::get_option( 'woopack_license_key' );
$status 	  = self::get_option( 'woopack_license_status' );
$current_tab  = isset( $_REQUEST['tab'] ) ? $_REQUEST['tab'] : 'general';

?>

<div class="wrap">

    <h2>
        <?php
            $admin_label = self::get_option( 'woopack_admin_label' );
            $admin_label = trim( $admin_label ) !== '' ? trim( $admin_label ) : esc_html__( 'WooPack', 'woopack' );
            echo sprintf( esc_html__( '%s Settings', 'woopack' ), $admin_label );
        ?>
    </h2>

    <?php WooPack_Admin_Settings::render_update_message(); ?>

    <form method="post" id="woopack-settings-form" action="<?php echo self::get_form_action( '&tab=' . $current_tab ); ?>">

        <div class="icon32 icon32-woopack-settings" id="icon-pp"><br /></div>

        <h2 class="nav-tab-wrapper woopack-nav-tab-wrapper">

            <?php if ( is_network_admin() || ! is_multisite() ) { ?>
                <a href="<?php echo self::get_form_action( '&tab=general' ); ?>" class="nav-tab<?php echo ( $current_tab == 'general' ? ' nav-tab-active' : '' ); ?>"><?php esc_html_e( 'General', 'woopack' ); ?></a>
                <?php if ( ! self::get_option( 'woopack_hide_form' ) || self::get_option( 'woopack_hide_form' ) == 0 ) { ?>
                    <a href="<?php echo self::get_form_action( '&tab=white-label' ); ?>" class="nav-tab<?php echo ( $current_tab == 'white-label' ? ' nav-tab-active' : '' ); ?>"><?php esc_html_e( 'White Label', 'woopack' ); ?></a>
                <?php } ?>
            <?php } ?>
        </h2>

        <?php

        // General settings.
        if ( ! isset($_GET['tab']) || 'general' == $current_tab ) {
            include WOOPACK_DIR . 'includes/admin-settings-license.php';
        }

        // White Label settings.
        if ( 'white-label' == $current_tab ) {
            include WOOPACK_DIR . 'includes/admin-settings-wl.php';
        }
        ?>

    </form>

    <?php if ( ! self::get_option( 'woopack_hide_support_msg' ) || self::get_option( 'woopack_hide_support_msg' ) == 0 ) { ?>
    <hr />

    <h2><?php esc_html_e('Support', 'woopack'); ?></h2>
    <p>
        <?php
            $support_link = self::get_option( 'woopack_support_link' );
            $support_link = !empty( $support_link ) ? $support_link : 'https://wpbeaveraddons.com/contact/';
            esc_html_e('For submitting any support queries, feedback, bug reports or feature requests, please visit', 'woopack'); ?> <a href="<?php echo $support_link; ?>" target="_blank"><?php esc_html_e('this link', 'woopack'); ?></a>
    </p>
    <?php } ?>

</div>
