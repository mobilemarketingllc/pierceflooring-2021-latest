<?php
// ******************* Border *******************
// Box Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'box_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce.woopack-products-grid-wrap ul.products li.product,
						.fl-node-$id .woocommerce.woopack-products-carousel div.products div.product",
) );
// Box Border Hover - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'box_border_hover_group',
	'selector' 		=> ".fl-node-$id .woocommerce.woopack-products-grid-wrap ul.products li.product:hover,
						.fl-node-$id .woocommerce.woopack-products-carousel div.products div.product:hover",
) );

// ******************* Padding *******************
// Box Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'box_padding',
	'selector' 		=> ".fl-node-$id .woocommerce.woopack-products-grid-wrap ul.products li.product,
						.fl-node-$id .woocommerce.woopack-products-carousel div.products div.product",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'box_padding_top',
		'padding-right' 	=> 'box_padding_right',
		'padding-bottom' 	=> 'box_padding_bottom',
		'padding-left' 		=> 'box_padding_left',
	),
) );

if ( 'default' == $settings->product_align ) {
	if ( 1 == $settings->product_layout ) {
		$default_align = 'center';
	} elseif ( 2 == $settings->product_layout ) {
		$default_align = 'center';
	} elseif ( 3 == $settings->product_layout ) {
		$default_align = 'left';
	} elseif ( 4 == $settings->product_layout ) {
		$default_align = 'left';
	} else {
		$default_align = $settings->product_align;
	}
} else {
	$default_align = $settings->product_align;
}

	$product_col            = ! empty( $settings->product_columns ) ? $settings->product_columns : '3' ;
	$product_col_medium     = ! empty( $settings->product_columns_medium ) ? $settings->product_columns_medium : '2' ;
	$product_col_responsive = ! empty( $settings->product_columns_responsive ) ? $settings->product_columns_responsive : '1' ;
?>

.fl-node-<?php echo $id; ?> .woocommerce a {
	text-decoration: none;
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product,
.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color ); ?>
	max-width: none;
	clear: none !important;
	position: relative;
	text-align: <?php echo $default_align; ?>;
}

.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product:hover,
.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color_hover ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product,
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
		text-align : center;
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-grid-wrap ul.products li.product,
	.fl-node-<?php echo $id; ?> .woocommerce.woopack-products-carousel div.products div.product {
		text-align : center;
	}
}
