<?php
// ******************* Padding *******************
// Quick View Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'quick_view_padding',
	'selector' 		=> ".fl-node-$id .woopack-product-quick-view",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'quick_view_padding_top',
		'padding-right' 	=> 'quick_view_padding_right',
		'padding-bottom' 	=> 'quick_view_padding_bottom',
		'padding-left' 		=> 'quick_view_padding_left',
	),
) );

// Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce button.button.alt",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );

// Product Meta Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'meta_padding',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce .product_meta",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'meta_padding_top',
		'padding-right' 	=> 'meta_padding_right',
		'padding-bottom' 	=> 'meta_padding_bottom',
		'padding-left' 		=> 'meta_padding_left',
	),
) );

// ******************* Border *******************
// Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce button.button.alt",
) );

// ******************* Typography *******************
// Product Title Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'product_title_typography',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce div.product .product_title",
) );

// Short Description Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'short_description_typography',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce div.product .woocommerce-product-details__short-description",
) );

// Product Taxonomy Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'meta_typography',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce .product_meta",
) );

// Quick View Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'quick_view_typography',
	'selector' 		=> ".fl-node-$id .woopack-product-quick-view p",
) );

// Button Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce button.button.alt",
) );


// Product Price Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'regular_price_typography',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce div.product .price del,
						.woopack-modal-content .fl-node-$id .woocommerce div.product .price .amount",
) );

// Sale Price Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'sale_price_typography',
	'selector' 		=> ".woopack-modal-content .fl-node-$id .woocommerce div.product .price ins .amount",
) );
?>
.woopack-product-quick-view {
	position: absolute;
	width: 100%;
	z-index: 90;
	background-color: rgba(255,255,255,.5);
	bottom: 0;
	text-align: center;
	opacity: 0;
	transition: 0.3s ease-in-out;
}
.woopack-product-quick-view {
	width: 100%;
	color: #434343;
	cursor: pointer;
	padding: 15px;
}
.woopack-product-quick-view p {
	font-size: 14px;
	text-transform: capitalize;
	margin: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product:hover .woopack-product-quick-view,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product:hover .woopack-product-quick-view {
	opacity: 1;
}
.fl-node-<?php echo $id; ?> .woopack-product-quick-view {
	<?php WooPack_Helper::print_css( 'background-color', $settings->quick_view_bg_color ); ?>
	<?php if ( isset( $settings->quick_view_type ) && 'overlay' == $settings->quick_view_type ) { ?>
		top: 0;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-quick-view {
	<?php WooPack_Helper::print_css( 'color', $settings->quick_view_text_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-quick-view p {
	<?php WooPack_Helper::print_css( 'color', $settings->quick_view_text_color ); ?>
	<?php if ( isset( $settings->quick_view_type ) && 'overlay' == $settings->quick_view_type ) { ?>
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateX(-50%);
	<?php } ?>
}

<?php
// Quick View Modal
?>
div.woopack-modal-<?php echo $id; ?>.woopack-ajax-loaded .woopack-modal-overlay {
	<?php WooPack_Helper::print_css( 'background-color', $settings->quick_view_popup_overlay_bg_color ); ?>
}

/******************************************
 * Product Title
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .product_title {
	<?php WooPack_Helper::print_css( 'color', $settings->product_title_color ); ?>
	font-size: 26px;
	margin-top: 0;
	margin-bottom: 0;
}

/******************************************
 * Product Price
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .price del,
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .price .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .price ins .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
	text-decoration: none;
}

/******************************************
 * Product Rating
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce-product-rating .star-rating:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_default_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce-product-rating .star-rating span:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_color, ' !important' ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce-product-rating .woocommerce-review-link {
	display: none;
 }

/******************************************
 * Product Description
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product .woocommerce-product-details__short-description {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
}

/******************************************
 * Product Variations
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart table.variations {
	margin: 0 auto;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart table.variations .reset_variations {
	float: right;
	padding: 0;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart .woocommerce-variation-price {
	text-align: center;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart .woocommerce-variation-add-to-cart {
	margin-top: 10px;
    text-align: center;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart .woocommerce-variation-add-to-cart div.quantity {
	display: inline-block;
    float: none;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce div.product form.cart button.alt {
	display: inline-block;
	float: none;
	width: auto;
}

/******************************************
 * Button
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce button.button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>

	<?php if ( 'full_width' == $settings->button_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->button_width ) { ?>
		<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%' ); ?>
	<?php } else { ?>
		width: auto;
	<?php } ?>
	-webkit-transition: 0.3s ease-in-out;
		-moz-transition: 0.3s ease-in-out;
			transition: 0.3s ease-in-out;
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce button.button.alt:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce a.added_to_cart {
	background: none;
    font-weight: normal;
    color: inherit;
    border: 1px solid;
    margin-left: 4px;
}

/******************************************
 * Meta
 ******************************************/
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce .product_meta {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_text_color ); ?>
	<?php if ( 'yes' == $settings->meta_border && '' != $settings->meta_border_color ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->meta_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
}
.woopack-modal-content .fl-node-<?php echo $id; ?> .woocommerce .product_meta a {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_link_color ); ?>
}

@media (min-width: 768px) {
	div.woopack-modal-<?php echo $id; ?> .woopack-modal-inner {
		<?php WooPack_Helper::print_css( 'width', $settings->quick_view_popup_width_custom, 'px', 'custom' == $settings->quick_view_popup_width ); ?>
	}
}
