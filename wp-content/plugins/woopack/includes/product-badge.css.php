<?php if ( ! isset( $settings->layout ) || 'custom' !== $settings->layout ) : ?>

<?php
// ******************* Border *******************
// Sale Badge Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'sale_badge_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .onsale,
						.fl-node-$id .woocommerce div.products div.product .onsale",
) );
// Out of Stock Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'out_of_stock_border_group',
	'selector' 		=> ".fl-node-$id .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span",
) );
// ******************* Padding *******************
// Out of Stock Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'out_of_stock_padding',
	'selector' 		=> ".fl-node-$id .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'out_of_stock_padding_top',
		'padding-right' 	=> 'out_of_stock_padding_right',
		'padding-bottom' 	=> 'out_of_stock_padding_bottom',
		'padding-left' 		=> 'out_of_stock_padding_left',
	),
) );
// ******************* Typography *******************
// Sale Badge Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'sale_badge_typography',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .onsale,
						.fl-node-$id .woocommerce div.products div.product .onsale,
						.fl-node-$id .woocommerce .product span.onsale",
) );
// Out of Stock Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'out_of_stock_typography',
	'selector' 		=> ".fl-node-$id .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span",
) );
?>
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .onsale,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .onsale,
.fl-node-<?php echo $id; ?> .woocommerce .product span.onsale {
	position: absolute;
	<?php if ( 'top-left' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: 0 !important;
		bottom: auto !important;
		right: auto !important;
	<?php } elseif ( 'top-center' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: 50% !important;
		bottom: auto !important;
		right: auto !important;
		transform: translateX(-50%);
	<?php } elseif ( 'top-right' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: auto !important;
		bottom: auto !important;
		right: 0 !important;
	<?php } elseif ( 'bottom-left' == $settings->badge_position ) { ?>
		top: auto !important;
		left: 0 !important;
		bottom: 0 !important;
		right: auto !important;
	<?php } elseif ( 'bottom-center' == $settings->badge_position ) { ?>
		top: auto !important;
		left: 50% !important;
		bottom: 0 !important;
		right: auto !important;
		transform: translateX(-50%);
	<?php } elseif ( 'bottom-right' == $settings->badge_position ) { ?>
		top: auto !important;
		left: auto !important;
		bottom: 0 !important;
		right: 0 !important;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'background-color', $settings->badge_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->badge_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-left', $settings->badge_margin_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->badge_margin_left_right, 'px' ); ?>
	min-width: 0;
	min-height: 0;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .onsale,
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image img {
	opacity: 0.4;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock {
	position: absolute;
    display: flex;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span {
	<?php WooPack_Helper::print_css( 'background-color', $settings->out_of_stock_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->out_of_stock_color ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce .product span.onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_medium, 'px' ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .onsale,
	.fl-node-<?php echo $id; ?> .woocommerce .product span.onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_responsive, 'px' ); ?>
	}
}

<?php endif; ?>