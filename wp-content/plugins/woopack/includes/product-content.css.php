.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product.woopack-product-align-left,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product.woopack-product-align-left {
	text-align: left;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product.woopack-product-align-center,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product.woopack-product-align-center {
	text-align: center;
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product.woopack-product-align-right,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product.woopack-product-align-right {
	text-align: right;
}

<?php if ( ! isset( $settings->layout ) || 'custom' !== $settings->layout ) : ?>

<?php
// ******************* Padding *******************
// Content Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'content_padding',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .woopack-product-content,
						.fl-node-$id .woocommerce div.products div.product .woopack-product-content",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'content_padding_top',
		'padding-right' 	=> 'content_padding_right',
		'padding-bottom' 	=> 'content_padding_bottom',
		'padding-left' 		=> 'content_padding_left',
	),
) );
// Product Taxonomy Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'meta_padding',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .woopack-product-meta .product_meta,
						.fl-node-$id .woocommerce div.products div.product .woopack-product-meta .product_meta",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'meta_padding_top',
		'padding-right' 	=> 'meta_padding_right',
		'padding-bottom' 	=> 'meta_padding_bottom',
		'padding-left' 		=> 'meta_padding_left',
	),
) );
// ******************* Typography *******************
// Product Title Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'product_title_typography',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .woopack-product-title,
						.fl-node-$id .woocommerce div.products div.product .woopack-product-title",
) );
// Product Price Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'regular_price_typography',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .price .amount,
						.fl-node-$id .woocommerce div.products div.product .price .amount",
) );
// Sale Price Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'sale_price_typography',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .price ins .amount,
						.fl-node-$id .woocommerce div.products div.product .price ins .amount",
) );
// Short Description Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'short_description_typography',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .woopack-product-description,
						.fl-node-$id .woocommerce div.products div.product .woopack-product-description",
) );
// Product Taxonomy Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'meta_typography',
	'selector' 		=> ".fl-node-$id .woocommerce ul.products li.product .woopack-product-meta .product_meta,
						.fl-node-$id .woocommerce div.products div.product .woopack-product-meta .product_meta",
) );
// Rating Count Typography
FLBuilderCSS::typography_field_rule(
	array(
		'settings'     => $settings,
		'setting_name' => 'rating_count_taxonomy',
		'selector'     => ".fl-node-$id .woocommerce ul.products li.product .woocommerce-rating-count,
							.fl-node-$id .woocommerce div.products div.product .woocommerce-rating-count",
	)
);
?>

.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->product_title_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title a,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title a {
	<?php WooPack_Helper::print_css( 'color', $settings->product_title_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title a:hover,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title a:hover {
	<?php
	if ( isset( $settings->product_title_color_hover ) ) {
		WooPack_Helper::print_css( 'color', $settings->product_title_color_hover );
	}
	?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size, 'px !important' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_bottom, 'px !important' ); ?>

	<?php if ( $default_align ) { ?>
		<?php if ( 'center' == $default_align ) { ?>
			margin-left: auto !important;
			margin-right: auto !important;
		<?php } elseif ( 'left' == $default_align ) { ?>
			margin-right: auto !important;
			margin-left: 0 !important;
		<?php } elseif ( 'center' == $default_align ) { ?>
			margin-left: auto!important;
			margin-right: auto!important;
		<?php } elseif ( 'right' == $default_align ) { ?>
			margin-left: auto !important;
			margin-right: 0 !important;
		<?php } elseif ( 'left' == $default_align ) { ?>
			margin-right: auto !important;
			margin-left: 0 !important;
		<?php } ?>
	<?php } elseif ( 'right' == $settings->product_align ) { ?>
		margin-left: auto !important;
		margin-right: 0 !important;
	<?php } elseif ( 'left' == $settings->product_align ) { ?>
		margin-right: auto !important;
		margin-left: 0 !important;
	<?php } else { ?>
		margin-left: auto !important;
		margin-right: auto !important;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating:before,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_default_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating span:before,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating span:before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woocommerce-rating-count,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woocommerce-rating-count {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_count_color, ' !important' ); ?>
}

.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price del,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price del {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price .amount,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price ins .amount,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price ins .amount {
	text-decoration: none;
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-description,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-description {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom, 'px' ); ?>

	<?php if ( 2 == $settings->product_layout ) { ?>
		margin-top: 15px;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-meta .product_meta,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-meta .product_meta {
	<?php if ( $default_align && 'default' == $settings->product_align ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->product_align ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'color', $settings->meta_text_color ); ?>
	<?php if ( 'yes' == $settings->meta_border && '' != $settings->meta_border_color ) { ?>
		<?php WooPack_Helper::print_css( 'border-color', $settings->meta_border_color ); ?>
	<?php } else { ?>
		border: none;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-meta .product_meta .posted_in a,
.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-meta .product_meta .posted_in a {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_link_color ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->product_title_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_medium, 'px !important' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_bottom_medium, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-description,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-description {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_medium, 'px' ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-title,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->product_title_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .star-rating,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .star-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_responsive, 'px !important' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_bottom_responsive, 'px !important' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .price,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce ul.products li.product .woopack-product-description,
	.fl-node-<?php echo $id; ?> .woocommerce div.products div.product .woopack-product-description {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_responsive, 'px' ); ?>
	}
}
<?php endif; ?>