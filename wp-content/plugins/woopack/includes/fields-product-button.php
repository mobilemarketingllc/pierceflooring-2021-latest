<?php

/**
 * Button display option for product.
 *
 * @since 1.0.0
 */
function filter_product_button_settings( $settings ) {
	// Handle old Button Border setting.
	$settings = WooPack_Fields::handle_border_field( $settings, array(
		'button_border_style'   => array(
			'type'                  => 'style'
		),
		'button_border_width'   => array(
			'type'                  => 'width'
		),
		'button_border_color'   => array(
			'type'                  => 'color'
		),
		'button_border_radius'  => array(
			'type'                  => 'radius'
		)
	), 'button_border_group' );

	// Handle old Button font size setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'button_font'   => array(
			'type'          => 'font'
		),
		'button_font_size_custom'   => array(
			'type'          => 'font_size',
			'condition'     => ( isset( $settings->button_font_size ) && 'custom' == $settings->button_font_size )
		),
		'button_text_transform' => array(
			'type'          => 'text_transform',
			'condition'     => ( isset( $settings->button_text_transform ) && 'default' != $settings->button_text_transform )
		)
	), 'button_typography' );
	
	// Return the filtered settings.
	return $settings;
}

function woopack_product_button_fields()
{
	return apply_filters( 'woopack_product_button_fields', array(
		'button_structure'   => array(
			'title'             => __('Structure', 'woopack'),
			'fields'            => array(
				'button_alignment'		=> array(
					'type'              	=> 'align',
					'label'             	=> __('Alignment', 'woopack'),
					'default'           	=> '',
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce ul.products .woopack-product-action, .woocommerce div.products .woopack-product-action, #payment .form-row.place-order, .woopack-product-category .woopack-product-category__button, .woopack-my-account .woocommerce .woocommerce-form-register .form-row:last-child, .woopack-my-account .woocommerce .woocommerce-form-login .form-row:last-child, .woopack-my-account .woocommerce form .woopack-my-account-button',
						'property'          	=> 'text-align',
					),
				),
				'button_width'			=> array(
					'type'                  => 'select',
					'label'                 => __('Width', 'woopack'),
					'default'               => 'auto',
					'options'               => array(
						'auto'                  => __('Auto', 'woopack'),
						'full_width'            => __('Full Width', 'woopack'),
						'custom'                => __('Custom', 'woopack'),
					),
					'toggle'                => array(
						'custom'                => array(
							'fields'                => array('button_width_custom')
						),
					),
				),
				'button_width_custom'	=> array(
					'type'              => 'unit',
					'label'                 => __('Custom Width', 'woopack'),
					'units'					=> array('%'),
					'slider'				=> true,
					'responsive' 			=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button, .woopack-my-account .woocommerce .form-row button',
						'property'          	=> 'width',
						'unit'					=> '%'
					),
				),
				'button_margin_top'		=> array(
					'type'                  => 'unit',
					'label'                 => __('Margin Top', 'woopack'),
					'default'				=> 10,
					'units'					=> array('px'),
					'slider'				=> true,
					'responsive'        	=> true,
				),
				'button_margin_bottom'	=> array(
					'type'                  => 'unit',
					'label'                 => __('Margin Bottom', 'woopack'),
					'default'				=> 15,
					'units'					=> array('px'),
					'slider'				=> true,
					'responsive'        	=> true,
				),
			),
		),
		'button_color'      => array(
			'title'             => __('Colors', 'woopack'),
			'collapsed'			=> true,
			'fields'            => array(
				'button_bg_color'       => array(
					'type'              	=> 'color',
					'label'             	=> __('Background Color', 'woopack'),
					'show_reset'        	=> true,
					'show_alpha'        	=> true,
					'preview'           	=> array(
						'type' 					=> 'css',
						'selector'				=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button, .woopack-my-account .woocommerce .form-row button',
						'property'				=> 'background-color',
					),
				),
				'button_bg_color_hover'	=> array(
					'type'              	=> 'color',
					'label'             	=> __('Background Hover Color', 'woopack'),
					'show_reset'        	=> true,
					'show_alpha'        	=> true,
				),
				'button_color'          => array(
					'type'              	=> 'color',
					'label'             	=> __('Text Color', 'woopack'),
					'show_reset'        	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button, .woopack-my-account .woocommerce .form-row button',
						'property'          	=> 'color',
					),
				),
				'button_color_hover'    => array(
					'type'              	=> 'color',
					'label'             	=> __('Text Hover Color', 'woopack'),
					'show_reset'        	=> true,
				),
			),
		),
		'button_border'     => array(
			'title'             => __('Border', 'woopack'),
			'collapsed'			=> true,
			'fields'            => array(
				'button_border_group'       => array(
					'type'                      => 'border',
					'label'                     => __('Border Style', 'woopack'),
					'responsive'                => true,
					'preview'                   => array(
						'type'                      => 'css',
						'selector' 		            => '.woocommerce .products .woopack-product-action a.button, 
														.woocommerce .products .woopack-product-action a.add_to_cart_button, 
														.woocommerce .products .woopack-product-action a.added_to_cart, 
														.woocommerce-checkout #place_order, 
														.woopack-product-category .woopack-product-category__button,
														.woopack-my-account .woocommerce .form-row button',
					),
				),
				'button_border_color_hover'	=> array(
					'type' 						=> 'color',
					'label' 					=> __('Border Hover Color', 'woopack'),
					'show_reset' 				=> true,
				),
			),
		),
		'button_padding'    => array(
			'title'             => __('Padding', 'woopack'),
			'collapsed'			=> true,
			'fields'            => array(
				'button_padding'    => array(
					'type'              	=> 'dimension',
					'label' 				=> __('Padding', 'woopack'),
					'units'					=> array('px'),
					'slider'				=> true,
					'preview' 				=> array(
						'type' 					=> 'css',
						'selector'				=> '.woocommerce .products .woopack-product-action a.button, .woocommerce .products .woopack-product-action a.add_to_cart_button, .woocommerce .products .woopack-product-action a.added_to_cart, #place_order, .woopack-product-category .woopack-product-category__button, .woopack-my-account .woocommerce .form-row button',
						'property'				=> 'padding',
						'unit' 					=> 'px'
					),
					'responsive' 			=> true,
				),
			),
		),
		'button_font'       => array(
			'title'             => __('Typography', 'woopack'),
			'collapsed'			=> true,
			'fields'            => array(
				'button_typography' => array(
					'type'        	    => 'typography',
					'label'       	    => __( 'Button Typography', 'woopack' ),
					'responsive'  	    => true,
					'preview'           => array(
						'type'              => 'css',
						'selector' 		    => '.woocommerce .products .woopack-product-action a.button, 
												.woocommerce .products .woopack-product-action a.add_to_cart_button,
												.woocommerce .products .woopack-product-action a.added_to_cart,
												#place_order,
												.woopack-product-category .woopack-product-category__button,
												.woopack-my-account .woocommerce .form-row button',
					),
				),
			),
		),
	) );
}
