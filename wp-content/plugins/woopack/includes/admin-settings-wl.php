<?php
/**
 * WooPack admin settings white-label tab.
 *
 * @since 1.0.0
 * @package woopack
 */

?>

<?php if ( ! self::get_option('woopack_hide_form') || self::get_option('woopack_hide_form') == 0 ) { ?>

	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Plugin Name', 'woopack'); ?>
				</th>
				<td>
					<input id="woopack_plugin_name" name="woopack_plugin_name" type="text" class="regular-text" value="<?php esc_attr_e( self::get_option('woopack_plugin_name') ); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Plugin Description', 'woopack'); ?>
				</th>
				<td>
					<textarea id="woopack_plugin_desc" name="woopack_plugin_desc" style="width: 25em;"><?php echo self::get_option('woopack_plugin_desc'); ?></textarea>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Developer / Agency Name', 'woopack'); ?>
				</th>
				<td>
					<input id="woopack_plugin_author" name="woopack_plugin_author" type="text" class="regular-text" value="<?php echo self::get_option('woopack_plugin_author'); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Website URL', 'woopack'); ?>
				</th>
				<td>
					<input id="woopack_plugin_uri" name="woopack_plugin_uri" type="text" class="regular-text" value="<?php echo esc_url( self::get_option('woopack_plugin_uri') ); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Admin Label', 'woopack'); ?>
				</th>
				<td>
					<input id="woopack_admin_label" name="woopack_admin_label" type="text" class="regular-text" value="<?php echo self::get_option('woopack_admin_label'); ?>" placeholder="WooPack" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Category in Panel', 'woopack'); ?>
				</th>
				<td>
					<input id="woopack_builder_label" name="woopack_builder_label" type="text" class="regular-text" value="<?php echo self::get_option('woopack_builder_label'); ?>" placeholder="WooPack <?php _e( 'Modules', 'woopack' ); ?>" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<?php esc_html_e('Support link', 'woopack'); ?>
				</th>
				<td>
					<input id="woopack_support_link" name="woopack_support_link" type="text" class="regular-text" value="<?php echo self::get_option('woopack_support_link'); ?>" placeholder="https://wpbeaveraddons.com/contact/" />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<label for="woopack_list_modules_with_standard"><?php esc_html_e('List WooPack modules with Standard Modules', 'bb-powerpack'); ?></label>
				</th>
				<td>
					<input id="woopack_list_modules_with_standard" name="woopack_list_modules_with_standard" type="checkbox" value="1" <?php echo self::get_option('woopack_list_modules_with_standard') == 1 ? 'checked="checked"' : '' ?> />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<label for="woopack_hide_support_msg"><?php esc_html_e('Hide Support message', 'woopack'); ?></label>
				</th>
				<td>
					<input id="woopack_hide_support_msg" name="woopack_hide_support_msg" type="checkbox" value="1" <?php echo self::get_option('woopack_hide_support_msg') == 1 ? 'checked="checked"' : '' ?> />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<label for="woopack_hide_form"><?php esc_html_e('Hide white label settings', 'woopack'); ?></label>
				</th>
				<td>
					<input id="woopack_hide_form" name="woopack_hide_form" type="checkbox" value="1" <?php echo self::get_option('woopack_hide_form') == 1 ? 'checked="checked"' : '' ?> />
				</td>
			</tr>
			<tr valign="top">
				<th scope="row" valign="top">
					<label for="woopack_hide_plugin"><?php esc_html_e('Hide plugin', 'woopack'); ?></label>
				</th>
				<td>
					<input id="woopack_hide_plugin" name="woopack_hide_plugin" type="checkbox" value="<?php echo absint( self::get_option('woopack_hide_plugin') ); ?>" <?php echo self::get_option('woopack_hide_plugin') == 1 ? 'checked="checked"' : '' ?> />
				</td>
			</tr>
		</tbody>
	</table>
	<script>
	jQuery(document).ready(function(){
		jQuery('#woopack_hide_plugin').on('change', function() {
			if ( jQuery(this).is(':checked') ) {
				jQuery(this).val(1);
			} else {
				jQuery(this).val(0);
			}
		});
	});
	</script>
	<p style="">
		<?php esc_html_e( 'You can hide this form to prevent your client from seeing these settings.', 'woopack' ); ?><br />
		<?php esc_html_e( 'To re-enable the form, you will need to first deactivate the plugin and activate it again.', 'woopack' ); ?>
	</p>
	<?php submit_button(); ?>
	<?php wp_nonce_field('woopack-settings', 'woopack-settings-nonce'); ?>

<?php } else { ?>

    <?php if ( isset($_GET['tab']) && 'white-label' == $_GET['tab'] ) { ?>

        <p style=""><?php esc_html_e( 'Done! To re-enable the form, you will need to first deactivate the plugin and activate it again.', 'woopack' ); ?></p>

    <?php } ?>

<?php } ?>
