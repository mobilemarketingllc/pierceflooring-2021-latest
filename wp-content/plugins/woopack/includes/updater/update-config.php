<?php

define( 'WOOPACK_SL_URL', 'https://wpbeaveraddons.com' );

define( 'WOOPACK_ITEM_NAME', 'WooPack for Beaver Builder' );

if( !class_exists( 'WooPack_Plugin_Updater' ) ) {
	// load our custom updater
	include('class-plugin-updater.php' );
}

function woopack_get( $key ) {
	if ( is_network_admin() ) {
		return get_site_option( $key );
	} else {
		return get_option( $key );
	}
}

function woopack_update( $key, $value ) {
	if ( is_network_admin() ) {
		return update_site_option( $key, $value );
	} else {
		return update_option( $key, $value );
	}
}

function woopack_delete( $key ) {
	if ( is_network_admin() ) {
		return delete_site_option( $key );
	} else {
		return delete_option( $key );
	}
}

function woopack_get_license_key() {
	if ( defined( 'WOOPACK_LICENSE_KEY' ) ) {
		return WOOPACK_LICENSE_KEY ? trim( WOOPACK_LICENSE_KEY ) : '';
	} else {
		return trim( woopack_get( 'woopack_license_key' ) );
	}
}

function woopack_license( $action = '', $license_key = '' ) {
	$license = trim( $license_key );

	if ( empty( $license ) ) {
		$license = woopack_get_license_key();
	}

	// data to send in our API request
	$api_params = array(
		'edd_action'=> $action,
		'license' 	=> $license,
		'item_name' => urlencode( WOOPACK_ITEM_NAME ), // the name of our product in EDD
		'url'       => network_home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( WOOPACK_SL_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	return $response;
}

function woopack_plugin_updater() {

	// retrieve our license key from the DB
	$license_key = trim( woopack_get( 'woopack_license_key' ) );

	// setup the updater
	$edd_updater = new WooPack_Plugin_Updater( WOOPACK_SL_URL, WOOPACK_DIR . '/woopack.php', array(
			'version' 	=> WOOPACK_VER, 					// current version number
			'license' 	=> $license_key, 						// license key (used woopack_get above to retrieve from DB)
			'item_name' => WOOPACK_ITEM_NAME, 			// name of this plugin
			'author' 	=> 'Team IdeaBox - Beaver Addons' 	// author of this plugin
		)
	);

}
add_action( 'admin_init', 'woopack_plugin_updater', 0 );

/************************************
* this illustrates how to activate
* a license key
*************************************/

function woopack_activate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['woopack_license_key'] ) ) {

		// run a quick security check
	 	if( ! isset( $_POST['woopack_nonce'] ) || ! wp_verify_nonce( $_POST['woopack_nonce'], 'woopack_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( $_POST['woopack_license_key'] );

		$response = woopack_license( 'activate_license', $license );

		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.', 'woopack' );
			}

		} else {

			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			if ( false === $license_data->success ) {

				$message = woopack_license_messages( $license_data->error );

			}

		}

		// Check if anything passed on a message constituting a failure
		if ( ! empty( $message ) ) {
			$base_url = WooPack_Admin_Settings::get_form_action();
			$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );

			wp_redirect( $redirect );
			exit();
		}

		// $license_data->license will be either "valid" or "invalid"

		woopack_update( 'woopack_license_status', $license_data->license );
		wp_redirect( WooPack_Admin_Settings::get_form_action() );
		exit();
	}
}
add_action('admin_init', 'woopack_activate_license');

/***********************************************
* Illustrates how to deactivate a license key.
* This will descrease the site count
***********************************************/

function woopack_deactivate_license() {

	// listen for our activate button to be clicked
	if( isset( $_POST['woopack_license_deactivate'] ) && isset( $_POST['woopack_license_key'] ) ) {

		// run a quick security check
	 	if( ! isset( $_POST['woopack_nonce'] ) || ! wp_verify_nonce( $_POST['woopack_nonce'], 'woopack_nonce' ) )
			return; // get out if we didn't click the Activate button

		// retrieve the license from the database
		$license = trim( $_POST['woopack_license_key'] );

		$response = woopack_license( 'deactivate_license', $license );

		// make sure the response came back okay
		if ( is_wp_error( $response ) )
			return false;

		// decode the license data
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );

		// $license_data->license will be either "deactivated" or "failed"
		if( $license_data->license == 'deactivated' )
			woopack_delete( 'woopack_license_status' );

	}
}
add_action('admin_init', 'woopack_deactivate_license');

/************************************
* check if
* a license key is still valid
* so this is only needed if we
* want to do something custom
*************************************/

function woopack_check_license() {

	global $wp_version;

	$license = trim( woopack_get( 'woopack_license_key' ) );
	$license_status = trim( woopack_get( 'woopack_license_status' ) );

	if ( '' == $license_status || 'invalid' == $license_status ) {
		//return 'invalid';
	}

	$api_params = array(
		'edd_action' => 'check_license',
		'license' 	=> $license,
		'item_name' => urlencode( WOOPACK_ITEM_NAME ),
		'url'       => network_home_url()
	);

	// Call the custom API.
	$response = wp_remote_post( WOOPACK_SL_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

	if ( is_wp_error( $response ) )
		return false;

	$license_data = json_decode( wp_remote_retrieve_body( $response ) );

	if( $license_data->license == 'valid' ) {
		return 'valid';
		// this license is still valid
	} else {
		return 'invalid';
		// this license is no longer valid
	}
}

function woopack_license_messages( $status ) {
	$message = '';

	switch ( $status ) {

		case 'expired' :

			$message = sprintf(
				__('Your license key expired on %s.', 'woopack'),
				date_i18n(get_option('date_format'), strtotime($license_data->expires, current_time('timestamp')))
			);
			break;

		case 'revoked' :
		case 'disabled':

			$message = __('Your license key has been disabled.', 'woopack');
			break;

		case 'missing' :
		case 'invalid' :

			$message = __('Invalid license.', 'woopack');
			break;

		case 'site_inactive' :

			$message = __('Your license is not active for this URL.', 'woopack');
			break;

		case 'item_name_mismatch' :

			$message = sprintf(__('This appears to be an invalid license key for %s.', 'woopack'), WOOPACK_ITEM_NAME);
			break;

		case 'no_activations_left':

			$message = __('Your license key has reached its activation limit.', 'woopack');
			break;

		default :

			$message = $license_data->error . ' ' . __( 'An error occurred, please try again.', 'woopack' );
			break;
	}

	return $message;
}

 /**
  * Show update message on plugins page
  */
function woopack_update_message( $plugin_data, $response ) {
	if ( 'invalid' == woopack_check_license() ) {

		$message  = '';
		$message .= '<p style="padding: 5px 10px; margin-top: 10px; background: #d54e21; color: #fff;">';
		$message .= __( '<strong>UPDATE UNAVAILABLE!</strong>', 'woopack' );
		$message .= '&nbsp;&nbsp;&nbsp;';
		$message .= __( 'Please activate the license key to enable automatic updates for this plugin.', 'woopack' );

		$message .= ' <a href="' . WOOPACK_SL_URL . '" target="_blank" style="color: #fff; text-decoration: underline;">';
		$message .= __( 'Buy Now', 'woopack' );
		$message .= ' &raquo;</a>';

		$message .= '</p>';

		echo $message;
	}
}
add_action( 'in_plugin_update_message-' . WOOPACK_PATH, 'woopack_update_message', 1, 2 );
