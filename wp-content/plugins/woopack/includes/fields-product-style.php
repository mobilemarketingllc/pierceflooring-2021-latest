<?php
/**
 * Style option for products.
 *
 * @since 1.0.0
 */
function filter_product_style_settings( $settings ) {
	// Handle old Button Border setting.
	$settings = WooPack_Fields::handle_border_field( $settings, array(
		'box_border_style'   => array(
			'type'                  => 'style'
		),
		'box_border_color'   => array(
			'type'                  => 'color'
		),
		'box_border_radius'  => array(
			'type'                  => 'radius'
		),
		'box_shadow_h'      => array(
			'type'              => 'shadow_horizontal',
			'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
		),
		'box_shadow_v'      => array(
			'type'              => 'shadow_vertical',
			'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
		),
		'box_shadow_blur'   => array(
			'type'              => 'shadow_blur',
			'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
		),
		'box_shadow_spread'      => array(
			'type'              => 'shadow_spread',
			'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
		),
		'box_shadow_color'      => array(
			'type'              => 'shadow_color',
			'condition'         => isset( $settings->box_shadow ) ? 'yes' == $settings->box_shadow : false
		)
	), 'box_border_group' );

	$settings = WooPack_Fields::handle_border_dimension_field( $settings, 'box_border', 'box_border_group' );
	
	// Handle old Button Border Hover (Shadow) setting.
	$settings = WooPack_Fields::handle_border_field( $settings, array(
		'box_shadow_hover_h'      => array(
			'type'              => 'shadow_horizontal',
			'condition'         => isset( $settings->box_shadow_hover ) ? 'yes' == $settings->box_shadow_hover : false
		),
		'box_shadow_hover_v'      => array(
			'type'              => 'shadow_vertical',
			'condition'         => isset( $settings->box_shadow_hover ) ? 'yes' == $settings->box_shadow_hover : false
		),
		'box_shadow_hover_blur'   => array(
			'type'              => 'shadow_blur',
			'condition'         => isset( $settings->box_shadow_hover ) ? 'yes' == $settings->box_shadow_hover : false
		),
		'box_shadow_hover_spread'      => array(
			'type'              => 'shadow_spread',
			'condition'         => isset( $settings->box_shadow_hover ) ? 'yes' == $settings->box_shadow_hover : false
		),
		'box_shadow_hover_color'      => array(
			'type'              => 'shadow_color',
			'condition'         => isset( $settings->box_shadow_hover ) ? 'yes' == $settings->box_shadow_hover : false
		)
	), 'box_border_hover_group' );

	// Handle old Sale Badge Border setting.
	$settings = WooPack_Fields::handle_border_field( $settings, array(
		'sale_badge_border_style'   => array(
			'type'                  => 'style'
		),
		'sale_badge_border_width'   => array(
			'type'                  => 'width'
		),
		'sale_badge_border_color'   => array(
			'type'                  => 'color'
		),
		'sale_badge_border_radius'  => array(
			'type'                  => 'radius'
		)
	), 'sale_badge_border_group' );

	// Handle old Out of Stock Border setting.
	$settings = WooPack_Fields::handle_border_field( $settings, array(
		'out_of_stock_border_style'		=> array(
			'type'                  		=> 'style'
		),
		'out_of_stock_border_width'		=> array(
			'type'                  		=> 'width'
		),
		'out_of_stock_border_color'		=> array(
			'type'                  		=> 'color'
		),
		'out_of_stock_border_radius'	=> array(
			'type'                  		=> 'radius'
		)
	), 'out_of_stock_border_group' );	

	// Padding
	$settings = WooPack_Fields::handle_dimension_field( $settings, 'quick_view_padding', 'quick_view_padding_top' );
	$settings = WooPack_Fields::handle_dimension_field( $settings, 'quick_view_padding', 'quick_view_padding_bottom' );
	$settings = WooPack_Fields::handle_dimension_field( $settings, 'quick_view_padding', 'quick_view_padding_left' );
	$settings = WooPack_Fields::handle_dimension_field( $settings, 'quick_view_padding', 'quick_view_padding_right' );

	// Return the filtered settings.
	return $settings;
}

function woopack_product_style_fields()
{
	return apply_filters( 'woopack_product_style_fields', array(
        'box'                  => array(
            'title'     			=> __('Box', 'woopack'),
            'fields'    			=> array(
                'content_bg_color'  	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Background Color', 'woopack'),
                    'default'           	=> '',
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'				=> 'background-color',
                    ),
                ),
                'content_bg_color_hover'=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Background Hover Color', 'woopack'),
                    'default'           	=> '',
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                ),
                'box_padding'   		=> array(
                    'type'              	=> 'dimension',
                    'label' 				=> __('Padding', 'woopack'),
					'units'					=> array('px'),
					'slider'				=> true,
                    'responsive' 			=> true,
					'preview' 				=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product, .woocommerce div.products div.product',
                        'property'      		=> 'padding',
                        'unit'					=> 'px',
                    ),
                ),
            ),
		),
        'box_border'           => array(
			'title'         		=> __('Box Border', 'woopack'),
			'collapsed'				=> true,
            'fields'        		=> array(
				'box_border_group'			=> array(
					'type'                  => 'border',
					'label'                 => __('Border Style', 'woopack'),
					'responsive'            => true,
					'preview'               => array(
						'type'                  => 'css',
						'selector' 		        => '.woocommerce ul.products li.product,
													.woocommerce div.products div.product',
					),
				),
				'box_border_hover_group'	=> array(
					'type'                  	=> 'border',
					'label'                 	=> __('Border Hover Style', 'woopack'),
					'responsive'            	=> true,
					'preview'               	=> array(
						'type'                  	=> 'css',
						'selector' 		        	=> '.woocommerce ul.products li.product:hover,
														.woocommerce div.products div.product:hover',
					),
				),
            ),
        ),
        'content_style'        => array(
			'title'             	=> __('Content', 'woopack'),
			'collapsed'				=> true,
            'fields'            	=> array(
                'content_padding'	=> array(
                    'type'              	=> 'dimension',
                    'label' 				=> __('Padding', 'woopack'),
					'units'					=> array('px'),
					'slider'				=> true,
                    'responsive' 			=> true,
					'preview' 				=> array(
                        'type'              	=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-content, .woocommerce div.products div.product .woopack-product-content',
                        'property'          	=> 'padding',
                        'unit' 					=> 'px'
                    ),
                ),
            ),
        ),
        'sale_badge_style'     => array(
			'title'             	=> __('Sale Badge', 'woopack'),
			'collapsed'				=> true,
            'fields'            	=> array(
                'badge_position'        	=> array(
                    'type'                  	=> 'select',
                    'label'  			    	=> __('Sale Badge Position', 'woopack'),
                    'default'               	=> 'top-left',
                    'options' 		        	=> array(
                        'top-left'              	=> __('Top Left', 'woopack'),
                        'top-center'              	=> __('Top Center', 'woopack'),
                        'top-right'             	=> __('Top Right', 'woopack'),
                        'bottom-left'           	=> __('Bottom Left', 'woopack'),
                        'bottom-center'           	=> __('Bottom Center', 'woopack'),
                        'bottom-right'          	=> __('Bottom Right', 'woopack'),
                    ),
                ),
                'badge_bg_color'			=> array(
                    'type'              		=> 'color',
                    'label'             		=> __('Background Color', 'woopack'),
                    'show_reset'        		=> true,
                    'show_alpha'        		=> true,
                    'preview'           		=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'					=> 'background-color',
                    ),
                ),
                'badge_color'           	=> array(
                    'type'              		=> 'color',
                    'label'             		=> __('Text Color', 'woopack'),
                    'show_reset'        		=> true,
                    'preview'           		=> array(
                        'type'              		=> 'css',
                        'selector'          		=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
                        'property'          		=> 'color',
                    ),
                ),
				'badge_margin_left_right'	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Horizontal Spacing', 'woopack'),
					'slider'					=> true,
					'units'						=> array( 'px' ),
                    'responsive' 				=> true,
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-left',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-right',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                ),
				'badge_margin_top_bottom'	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Vertical Spacing', 'woopack'),
					'slider'					=> true,
					'units'						=> array( 'px' ),
                    'responsive' 				=> true,
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-top',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'margin-bottom',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                ),
				'badge_padding_left_right'	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Padding Left/Right', 'woopack'),
					'slider'					=> true,
					'units'						=> array( 'px' ),
                    'responsive' 				=> true,
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-left',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-right',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                ),
				'badge_padding_top_bottom'	=> array(
                    'type'              		=> 'unit',
                    'label' 					=> __('Padding Top/Bottom', 'woopack'),
					'slider'					=> true,
					'units'						=> array( 'px' ),
                    'responsive' 				=> true,
                    'preview' 					=> array(
                        'type' 						=> 'css',
                        'rules'						=> array(
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-top',
		                        'unit' 						=> 'px'
							),
							array(
								'selector'					=> '.woocommerce ul.products li.product .onsale, .woocommerce div.products div.product .onsale',
		                        'property'					=> 'padding-bottom',
		                        'unit' 						=> 'px'
							),
						)
                    ),
                ),
				'sale_badge_border_group'	=> array(
					'type'                  	=> 'border',
					'label'                 	=> __('Border Style', 'woopack'),
					'responsive'            	=> true,
					'preview'               	=> array(
						'type'              	    => 'css',
						'selector' 		    	    => '.woocommerce ul.products li.product .onsale,
														.woocommerce div.products div.product .onsale',
					),
				),
            ),
        ),
        'out_of_stock_style'   => array(
			'title'             	=> __('Out of Stock', 'woopack'),
			'collapsed'				=> true,
            'fields'            	=> array(
                'out_of_stock_bg_color'		 => array(
                    'type'              		=> 'color',
                    'label'             		=> __('Background Color', 'woopack'),
                    'show_reset'        		=> true,
					'show_alpha'        		=> true,
					'default'					=> 'rgba(0,0,0,.7)',
                    'preview'           		=> array(
                        'type' 						=> 'css',
                        'selector'					=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'					=> 'background-color',
                    ),
                ),
                'out_of_stock_color'         => array(
                    'type'              		=> 'color',
                    'label'             		=> __('Text Color', 'woopack'),
					'show_reset'        		=> true,
					'default'					=> 'ffffff',
                    'preview'           		=> array(
                        'type'              		=> 'css',
                        'selector'          		=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
                        'property'          		=> 'color',
                    ),
				),
				'out_of_stock_padding'       => array(
					'type'			=> 'dimension',
					'label'			=> __( 'Padding', 'woopack' ),
					'units' 		=> array('px'),
					'slider'		=> true,
					'default'     	=> '8',
					'preview'     	=> array(
						'type'    		=> 'css',
						'selector'		=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
						'property'		=> 'padding',
						'unit'			=> 'px',
					),
					'responsive'  	=> true,
				),
				'out_of_stock_border_group'	 => array(
					'type'                  	=> 'border',
					'label'                 	=> __('Border Style', 'woopack'),
					'responsive'            	=> true,
					'preview'               	=> array(
						'type'              	    => 'css',
						'selector' 		    	    => '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
					),
				),
            ),
        ),
        'meta_style'           => array(
			'title'             	=> __('Product Taxonomy', 'woopack'),
			'collapsed'				=> true,
            'fields'            	=> array(
                'meta_text_color'		=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Text Color', 'woopack'),
                    'show_reset'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta, .woocommerce div.products div.product .woopack-product-meta .product_meta',
                        'property'				=> 'color',
                    ),
                ),
                'meta_link_color'    	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Link Color', 'woopack'),
                    'show_reset'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta .posted_in a, .woocommerce div.products div.product .woopack-product-meta .product_meta .posted_in a',
                        'property'				=> 'color',
                    ),
                ),
                'meta_border'	     	=> array(
                    'type'                  => 'select',
                    'label'  			    => __('Show Divider?', 'woopack'),
                    'default'               => 'yes',
                    'options' 		        => array(
                        'yes'              		=> __('Yes', 'woopack'),
                        'no'             		=> __('No', 'woopack'),
                    ),
                    'toggle'				=> array(
                        'yes'					=> array(
                            'fields'				=> array('meta_border_color')
                        )
                    )
                ),
                'meta_border_color'  	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Divider Color', 'woopack'),
                    'default'           	=> 'eeeeee',
                    'show_reset'        	=> true,
                ),
                'meta_padding'   		=> array(
                    'type'                  => 'dimension',
                    'label'                 => __('Padding', 'woopack'),
					'units'					=> array('px'),
					'slider'				=> true,
                    'responsive' 			=> true,
					'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta, .woocommerce div.products div.product .woopack-product-meta .product_meta',
                        'property'				=> 'padding',
						'unit'					=> 'px'
                    ),
                ),
            ),
        ),
        'product_rating_style' => array(
			'title'                 => __('Rating', 'woopack'),
			'collapsed'				=> true,
            'fields'                => array(
                'product_rating_size'          => array(
                    'type'              	=> 'unit',
                    'label'                 => __('Size', 'woopack'),
					'units'					=> array('px'),
					'slider'				=> true,
                    'responsive' 			=> true,
					'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .star-rating, .woocommerce div.products div.product .star-rating',
                        'property'				=> 'font-size',
						'unit'					=> 'px'
                    ),
                ),
                'product_rating_default_color' => array(
                    'type'              			=> 'color',
                    'label'             			=> __('Rating Star Color', 'woopack'),
                    'show_reset'        			=> true,
					'preview'           			=> array(
                        'type' 							=> 'css',
                        'selector'						=> '.woocommerce ul.products li.product .star-rating:before, .woocommerce div.products div.product .star-rating:before',
                        'property'						=> 'color',
                    ),
                ),
                'product_rating_color'         => array(
                    'type'              			=> 'color',
                    'label'             			=> __('Rating Star Active Color', 'woopack'),
                    'show_reset'        			=> true,
					'preview'           			=> array(
                        'type' 							=> 'css',
                        'selector'						=> '.woocommerce ul.products li.product .star-rating span:before, .woocommerce div.products div.product .star-rating span:before',
                        'property'						=> 'color',
                    ),
                ),
                'product_rating_margin_bottom' => array(
                    'type' 							=> 'unit',
                    'label' 						=> __('Margin Bottom', 'woopack'),
                    'default' 						=> '10',
					'units'							=> array('px'),
					'slider'						=> true,
                    'responsive' 					=> true,
					'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woocommerce ul.products li.product .star-rating, .woocommerce div.products div.product .star-rating',
                        'property'				=> 'margin-bottom',
						'unit'					=> 'px'
                    ),
				),
                'product_rating_count_color'   => array(
                    'type'       => 'color',
                    'label'      => __('Rating Count Color', 'woopack'),
					'show_reset' => true,
					'preview'    => array(
                        'type'     => 'css',
                        'selector' => '.woocommerce ul.products li.product .woocommerce-rating-count, .woocommerce div.products div.product .woocommerce-rating-count',
                        'property' => 'color',
                    ),
                ),
            ),
        ),
        'quick_view_style' 	   => array(
			'title'             	=> __('Quick View', 'woopack'),
			'collapsed'				=> true,
            'fields'            	=> array(
                'quick_view_text_color' => array(
                    'type'              	=> 'color',
                    'label'             	=> __('Text Color', 'woopack'),
                    'show_reset'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woopack-product-quick-view',
                        'property'				=> 'color',
                    ),
                ),
                'quick_view_bg_color'	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Background Color', 'woopack'),
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                    'preview'           	=> array(
                        'type' 					=> 'css',
                        'selector'				=> '.woopack-product-quick-view',
                        'property'				=> 'background-color',
                    ),
                ),
                'quick_view_padding'    => array(
                    'type'              	=> 'dimension',
                    'label' 				=> __('Padding', 'woopack'),
					'slider'				=> true,
					'units'					=> array( 'px' ),
                    'responsive' 			=> true,
                    'preview'               => array(
                        'type'                  => 'css',
                        'selector'              => '.woopack-product-quick-view',
                        'property'              => 'padding',
                        'unit'                  => 'px'
                    ),
                ),
				'quick_view_popup_width'	=> array(
					'type'		=> 'select',
					'label'		=> __('Popup Width', 'woopack'),
					'default'	=> 'auto',
					'options'	=> array(
						'auto'		=> __('Auto', 'woopack'),
						'custom'	=> __('Custom', 'woopack')
					),
					'toggle'	=> array(
						'custom'	=> array(
							'fields'	=> array('quick_view_popup_width_custom')
						)
					)
				),
				'quick_view_popup_width_custom'	=> array(
					'type'			=> 'unit',
					'label'			=> __('Custom Popup Width', 'woopack'),
					'default'		=> 670,
					'units'			=> array('px'),
					'slider'		=> true,
				),
				'quick_view_popup_overlay_bg_color'	=> array(
                    'type'              	=> 'color',
                    'label'             	=> __('Popup Overlay Background Color', 'woopack'),
                    'show_reset'        	=> true,
                    'show_alpha'        	=> true,
                ),
            ),
		),
		'variation_style'		=> array(
			'title'					=> __('Variation Table', 'woopack'),
			'collapsed'				=> true,
			'fields'				=> woopack_product_variation_style_fields()
		)
	) );
}

function woopack_product_variation_style_fields() {
	$fields = array(
		'variation_table_style'			=> array(
			'type'					=> 'select',
			'label'					=> __('Use Custom Style', 'woopack'),
			'default'				=> 'no',
			'options'				=> array(
				'yes'					=> __('Yes', 'bb-powerpack'),
				'no'					=> __('No', 'bb-powerpack')
			),
			'toggle'				=> array(
				'yes'					=> array(
					'fields'				=> array(
						'variation_table_width',
						'variation_table_border',
						'variation_table_border_style',
						'variation_table_border_color',
						'variation_table_cell_padding',
						'variation_table_label_bg_color',
						'variation_table_label_text_color',
						'variation_table_value_bg_color',
						'variation_table_value_text_color'
					)
				)
			)
		),
		'variation_table_width'			=> array(
			'type'						=> 'select',
			'label'						=> __('Width', 'woopack'),
			'default'					=> 'full',
			'options'					=> array(
				'auto'						=> __('Auto', 'woopack'),
				'full'						=> __('Full Width', 'woopack')
			)
		),
		'variation_table_border'		=> array(
			'type'						=> 'unit',
			'label'						=> __('Border Width', 'woopack'),
			'default'					=> '1',
			'units'						=> array('px'),
			'slider'					=> true,
		),
		'variation_table_border_style'	=> array(
			'type'							=> 'select',
			'label'							=> __('Border Style', 'woopack'),
			'default'						=> 'solid',
			'options'						=> array(
				'dashed'						=> __('Dashed', 'woopack'),
				'dotted'						=> __('Dotted', 'woopack'),
				'solid'							=> __('Solid', 'woopack'),
			)
		),
		'variation_table_border_color'	=> array(
			'type'							=> 'color',
			'label'							=> __('Border Color', 'woopack'),
			'default'						=> 'eeeeee',
			'show_reset'					=> true
		),
		'variation_table_cell_padding'	=> array(
			'type'							=> 'unit',
			'label'							=> __('Spacing', 'woopack'),
			'default'						=> '5',
			'units'							=> array('px'),
			'slider'						=> true,
		),
		'variation_table_label_bg_color'	=> array(
			'type'								=> 'color',
			'label'								=> __('Label Background Color', 'woopack'),
			'default'							=> 'fcfcfc',
			'show_reset'						=> true
		),
		'variation_table_label_text_color'	=> array(
			'type'								=> 'color',
			'label'								=> __('Label Text Color', 'woopack'),
			'show_reset'						=> true
		),
		'variation_table_value_bg_color'	=> array(
			'type'								=> 'color',
			'label'								=> __('Value Background Color', 'woopack'),
			'show_reset'						=> true
		),
		'variation_table_value_text_color'	=> array(
			'type'								=> 'color',
			'label'								=> __('Value Text Color', 'woopack'),
			'show_reset'						=> true
		),
	);

	return apply_filters( 'woopack_product_variation_style_fields', $fields );
}