.woopack-product-image {
	position: relative;
    padding: 0;
}
.woopack-product-image a {
    display: block;
}
.woopack-product-content {
    padding: 0;
}
.woopack-product-title {
	margin: 0;
	padding: 0;
}
.woopack-product-rating .star-rating {
	margin-left: auto;
	margin-right: auto;
}
.woopack-product-meta {
	padding: 0;
	margin-top: 10px;
}
.woopack-product-meta a {
	text-decoration: none;
}
.woopack-product-meta,
.woopack-product-meta a {
    font-size: 12px;
}
.woopack-product-action .woopack-qty-input,
.woopack-product-action .variations_form .quantity {
	display: inline-block;
    margin-right: 2px;
}
.woopack-product-action.woopack-qty-custom .quantity {
	border: 1px solid #ddd;
}
.woopack-product-action.woopack-qty-custom .quantity input.qty {
	border: 1px solid #ddd;
    border-top: 0;
    border-bottom: 0;
    background: none;
    border-radius: 0;
}
.woopack-product-action form > table {
	margin-left: auto;
	margin-right: auto;
	text-align: left;
}
.woopack-product-action form > table td.value {
	padding: 2px 0px;
}
.woopack-product-action form > table td.label {
	color: inherit;
    text-transform: capitalize;
}
.woopack-product-action form > table .reset_variations {
	margin-left: 5px;
}
.woopack-product-quick-view {
    background-color: rgba(255, 255, 255, 0.6);
    position: absolute;
    bottom: 0;
    height: 50px;
	width: 100%;
	opacity: 0;
    transition: all 0.3s;
}
.woopack-product-grid:hover .woopack-product-quick-view,
.woopack-product-carousel:hover .woopack-product-quick-view {
	opacity: 1;
}
.woopack-product-quick-icon + .woopack-quick-view-text {
    margin-left: 10px;
}