<?php
/**
 * Loop Product Image.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-image.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */

global $post;
?>

<?php do_action( 'woopack_loop_before_product_image_wrap', $settings, $product ); ?>

<div class="woopack-product-image">
	<a href="<?php echo get_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
		<?php do_action( 'woopack_product_before_image', $settings, $product ); ?>
		<?php do_action( 'woopack_loop_before_product_image', $settings, $product ); ?>
		
		<?php WooPack_Helper::get_template_file( 'loop-product-sale-badge', $settings, $product ); ?>

		<?php WooPack_Helper::render_product_images( $product, $settings ); ?>

		<?php WooPack_Helper::get_template_file( 'loop-product-quick-view', $settings, $product ); ?>
		
		<?php WooPack_Helper::get_template_file( 'loop-product-stock-status', $settings, $product ); ?>

		<?php do_action( 'woopack_loop_after_product_image', $settings, $product ); ?>
		<?php do_action( 'woopack_product_after_image', $settings, $product ); ?>
	</a>
</div>

<?php do_action( 'woopack_loop_after_product_image_wrap', $settings, $product ); ?>
