<?php
/**
 * Loop Product Rating.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-rating.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php
$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();

if ( $rating_count > 0 ) {
	if ( 'yes' === $settings->product_rating ) {
		echo wc_get_rating_html( $product->get_average_rating() );
	}

	if ( comments_open() && isset( $settings->product_rating_count ) && 'yes' === $settings->product_rating_count ) {
		?>
		<a href="#reviews" class="woocommerce-rating-count" rel="nofollow">
		<?php
		if ( isset( $settings->product_rating_text ) && ! empty( $settings->product_rating_text ) ) {
			$rating_text = $settings->product_rating_text;
			if ( isset( $settings->product_rating_text_plural ) && ! empty( $settings->product_rating_text_plural ) ) {
				if ( $review_count > 1 ) {
					$rating_text = $settings->product_rating_text_plural;
				}
			}
		?>
			(<span class="count"><?php echo $review_count; ?></span> <?php echo $rating_text; ?>)
		<?php } else { ?>
			<?php // translators: %s is for Rating. ?>
			(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'woocommerce' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?>)
		<?php } ?>
		</a>
		<?php
	}
}
