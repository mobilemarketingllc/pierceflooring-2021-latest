<?php
/**
 * Loop Product title.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-title.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php do_action( 'woopack_loop_before_product_title' ); ?>
<<?php echo $settings->product_title_heading_tag; ?> class="woopack-product-title">
	<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
		<?php echo get_the_title(); ?>
	</a>
</<?php echo $settings->product_title_heading_tag; ?>>
<?php do_action( 'woopack_loop_after_product_title' ); ?>
