<?php
/**
 * Loop Product Image.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-sale-badge.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php
global $post;

if ( $product->is_on_sale() && 'yes' == $settings->show_sale_badge ) :
		$sale_badge = sprintf( '<span class="onsale">%s</span>', esc_html__( 'Sale!', 'woopack' ) );
		echo apply_filters( 'woocommerce_sale_flash', $sale_badge, $post, $product );
endif;
