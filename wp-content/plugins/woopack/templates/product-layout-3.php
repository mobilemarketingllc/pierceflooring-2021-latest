<?php
/**
 * Product Layout 3
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/product-layout-3.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php global $post, $product; ?>

<<?php echo $settings->item_html_tag; ?> id="woopack-product-<?php echo get_the_ID(); ?>"<?php echo WooPack_Helper::render_post_class( $product, $settings, $settings->layout_style ); ?> data-product-id="<?php echo get_the_ID(); ?>">
	<div class="woopack-product-wrapper">

		<?php
		if ( 'yes' == $settings->show_image ) {
			WooPack_Helper::get_template_file( 'loop-product-image', $settings, $product );
		}
		?>

		<div class="woopack-product-content">
			<?php
			if ( 'yes' == $settings->product_title ) {
				WooPack_Helper::get_template_file( 'loop-product-title', $settings, $product );
			}
			if ( 'yes' == $settings->product_rating ) {
				WooPack_Helper::get_template_file( 'loop-product-rating', $settings, $product );
			}
			if ( 'yes' == $settings->product_price ) {
				wc_get_template( 'loop/price.php' );
			}
			if ( 'yes' == $settings->product_short_description ) {
				WooPack_Helper::get_template_file( 'loop-product-description', $settings, $product );
			}
			if ( 'none' != $settings->button_type ) {
				WooPack_Helper::get_template_file( 'loop-product-button', $settings, $product );
			}
			?>
		</div>
	</div>
	<div class="woopack-product-meta-wrapper">
		<?php
		if ( 'yes' == $settings->show_taxonomy ) {
			include WOOPACK_DIR . 'templates/loop-product-meta.php';
		}
		?>
	</div>
</<?php echo $settings->item_html_tag; ?>>
