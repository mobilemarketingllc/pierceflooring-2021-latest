<?php
/**
 * Loop Product Button.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-button.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php do_action( 'woopack_loop_before_product_action_wrap', $settings, $product ); ?>

<div class="woopack-product-action<?php echo isset( $settings->qty_style ) && 'custom' === $settings->qty_style ? ' woopack-qty-custom' : ''; ?>">
	<?php do_action( 'woopack_loop_product_action_wrap_start', $settings, $product ); ?>

	<?php if ( 'cart' == $settings->button_type ) : ?>

		<?php if ( ! $product->is_type( 'variable' ) && ! $product->is_type( 'grouped' ) && isset( $settings->qty_input ) && ( 'before_button' == $settings->qty_input || 'above_button' == $settings->qty_input ) ) : // Quantity input field before button. ?>
			
		<?php do_action( 'woopack_loop_before_product_quantity', $settings, $product ); ?>

			<span class="woopack-qty-input quantity">
				<input type="number" id="quantity_<?php echo $product->get_id(); ?>" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="<?php _e( 'Quantity', 'woopack' ); ?>" size="4" pattern="[0-9]*" inputmode="numeric" />
			</span>

		<?php do_action( 'woopack_loop_after_product_quantity', $settings, $product ); ?>

		<?php endif; ?>

		<?php do_action( 'woopack_loop_before_product_button', $settings, $product ); ?>

		<?php

		if ( $product->is_type( 'variable' ) && isset( $settings->variation_fields ) && 'yes' == $settings->variation_fields ) {
			woocommerce_variable_add_to_cart();
		} elseif ( $product->is_type( 'grouped' ) && isset( $settings->variation_fields ) && 'yes' == $settings->variation_fields ) {
			woocommerce_grouped_add_to_cart();
		} else {
			// Add to Cart button.
			$class = implode( ' ', array_filter( array(
				'button',
				'product_type_' . $product->get_type(),
				$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
				$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
				'alt',
			) ) );

			$args = array(
				'quantity' => esc_attr( isset( $quantity ) ? $quantity : 1 ),
				'class' => $class,
				'attributes' => array(
					'data-product_id' 	=> $product->get_id(),
					'data-product_sku' 	=> $product->get_sku(),
					'data-woopack' 		=> true,
					'aria-label'       	=> $product->add_to_cart_description(),
					'rel'              => 'nofollow',
				)
			);

			echo apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
				sprintf( '<a rel="nofollow" href="%s" target="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" data-woopack="true" class="%s" aria-label="%s">%s</a>',
					esc_url( $product->add_to_cart_url() ),
					esc_attr( $product->is_type( 'external' ) ? '_blank' : '_self' ),
					esc_attr( isset( $quantity ) ? $quantity : 1 ),
					esc_attr( $product->get_id() ),
					esc_attr( $product->get_sku() ),
					esc_attr( isset( $class ) ? $class : 'button' ),
					esc_attr( $product->add_to_cart_description() ),
					$product->add_to_cart_text()
				),
				$product,
				$args
			);
		}
		?>

		<?php do_action( 'woopack_loop_after_product_button', $settings, $product ); ?>

		<?php if ( ! $product->is_type( 'variable' ) && ! $product->is_type( 'grouped' ) && isset( $settings->qty_input ) && 'after_button' == $settings->qty_input ) : // Quantity input field after button. ?>

			<?php do_action( 'woopack_loop_before_product_quantity', $settings, $product ); ?>

			<span class="woopack-qty-input quantity">
				<input type="number" id="quantity_<?php echo $product->get_id(); ?>" class="input-text qty text" step="1" min="1" max="" name="quantity" value="1" title="<?php _e( 'Quantity', 'woopack' ); ?>" size="4" pattern="[0-9]*" inputmode="numeric" />
			</span>

			<?php do_action( 'woopack_loop_after_product_quantity', $settings, $product ); ?>

		<?php endif; ?>

	<?php endif; ?>

	<?php if ( 'custom' == $settings->button_type ) : // Custom button. ?>
		<?php do_action( 'woopack_loop_before_product_button', $settings, $product ); ?>
		<a href="<?php echo get_permalink(); ?>" rel="bookmark" class="button" target="<?php echo $settings->button_target; ?>" title="<?php echo get_the_title(); ?>"><?php echo $settings->button_text; ?></a>
		<?php do_action( 'woopack_loop_after_product_button', $settings, $product ); ?>
	<?php endif; ?>

	<?php do_action( 'woopack_loop_product_action_wrap_end', $settings, $product ); ?>
</div>

<?php do_action( 'woopack_loop_after_product_action_wrap', $settings, $product ); ?>
