<?php
/**
 * Loop Product Description.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-description.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php
	$product_data = $product->get_data();
?>

<?php do_action( 'woopack_loop_before_product_description', $settings, $product ); ?>
		
<div class="woopack-product-description">
	<?php echo apply_filters( 'woocommerce_short_description', $product_data['short_description'] ); ?>
</div>

<?php do_action( 'woopack_loop_after_product_description', $settings, $product ); ?>
