<?php
namespace JordanLeven\Plugins\BeaversaurusRex; define('EDD_SAMPLE_PLUGIN_LICENSE_PAGE', 'beaverbuildermegamenulicense'); function edd_sample_license_menu() { add_plugins_page('BB Mega Menu License', 'BB Mega Menu License', 'manage_options', EDD_SAMPLE_PLUGIN_LICENSE_PAGE, __NAMESPACE__ . '\\edd_sample_license_page'); } function edd_sample_license_page() { $sp39a595 = get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY); $sp4a79b9 = get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS); ?>
    <div class="wrap">
        <h2><?php  _e('Plugin License Options'); ?>
</h2>
        <?php  if (isset($_GET['sl_activation']) && $_GET['sl_activation'] === 'false' && isset($_REQUEST['message'])) { echo sprintf('<div class="notice notice-error"><p>Error activating: %s</p></div>', $_REQUEST['message']); } ?>
        <form method="post" action="options.php">

            <?php  settings_fields('edd_sample_license'); ?>

            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row" valign="top">
                            <?php  _e('License Key'); ?>
                        </th>
                        <td>
                            <input id="beaversaurus_rex_license_key" name="beaversaurus_rex_license_key" type="text" class="regular-text" value="<?php  esc_attr_e($sp39a595); ?>
" />
                            <label class="description" for="beaversaurus_rex_license_key"><?php  _e('Enter your license key'); ?>
</label>
                        </td>
                    </tr>
                    <?php  if (false !== $sp39a595) { ?>
                        <tr valign="top">
                            <th scope="row" valign="top">
                                <?php  _e('Activate License'); ?>
                            </th>
                            <td>
                                <?php  if ($sp4a79b9 !== false && $sp4a79b9 == 'valid') { ?>
                                    <span style="color:green;"><?php  _e('active'); ?>
</span>
                                    <?php  wp_nonce_field('beaversaurus_rex_noonce', 'beaversaurus_rex_noonce'); ?>
                                    <input type="submit" class="button-secondary" name="beaversaurus_rex_edd_license_deactivate" value="<?php  _e('Deactivate License'); ?>
"/>
                                <?php  } else { wp_nonce_field('beaversaurus_rex_noonce', 'beaversaurus_rex_noonce'); ?>
                                    <input type="submit" class="button-secondary" name="beaversaurus_rex_edd_license_activate" value="<?php  _e('Activate License'); ?>
"/>
                                <?php  } ?>
                            </td>
                        </tr>
                    <?php  } ?>
                </tbody>
            </table>
            <?php  submit_button(); ?>

        </form>
        <?php  } function edd_sample_register_option() { register_setting('edd_sample_license', 'beaversaurus_rex_license_key', 'edd_sanitize_license'); } function edd_sanitize_license($sp10d00b) { $spc97d0b = get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY); if ($spc97d0b && $spc97d0b != $sp10d00b) { delete_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS); } return $sp10d00b; } function edd_sample_activate_license() { if (isset($_POST['beaversaurus_rex_edd_license_deactivate'])) { delete_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY); delete_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS); } else { if (isset($_POST['beaversaurus_rex_edd_license_activate'])) { if (!check_admin_referer('beaversaurus_rex_noonce', 'beaversaurus_rex_noonce')) { return; } $sp39a595 = trim(get_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_KEY)); $spc5a7a5 = array('edd_action' => 'activate_license', 'license' => $sp39a595, 'item_id' => EDD_SL_ITEM_ID, 'url' => home_url()); $sp666db4 = wp_remote_post(EDD_SL_STORE_URL, array('timeout' => 15, 'sslverify' => false, 'body' => $spc5a7a5)); if (is_wp_error($sp666db4) || 200 !== wp_remote_retrieve_response_code($sp666db4)) { $spcbf0b5 = is_wp_error($sp666db4) && !empty($sp666db4->get_error_message()) ? $sp666db4->get_error_message() : __('An error occurred, please try again.'); } else { $sp19302b = json_decode(wp_remote_retrieve_body($sp666db4)); if (false === $sp19302b->success) { switch ($sp19302b->error) { case 'expired': $spcbf0b5 = sprintf(__('Your license key expired on %s.'), date_i18n(get_option('date_format'), strtotime($sp19302b->expires, current_time('timestamp')))); break; case 'revoked': $spcbf0b5 = __('Your license key has been disabled.'); break; case 'missing': $spcbf0b5 = __('Invalid license.'); break; case 'invalid': case 'site_inactive': $spcbf0b5 = __('Your license is not active for this URL.'); break; case 'item_name_mismatch': $spcbf0b5 = sprintf(__('This appears to be an invalid license key for %s.'), EDD_SL_ITEM_ID); break; case 'no_activations_left': $spcbf0b5 = __('Your license key has reached its activation limit.'); break; default: $spcbf0b5 = __('An error occurred, please try again.'); break; } } } if (!empty($spcbf0b5)) { $sp478d1f = admin_url('plugins.php?page=' . EDD_SAMPLE_PLUGIN_LICENSE_PAGE); $spcaa3b9 = add_query_arg(array('sl_activation' => 'false', 'message' => urlencode($spcbf0b5)), $sp478d1f); wp_redirect($spcaa3b9); die; } update_option(WP_OPTION_BEAVERSAURUS_REX_LICENSE_STATUS, $sp19302b->license); wp_redirect(admin_url('plugins.php?page=' . EDD_SAMPLE_PLUGIN_LICENSE_PAGE)); die; } } } add_action('admin_init', __NAMESPACE__ . '\\edd_sample_activate_license'); add_action('admin_init', __NAMESPACE__ . '\\edd_sample_register_option'); add_action('admin_menu', __NAMESPACE__ . '\\edd_sample_license_menu');